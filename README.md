<h1 align="center"> FoodMate - Tinder for food </h1> <br>
<p align="center">
<img alt="FoodMate" title="FoodMate" src="FoodMatePictures/foodmate_logo.png" width="450">
</p>

<p align="center">
  Aplikacija koja na osnovu lokacije korisnika daje predloge o restoranima u njegovoj blizini.
</p>
<p align="center">
    Tutorijal korišćenja aplikacije: <a href="https://www.youtube.com/watch?v=88GwJFY_pwQ">link</a>
</p>

## Sadržaj

- [Opis aplikacije](#opis-aplikacije)
- [Funkcionalnosti](#funkcionalnosti)
- [Instalacija](#instalacija)
- [FoodMate tim](#foodmate-tim)

## Opis aplikacije

FoodMate je android aplikacija koja korisniku preporučuje restorane i omogućava jednostavnu rezervaciju. Takođe, omogućava filtriranje restorana po načinu ishrane korisnika i definisanje maksimalne udaljenosti restorana od njegove trenutne lokacije. Zahvaljujući intuitivnom interfejsu koji se svodi na jednostavno prevlačenje levo ili desno, korisnik sa lakoćom bira ili odbacuje ponuđeni restoran. Prilikom rezervacije, moguće je pozvati prijatelje.

## Funkcionalnosti

* **Registracija i logovanje**
    <br/>
    <br/>
    Kako bi korisnik mogao koristiti aplikaciju, neophodno je da ima korisnički nalog. Korisnički nalog se kreira jednostavnom registracijom, tj unošenjem osnovnih podataka. Nakon registracije, korisnik dobija SMS poruku sa verifikacionim kodom kako bi potvrdio svoju registraciju. Pri pokretanju aplikacije, korisnik se prijavljuje na sistem.
    <p align="center">
      <img src = "FoodMatePictures/login.jpg" width=250 height=450>
      <img src = "FoodMatePictures/registration.jpg" width=250 height=450>
      <img src = "FoodMatePictures/verification.jpg" width=250 height=450>
    </p>

* **Podešavanja**
    <br/>
    <br/>
    Kako bi korisnik pronašao restoran koji mu odgovara, omogućena su mu sledeća podešavanja:
    
     - Udaljenost (maksimalna udaljenost restorana u odnosu na korisnikovu lokaciju)
     - Režim ishrane (regularni, vegeterijanski, veganski, bez laktoze, bez orašastih plodova…)
     <br/>
    <p align="center">
      <img src = "FoodMatePictures/settings.jpg" width=250 height=450>
    </p>

 * **Promena korisnikovih podataka**
     <br/>
     <br/>
      Korisnik ima mogućnost promene informacija o svom profilu.
    <p align="center">
      <img src = "FoodMatePictures/editProfile.jpg" width=250 height=450>
    </p>
      
* **Predlozi restorana**
   <br/>
   <br/>
    Nakon prijavljivanja na sistem, korisniku se prikazuju restorani koji zadovoljavaju kriterijume iz podešavanja. Interakcija korisnika i aplikacije se svodi na jednostavno prevlačenje. Prevlačenjem na desno korisnik vrši odabir restorana dok prevlačenjem na levo odbacuje restoran. Restorane je moguće odabrati i klikom na srce a odbiti na x. Klikom na sliku, prikazuju se detaljnije informacije o restoranu.
    <p align="center">
      <img src = "FoodMatePictures/main.jpg" width=250 height=450>
    </p>

* **Prikaz osnovnih informacija o restoranu**
   <br/>
   <br/>
   Na ekranu se nalaze sledeće informacije o restoranu:
   - Prezentacija slika koja može i da se prevlači
   - Naziv restorana
   - Ocena restorana
   - Ostale informacije kao što je: lokacija, broj telefona, opis...
   <br/>
   Prevlačenjem na desno, otvara se novi prozor sa prikazom menija restorana.
   <br/>
   <br/>
    <p align="center">
      <img src = "FoodMatePictures/restaurantInfo1.jpg" width=250 height=450>
      <img src = "FoodMatePictures/restaurantInfo.jpg" width=250 height=450>
      <img src = "FoodMatePictures/workingDays.jpg" width=250 height=450>
    </p>

* **Prikaz menija restorana**
    <br/>
    <br/>
    Meni restorana je izdeljen na sekcije gde svaki obrok sadrži sliku, sastojke i cenu.
   <p align="center">
      <img src = "FoodMatePictures/menu.jpg" width=250 height=450>
    </p>

* **Rezervacija**
    <br/>
    <br/>
    Prevlačenjem na desno, korisniku se prikazuje tačna lokacija restorana na mapi i polja za unos podataka koji su potrebni za rezervaciju.
    <br/>
    Prilikom rezervacije, korisnik može da pozove prijatelje iz svoje kontakt liste. Nakon uspešne rezervacije, svim pozvanim prijateljima će stići poruka a korisniku koji je izvršio rezervaciju mejl o uspešnosti rezervacije.
    <p align="center">
      <img src = "FoodMatePictures/reservation.jpg" width=250 height=450>
      <img src = "FoodMatePictures/reservation1.jpg" width=250 height=450>
      <img src = "FoodMatePictures/reservation2.jpg" width=250 height=450>
      <img src = "FoodMatePictures/reservation3.jpg" width=250 height=450>
      <img src = "FoodMatePictures/reservation4.jpg" width=250 height=450>
      <img src = "FoodMatePictures/reservation5.jpg" width=250 height=450>
    </p>

## Instalacija

   Instalacija aplikacije se vrši na osnovu APK fajla.
   
## FoodMate tim

<img width=40 height=50 src="FoodMatePictures/boki.jpg">
<img width=40 height=50 src="FoodMatePictures/andrej.jpg">
<img width=40 height=50 src="FoodMatePictures/jeca.jpg">