package ftn.uns.ac.rs.foodmateapp.activities;


import android.Manifest;
import android.accounts.Account;
import android.app.ActivityOptions;
import android.content.*;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.adapters.SuggestionAdapter;
import ftn.uns.ac.rs.foodmateapp.asynctasks.*;
import ftn.uns.ac.rs.foodmateapp.commons.Constants;
import ftn.uns.ac.rs.foodmateapp.commons.LocationUtils;
import ftn.uns.ac.rs.foodmateapp.commons.OnSwipeTouchListener;
import ftn.uns.ac.rs.foodmateapp.commons.Permissions;
import ftn.uns.ac.rs.foodmateapp.dto.NewUserChosenRestaurantDTO;
import ftn.uns.ac.rs.foodmateapp.libraries.swipecards.SwipeFlingAdapterView;
import ftn.uns.ac.rs.foodmateapp.models.dao.ChosenRestaurantDao;
import ftn.uns.ac.rs.foodmateapp.models.dao.RestaurantDao;
import ftn.uns.ac.rs.foodmateapp.models.database.AppDatabase;
import ftn.uns.ac.rs.foodmateapp.models.entities.ChosenRestaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.Location;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.relations.RestaurantAndImage;
import ftn.uns.ac.rs.foodmateapp.sync.Authenticator;
import ftn.uns.ac.rs.foodmateapp.sync.SyncAdapter;
import ftn.uns.ac.rs.foodmateapp.viewmodel.ChosenRestaurantViewModel;
import ftn.uns.ac.rs.foodmateapp.viewmodel.RestaurantViewModel;
import ftn.uns.ac.rs.foodmateapp.viewmodel.UserViewModel;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

public class SuggestionsActivity extends BaseActivity implements AsyncResponse {

    // Stub, required for SyncAdapter
    Account mAccount;

    public static final String EXTRA_RESTAURANT = "restaurant_extra";
    public static final String EXTRA_RESTAURANT_IMAGE = "restaurant_image_extra";
    public static final String ALREADY_SUGGESTED_IDS = "already_suggested_ids";
    public static final int NUM_SUGGESTIONS_TO_LOAD = 10;

    public static List<Long> alreadySuggested;
    public List<Long> swiped;
    private ArrayList<RestaurantAndImage> suggestions;
    private Map<Long, RestaurantAndImage> tempSuggestions;
    private ArrayAdapter<RestaurantAndImage> suggestionAdapter;

    private SwipeFlingAdapterView flingContainer;
    private TextView swipeIndicatorText;
    private TextView restaurantInfoText;
    private FloatingActionButton heartButton;
    private FloatingActionButton rejectButton;
    private ProgressBar progressBar;

    private boolean suggestionsLoaded;
    private boolean noMoreSuggestions;
    private boolean firstSuggestionLoaded;
    private boolean locationPermissionDenied;
    private int numImagesLoaded;

    private RestaurantViewModel restaurantViewModel;
    private ChosenRestaurantViewModel chosenRestaurantViewModel;
    private UserViewModel userViewModel;

    private SharedRef sharedRef;

    private final int REQUEST_LOCATION_PERMISSION = 1;
    private FusedLocationProviderClient fusedLocationClient;
    private android.location.Location userLocation;
    private LoadSuggestionsTask loadSuggestionsTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestions);

        restaurantViewModel = ViewModelProviders.of(this).get(RestaurantViewModel.class);
        chosenRestaurantViewModel = ViewModelProviders.of(this).get(ChosenRestaurantViewModel.class);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        sharedRef = new SharedRef(this);

        mAccount = Authenticator.CreateSyncAccount(getApplicationContext());

        swiped = new ArrayList<>();

        setRestaurantTextFields(null);

        locationPermissionDenied = false;
        noMoreSuggestions = false;
        firstSuggestionLoaded = false;
        numImagesLoaded = 0;
        alreadySuggested = null;
        setUpButtons();
        //setUpPermissions();
        hideProgressBar();
        setUpSwipeNavigation();
    }


    @Override
    protected void onResume() {
        super.onResume();
        //if(alreadySuggested==null)
        sharedRef = new SharedRef(this);
        if(suggestions != null)
            Log.d("SUGG", "suggestions: " + suggestions.toString());
        if(suggestions==null || suggestions.isEmpty()) {
            loadAlreadySuggested();
            loadSuggestions(NUM_SUGGESTIONS_TO_LOAD);
            if(!locationPermissionDenied)
                checkLocationPermission();
            setUpFlingContainer();

        }

        boolean needsRefresh = sharedRef.getDataBoolean("needsRefresh", false);
        if (needsRefresh)
            refreshSuggestions();
        
        registerReceiver(syncFinishedReceiver, new IntentFilter(SyncAdapter.SYNC_FINISHED_BROADCAST));
        registerReceiver(imageLoadedReceiver, new IntentFilter(LoadRestaurantImage.IMAGE_LOADED_BROADCAST));
        Log.d("SYNC", "Activity resumed, should sync now.");
        runSync();

    }

    @Override
    protected void onPause() {
        super.onPause();
        saveAlreadySuggested();
        unregisterReceiver(syncFinishedReceiver);
        unregisterReceiver(imageLoadedReceiver);
    }

    private void loadAlreadySuggested() {
        Gson gson = new Gson();
        String alreadySuggestedJson = sharedRef.getDataString(ALREADY_SUGGESTED_IDS, null);
        if(alreadySuggestedJson == null) {
            Log.d("SUGG", "No suggestions to load");
            alreadySuggested = new ArrayList<>();
            swiped = new ArrayList<>();
        } else {
            Type listLongType = new TypeToken<List<Long>>(){}.getType();
            alreadySuggested = gson.fromJson(alreadySuggestedJson, listLongType);
            if(alreadySuggested != null){
                Log.d("SUGG", alreadySuggested.toString());
                swiped = new ArrayList<>();
                swiped.addAll(alreadySuggested);
            }
        }
    }

    private void saveAlreadySuggested(){
        if(noMoreSuggestions && suggestions.isEmpty()){
            sharedRef.saveDataString(ALREADY_SUGGESTED_IDS, null);
            Log.d(ALREADY_SUGGESTED_IDS, "Saving already sugg: null");
        }
        else{
            Gson gson = new Gson();
            String alreadySuggestedJson = gson.toJson(swiped);
            sharedRef.saveDataString(ALREADY_SUGGESTED_IDS, alreadySuggestedJson);
            Log.d(ALREADY_SUGGESTED_IDS, "Saving already sugg: " + alreadySuggestedJson);
        }
    }

    private void checkLocationPermission() {
        int permission_status = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permission_status == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            getLocation();
        } else {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            Log.d("SUGG_LOCATION", "GrantResults: " + Arrays.toString(grantResults));
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("SUGG_LOCATION", "Location permission granted.");
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
                getLocation();
            } else {
                locationPermissionDenied = true;
                Log.d("SUGG_LOCATION", "Location permission denied.");
                Toast.makeText(this, "Location permission required to show suggestions.",
                        Toast.LENGTH_SHORT);
            }
        }

    }

    private void getLocation(){
        fusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
            if(location!=null) {
                Log.d("SUGG_LOCATION", "Location loaded: " + location.toString());
                int distance = Integer.parseInt(sharedRef.getDataString("distance", "20"));
                Log.d("LOAD_SUGG", "Loading restaurants within distance " + distance);
                if(loadSuggestionsTask.getStatus() == AsyncTask.Status.PENDING){
                    loadSuggestionsTask.execute(
                            LocationUtils.calculateCosLatitude(location.getLatitude()),
                            LocationUtils.calculateSinLatitude(location.getLatitude()),
                            LocationUtils.calculateCosLongitude(location.getLongitude()),
                            LocationUtils.calculateSinLongitude(location.getLongitude()),
                            LocationUtils.calculateCosScaledDistance(distance)
                    );
                }
                userLocation = location;
                getCity(location);
            } else {
                Log.d("SUGG_LOCATION", "Location is null");
            }
        });
    }

    private void getCity(android.location.Location location) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> list = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if(list != null && list.size()>0){
                String city = list.get(0).getLocality();
                //TODO: Save to sharedPref, if changed run sync again
            }
        } catch (IOException e) {
            Log.e("SUGG_LOCATION", "Can't get current city");
        }
    }



    private void runSync() {
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(mAccount, Authenticator.AUTHORITY, settingsBundle);
    }

    private final BroadcastReceiver syncFinishedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("SYNC", "SuggestionsActivity received broadcast: Sync finished.");
            if((suggestions==null || suggestions.isEmpty())){
                loadSuggestions(NUM_SUGGESTIONS_TO_LOAD);
                if(!locationPermissionDenied)
                    checkLocationPermission();
            }
        }
    };



    private void loadSuggestions(int count){
        initSuggestions();
        suggestionsLoaded = false;
        loadSuggestionsTask = new LoadSuggestionsTask(this, suggestions, count,
                loadedSuggestions -> {
                    if (loadedSuggestions.isEmpty())
                        onNoMoreSuggestions();
                    int index=0;
                    Log.d("LOAD_SUGG", "Loaded " + loadedSuggestions.size() + " suggestions");
                    for (RestaurantAndImage restaurantAndImage: loadedSuggestions) {
                        LoadRestaurantImage imageLoaderTask =
                                new LoadRestaurantImage(restaurantAndImage, suggestionAdapter, this);
                        imageLoaderTask.execute(restaurantAndImage.restaurant.getRestaurantId());
                        tempSuggestions.put(restaurantAndImage.restaurant.getRestaurantId(), restaurantAndImage);
                        alreadySuggested.add(restaurantAndImage.restaurant.getRestaurantId());
                        index++;
                    }
                });

//        loadSuggestionsTask.execute(
//                LocationUtils.calculateCosLatitude(45.255),
//                LocationUtils.calculateSinLatitude(45.255),
//                LocationUtils.calculateCosLongitude(19.84),
//                LocationUtils.calculateSinLongitude(19.84),
//                LocationUtils.calculateCosScaledDistance(50)
//        );
    }

    private void initSuggestions() {
        if(suggestions == null)
            suggestions = new ArrayList<>();

        if(suggestionAdapter == null)
            suggestionAdapter = new SuggestionAdapter(this, suggestions);

        if(tempSuggestions == null)
            tempSuggestions = new HashMap<>();

        if(alreadySuggested == null)
            alreadySuggested = new LinkedList<>();
    }

    private final BroadcastReceiver imageLoadedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Long position = intent.getLongExtra("position", 0);
            suggestions.add(tempSuggestions.get(position));
            //Log.d("LOAD_SUGG", "Loaded image for " + tempSuggestions.get(position).restaurant.getRestaurantId());
            tempSuggestions.remove(position);
            if((tempSuggestions.isEmpty() && suggestions.size()<2) || suggestions.size()==2)
                flingContainer.removeAllViewsInLayout();
            suggestionAdapter.notifyDataSetChanged();
            if(tempSuggestions.isEmpty()) {
                suggestionsLoaded = true;
            }
            if(!suggestions.isEmpty()) {
                setRestaurantTextFields(suggestions.get(0).restaurant);
                firstSuggestionLoaded = true;
            }
        }
    };


    private void onNoMoreSuggestions() {
        noMoreSuggestions=true;
        if(suggestions.isEmpty()){
            setRestaurantTextFields(null);
        }
    }

    private void setUpFlingContainer(){
        flingContainer = findViewById(R.id.frame);
        flingContainer.setAdapter(suggestionAdapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                swiped.add(suggestions.get(0).restaurant.getRestaurantId());
                suggestions.remove(0);
                suggestionAdapter.notifyDataSetChanged();
                if(suggestions.isEmpty()){
                    swipeIndicatorText.setText("No more suggestions.");
                    restaurantInfoText.setText("");
                } else {
                    setRestaurantTextFields(suggestions.get(0).restaurant);
                }

            }

            @Override
            public void onLeftCardExit(Object o) {
            }

            @Override
            public void onRightCardExit(Object o) {
                RestaurantAndImage restaurantAndImage = (RestaurantAndImage) o;
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(getApplicationContext(), ReservationActivity.class);
                        intent.putExtra(Constants.CHOSEN_RESTAURANT, Parcels.wrap(restaurantAndImage.restaurant));
                        startActivity(intent);
                    }
                }, 100);
                addToChosenRestaurants(restaurantAndImage);
            }

            @Override
            public void onAdapterAboutToEmpty(int i) {
                if(suggestionsLoaded){
                    Log.d("LOAD_SUGG", "Adapter about to empty, loading more suggestions...");
                    loadSuggestions(NUM_SUGGESTIONS_TO_LOAD);
                    if(!locationPermissionDenied)
                        checkLocationPermission();
                }
            }

            @Override
            public void onScroll(float v) {
                if(v <= 0.1f && v >=- 0.1f) {
                    ImageView cardHeart = flingContainer.getSelectedView().findViewById(R.id.suggestedCardHeart);
                    cardHeart.setAlpha(0f);
                    ImageView cardCancel = flingContainer.getSelectedView().findViewById(R.id.suggestedCardCancel);
                    cardCancel.setAlpha(0f);
                }
                else if(v > 0.1f) {
                    ImageView cardHeart = flingContainer.getSelectedView().findViewById(R.id.suggestedCardHeart);
                    cardHeart.setAlpha(v - 0.1f);
                }
                else if(v < -0.1f) {
                    ImageView cardCancel = flingContainer.getSelectedView().findViewById(R.id.suggestedCardCancel);
                    cardCancel.setAlpha(Math.abs(v) - 0.1f);
                }
            }
        });
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                Restaurant restaurant = ((RestaurantAndImage) dataObject).restaurant;

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                ((RestaurantAndImage) dataObject).image.getImage().compress(Bitmap.CompressFormat.JPEG, 50, stream);
                byte[] byteArray = stream.toByteArray();

                Intent intent = new Intent(getApplicationContext(), RestaurantInfoActivity.class);
                intent.putExtra(Constants.CHOSEN_RESTAURANT, Parcels.wrap(restaurant));
                intent.putExtra(Constants.CHOSEN_RESTAURANT_IMAGE, byteArray);
                ImageView imageView = flingContainer.getSelectedView().findViewById(R.id.suggestedImageView);
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                        SuggestionsActivity.this,
                        imageView,
                        imageView.getTransitionName());
                startActivity(intent, options.toBundle());
            }
        });
    }

    private void setRestaurantTextFields(Restaurant restaurant) {
        if(swipeIndicatorText == null)
            swipeIndicatorText = findViewById(R.id.mealNameText);
        if(restaurantInfoText == null)
            restaurantInfoText = findViewById(R.id.restaurantInfoText);
        if(restaurant==null) {
            swipeIndicatorText.setText("No suggestions.");
            restaurantInfoText.setText("");
            return;
        }
        swipeIndicatorText.setText(suggestions.get(0).restaurant.getName());
        if(userLocation != null) {
            android.location.Location restaurantLocation = new android.location.Location("restaurantLocation");
            restaurantLocation.setLatitude(restaurant.getLocation().getLatitude());
            restaurantLocation.setLongitude(restaurant.getLocation().getLongitude());
            float distanceToRestaurant = userLocation.distanceTo(restaurantLocation);
            restaurantInfoText.setText(LocationUtils.getDistanceString(distanceToRestaurant) + " away");
        }
    }


    private void addToChosenRestaurants(RestaurantAndImage restaurantAndImage) {
        restaurantViewModel.getRestaurantById(restaurantAndImage.getRestaurantId()).observe(this, restaurant -> {
            addAsyncTask(restaurant);
            addChosenToSQLite(restaurant);
        });
    }

    private void addChosenToSQLite(Restaurant restaurant) {
        System.out.println("Adding to SQLite db...");
        String username = sharedRef.getDataString("username", "null");
        userViewModel.getUserByUsername(username).observe(this, user -> {
                chosenRestaurantViewModel.insert(new ChosenRestaurant(user.getUserId(), restaurant.getRestaurantId()));});
    }

    private void addAsyncTask(Restaurant restaurant) {
        final String url = getString(R.string.add_chosen);
        final String username = sharedRef.getDataString("username", "null");

        NewUserChosenRestaurantDTO newUserChosenRestaurantDTO = new NewUserChosenRestaurantDTO(username, restaurant.getName());

        GenericAsyncTask<NewUserChosenRestaurantDTO, String> asyncTask = new GenericAsyncTask<>(this, String.class, this);
        asyncTask.setObjectToSend(newUserChosenRestaurantDTO);
        asyncTask.execute(url, sharedRef.getDataString("token", null));
    }

//    private void deleteFromChosenRestaurants(RestaurantAndImage restaurantAndImage) {
//        restaurantViewModel.getRestaurantById(restaurantAndImage.getRestaurantId()).observe(this, restaurant -> deleteAsyncTask(restaurant));
//    }

    private void deleteAsyncTask(Restaurant restaurant) {
        final String url = getString(R.string.delete_chosen);
        final String username = sharedRef.getDataString("username", "null");

        NewUserChosenRestaurantDTO newUserChosenRestaurantDTO = new NewUserChosenRestaurantDTO(username, restaurant.getName());

        GenericAsyncTask<NewUserChosenRestaurantDTO, String> asyncTask = new GenericAsyncTask<>(this, String.class, this);
        asyncTask.setObjectToSend(newUserChosenRestaurantDTO);
        asyncTask.execute(url, sharedRef.getDataString("token", null));
    }

    private void setUpButtons() {
        heartButton = findViewById(R.id.heartButton);
        rejectButton = findViewById(R.id.rejectButton);

        heartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flingContainer.getTopCardListener().selectRight();
                flingContainer.getSelectedView().findViewById(R.id.suggestedCardHeart).setAlpha(1f);
            }
        });

        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flingContainer.getTopCardListener().selectLeft();
                flingContainer.getSelectedView().findViewById(R.id.suggestedCardCancel).setAlpha(1f);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.suggestionsRefreshButton) {
            refreshSuggestions(true);
        }
        return super.onOptionsItemSelected(item);
    }

    private void refreshSuggestions() {
        refreshSuggestions(false);
    }

    private void refreshSuggestions(boolean withSync) {
        sharedRef.saveDataBoolean("needsRefresh", false);

        Log.d("REFRESH", "swiped: " + swiped.toString() + ", already: " + alreadySuggested.toString());

        boolean suggestionsWasEmpty = suggestions.isEmpty();
        suggestions.clear();
        suggestionAdapter.notifyDataSetChanged();
        flingContainer.removeAllViewsInLayout();

        firstSuggestionLoaded = false;
        noMoreSuggestions = false;
        if(!suggestionsWasEmpty)
            saveAlreadySuggested();
        alreadySuggested.clear();
        swiped.clear();
        if(!suggestionsWasEmpty)
            loadAlreadySuggested();
        if(withSync)
            runSync();
        else{
            loadSuggestions(NUM_SUGGESTIONS_TO_LOAD);
            if(!locationPermissionDenied)
                checkLocationPermission();
        }
        Log.d("REFRESH", "Refreshing...");
    }

    private void setUpPermissions() {
        Permissions permissions = new Permissions(this);
        if (!permissions.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        }
    }

    private void hideProgressBar() {
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
    }

    private void setUpSwipeNavigation() {
        ConstraintLayout view = findViewById(R.id.constraint_layout);
        view.setOnTouchListener(new OnSwipeTouchListener(this){
            public void onSwipeRight() {
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                Bundle bndlAnimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.left_to_right,0).toBundle();
                startActivity(intent,bndlAnimation);
            }
            public void onSwipeLeft() {
                Intent intent = new Intent(getApplicationContext(), ChosenRestaurantsActivity.class);
                Bundle bndlAnimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.right_to_left,0).toBundle();
                startActivity(intent,bndlAnimation);
            }
        });
    }

    private Restaurant createDummyRestaurant() {
        Location location = new Location(19.838157,45.239725,"Novi Sad","Srbija","Bulevar Oslobodjenja 41");
        return new Restaurant(1, "Savoca Pizzeria & Restaurant", "asdf", "asfd",
                "asdf", 5.0f, location, 1);
    }

    @Override
    public void onBackPressed(){
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }


    @Override
    public void onTaskCompleted(Object result) {
//        Toast.makeText(this, result.toString(),Toast.LENGTH_LONG).show();
    }

}
/*
    //Ovo dole izbrisete kada vidite kako se salje sa tokenom :)
    final String uri = getString(R.string.test_url);
    GenericAsyncTask<String, String> asyncTask =
            new GenericAsyncTask<>(this, String.class, this);
    Gson gson = new Gson();
    String json = gson.toJson("pliz");
        asyncTask.setObjectToSend(json);

                SharedRef sharedRef = new SharedRef(SuggestionsActivity.this);
                String token = sharedRef.getDataString("token",null);
                asyncTask.execute(uri, token);
// dovde se brise :) Ne ovo ispod!
 */
