package ftn.uns.ac.rs.foodmateapp.asynctasks;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import com.squareup.okhttp.OkHttpClient;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.adapters.SuggestionAdapter;
import ftn.uns.ac.rs.foodmateapp.commons.ImageAction;
import ftn.uns.ac.rs.foodmateapp.dto.sync.SyncDTO;
import ftn.uns.ac.rs.foodmateapp.dto.sync.SyncRequestDTO;
import ftn.uns.ac.rs.foodmateapp.models.dao.ImageDao;
import ftn.uns.ac.rs.foodmateapp.models.database.AppDatabase;
import ftn.uns.ac.rs.foodmateapp.models.entities.Image;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.relations.RestaurantAndImage;
import ftn.uns.ac.rs.foodmateapp.security.SSL;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.OkHttpClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.lang.ref.WeakReference;

public class LoadRestaurantImage extends AsyncTask<Long, Void, Long> {

    private final RestaurantAndImage restaurantAndImage;
    private final ArrayAdapter<RestaurantAndImage> adapter;
    private final WeakReference<Context> contextWeakReference;

    public static final String IMAGE_LOADED_BROADCAST =  "ftn.uns.ac.rs.foodmateapp.broadcasts.IMAGE_LOADED";

    public LoadRestaurantImage(RestaurantAndImage restaurantAndImage, ArrayAdapter<RestaurantAndImage> adapter, Context context) {
        this.restaurantAndImage = restaurantAndImage;
        this.adapter = adapter;
        this.contextWeakReference = new WeakReference<>(context);
    }

    @Override
    protected Long doInBackground(Long... params) {
        Context context = contextWeakReference.get();
        if(context == null)
            return params[0];

        Bitmap image = ImageAction.loadFromCache(restaurantAndImage.restaurant.getCachedImageFilename(), context);
        if(image == null) {
            String stringImage = sendImageRequest();
            System.out.println("IMAGEEEEE" + stringImage);
            image = ImageAction.stringToBitMap(stringImage);
            ImageAction.saveToCache(image, restaurantAndImage.restaurant.getCachedImageFilename(), context);
        }
        if(image != null) {
            restaurantAndImage.image.setImage(image);
        }
        return params[0];

    }

    private String sendImageRequest() {
        Context context = contextWeakReference.get();
        if (context == null)
            return null;

        SharedRef sharedRef = new SharedRef(context);

        String token = sharedRef.getDataString("token", null);
        HttpHeaders headers = new HttpHeaders();
        if(token != null){
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);
        }

        OkHttpClient client = SSL.createClient(context);
        ClientHttpRequestFactory requestFactory = new OkHttpClientHttpRequestFactory(client);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(requestFactory);

        ResponseEntity<String> response;
        final String url = context.getString(R.string.get_restaurant_image) + restaurantAndImage.getRestaurantId();

        try {
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            response = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
            return response.getBody();
        } catch (HttpClientErrorException ex) {
            Log.e("LOAD_IMAGE", ex.getResponseBodyAsString() + "; RestaurantID: "
                    + restaurantAndImage.getRestaurantId());
            return null;
        }catch(ResourceAccessException ex){
            Log.e("LOAD_IMAGE","Something went wrong! Please try again! RestaurantID: "
                    + restaurantAndImage.getRestaurantId());
            return null;
        }
    }

    @Override
    protected void onPostExecute(Long position) {
        super.onPostExecute(position);
        Context context = contextWeakReference.get();
        if(context != null){
            Intent intent = new Intent(IMAGE_LOADED_BROADCAST);
            intent.putExtra("position",position);
            context.sendBroadcast(intent);
        }

    }
}

