package ftn.uns.ac.rs.foodmateapp.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.asynctasks.AsyncResponse;
import ftn.uns.ac.rs.foodmateapp.asynctasks.GenericAsyncTask;
import ftn.uns.ac.rs.foodmateapp.commons.Constants;
import ftn.uns.ac.rs.foodmateapp.commons.CustomAdapter;
import ftn.uns.ac.rs.foodmateapp.commons.DatePickerFragment;
import ftn.uns.ac.rs.foodmateapp.commons.ErrorAlert;
import ftn.uns.ac.rs.foodmateapp.commons.Permissions;
import ftn.uns.ac.rs.foodmateapp.commons.TimePickerFragment;
import ftn.uns.ac.rs.foodmateapp.commons.ValidationData;
import ftn.uns.ac.rs.foodmateapp.dto.ReservationDTO;
import ftn.uns.ac.rs.foodmateapp.dto.ResponseMessageDTO;
import ftn.uns.ac.rs.foodmateapp.dto.UserDataDTO;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.TravelMode;
import com.r0adkll.slidr.Slidr;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.parceler.Parcels;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import com.google.maps.android.PolyUtil;

public class ReservationActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnTouchListener, AsyncResponse {

    EditText date;
    EditText time;
    NumberPicker numberPicker = null;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    private GoogleMap mMap;
    LatLng origin;
    LatLng destination;
    private static final int overview = 0;
    ArrayList<ArrayList<String>> contact_invite = new ArrayList<ArrayList<String>>();
    Permissions permissions = new Permissions(this);
    String fullName;
    String username;
    String dateReserve;
    String timeReserve;
    Restaurant restaurant = new Restaurant();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        permissions.checkIfLogin();
        setActionBar();
        checkPermission();
        setUpMap();
        setDate();
        setTime();
        setNumberPicker();
        setData();
        Slidr.attach(this);
        getLoginUser();
    }

    public void setActionBar(){
        setTitle("Make reservation");
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public void checkPermission(){
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!permissions.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION) ||
                    !permissions.checkPermission(Manifest.permission.READ_CONTACTS) || !
                    permissions.checkPermission(Manifest.permission.SEND_SMS)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_CONTACTS,
                                Manifest.permission.SEND_SMS}, 123);
            }
        }
    }

    public void setUpMap(){
        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager()
                        .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    public void setDate(){
        date = findViewById(R.id.date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });
    }

    public void setTime(){
        time = findViewById(R.id.time);
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "time picker");
            }
        });
    }

    public void setNumberPicker(){
        numberPicker = findViewById(R.id.number_picker);
        numberPicker.setMaxValue(100);
        numberPicker.setMinValue(1);
        numberPicker.setWrapSelectorWheel(false);
    }

    public void setData(){
        TextView textView = findViewById(R.id.invite_friends);
        textView.setText(getString(R.string.invite_friend));

        restaurant = Parcels.unwrap(getIntent().getParcelableExtra(Constants.CHOSEN_RESTAURANT));
        TextView restaurantNameView = findViewById(R.id.restaurant_name);
        restaurantNameView.setText(restaurant.getName());

        TextView addressView = findViewById(R.id.address);
        addressView.setText(restaurant.getLocation().getAddress() +", " + restaurant.getLocation().getCity());

        destination = new LatLng(restaurant.getLocation().getLatitude(), restaurant.getLocation().getLongitude());

        TextView friendsView = findViewById(R.id.friends);
        friendsView.setVisibility(View.INVISIBLE);
    }

    public void getLoginUser(){
        final String url = getString(R.string.get_user_url);
        SharedRef sharedRef = new SharedRef(this);
        String token = sharedRef.getDataString("token",null);
        String username = sharedRef.getDataString("username", null);
        GenericAsyncTask<String, UserDataDTO> asyncTask = new GenericAsyncTask<>(this, UserDataDTO.class, this);
        asyncTask.setObjectToSend(username);
        asyncTask.execute(url,token);
    }

    @Override
    public void onTaskCompleted(Object result) {
        if(result instanceof UserDataDTO) {
            UserDataDTO user = (UserDataDTO) result;
            fullName = user.getFirstName() + " " + user.getLastName();
            username = user.getUsername();
        }else if(result instanceof ResponseMessageDTO){
            progressDialog.dismiss();
            ResponseMessageDTO messageDTO = (ResponseMessageDTO) result;
            if(messageDTO.getMessage().equals("Reservation successfully made!")){
                if(contact_invite.size()!=0){
                    sendMessageInvite(timeReserve, dateReserve);
                }
                Intent intent = new Intent(this, SuggestionsActivity.class);
                startActivity(intent);
                Toast.makeText(this,messageDTO.getMessage(),Toast.LENGTH_LONG).show();
            }else{
                ErrorAlert.showErrorDialog(this, "Message", messageDTO.getMessage());
            }
        }else if(result == null){
            if(progressDialog !=null){
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String currentDateString = DateFormat.getDateInstance().format(c.getTime());
        date.setText(currentDateString);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String hours = String.valueOf(hourOfDay);
        String minutes = String.valueOf(minute);

        if(String.valueOf(hourOfDay).length() == 1){
            hours = "0" + hourOfDay;
        }
        if(String.valueOf(minute).length() == 1){
            minutes = "0" + minute;
        }

        String currentTimeString = hours + ":" + minutes;
        time.setText(currentTimeString);
    }

    public void inviteFriends(View view) {
        TextView textView = findViewById(R.id.invite_friends);
        String text = textView.getText().toString();
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, text.length(),0);
        textView.setText(content);

        Intent intent = new Intent(ReservationActivity.this, InviteFriendsActivity.class);
        intent.putExtra("contact_invite", Parcels.wrap(contact_invite));
        startActivityForResult(intent, 123);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 123) {
            if(resultCode == RESULT_OK) {
                contact_invite = Parcels.unwrap(data.getParcelableExtra("contact_invite"));
                populateListInvite();
            }
        }
    }

    public void populateListInvite(){
        ListView simpleList = findViewById(R.id.listView);
        CustomAdapter customAdapter = new CustomAdapter(this,contact_invite);
        simpleList.setAdapter(customAdapter);
        setListViewHeightBasedOnChildren(simpleList);
        TextView friendsView = findViewById(R.id.friends);
        friendsView.setVisibility(View.VISIBLE);

    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public void reservation(View view) throws ParseException {

        EditText dateBox = findViewById(R.id.date);
        dateReserve = dateBox.getText().toString();

        if(new ValidationData().checkInput(dateReserve)){
            ErrorAlert.showErrorDialog(this,"Message", getString(R.string.error_date));
            return;
        }else if(new ValidationData().checkDate(dateReserve)){
            ErrorAlert.showErrorDialog(this,"Message", getString(R.string.error_date_before));
            return;
        }

        EditText timeBox = findViewById(R.id.time);
        timeReserve = timeBox.getText().toString();
        if(new ValidationData().checkInput(timeReserve)){
            ErrorAlert.showErrorDialog(this,"Message", getString(R.string.error_time));
            return;
        }else if(new ValidationData().checkTime(dateReserve, timeReserve)){
            ErrorAlert.showErrorDialog(this,"Message", getString(R.string.error_time_before));
            return;
        }

        progressDialog = ProgressDialog.show(this,"Reservation in progress","Please wait...",
                false,true);

        NumberPicker numberPicker = findViewById(R.id.number_picker);
        int number_persons = numberPicker.getValue();

        ReservationDTO reservationDTO = new ReservationDTO();
        reservationDTO.setDate(dateReserve);
        reservationDTO.setNumber_persons(number_persons);
        reservationDTO.setRestaurant_id(restaurant.getRestaurantId());
        reservationDTO.setTime(timeReserve);
        reservationDTO.setUsername(username);

        final String uri = getString(R.string.reserve_url);
        GenericAsyncTask<ReservationDTO, ResponseMessageDTO> asyncTask =
                new GenericAsyncTask<>(this, ResponseMessageDTO.class, this);
        asyncTask.setObjectToSend(reservationDTO);
        asyncTask.execute(uri, new SharedRef(this).getDataString("token",null));

    }

    public void sendMessageInvite(String time, String date){

        for (ArrayList item: contact_invite) {
            sendMessage(time,date,item.get(1).toString());
        }
        Toast.makeText(this, getString(R.string.invitations_sent), Toast.LENGTH_LONG).show();

    }

    public void sendMessage(String time, String date, String number){
        if (permissions.checkPermission(Manifest.permission.SEND_SMS)) {
            try {
                SmsManager smsManager = SmsManager.getDefault();

                ArrayList<String> moreLines = new ArrayList<>();
                moreLines.add("You've been invited to " + getTypeOfDate(time) + "!\n");
                moreLines.add(fullName + " invited you to go to restaurant " + restaurant.getName() + "\n");
                moreLines.add(" on " + date + " at " + time + ".\n");
                moreLines.add("Have a great time!\n");
                moreLines.add("FoodMate Team");

                smsManager.sendMultipartTextMessage(number, null, moreLines, null, null);

            } catch (Exception e) {
                Toast.makeText(this, getString(R.string.sms_fail), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public String getTypeOfDate(String time){

        LocalTime local = new LocalTime(time);
        int hours = local.getHourOfDay();

        if(hours>=1 && hours<=12){
            return "breakfast";
        }else if(hours>=12 && hours<=18){
            return "lunch";
        }else if(hours>=18 && hours<=24){
            return "dinner";
        }
        return null;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            FusedLocationProviderClient client =
                    LocationServices.getFusedLocationProviderClient(this);

            client.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            origin = new LatLng(location.getLatitude(),location.getLongitude());
                            Location lo = new Location("");
                            lo.setLatitude(destination.latitude);
                            lo.setLongitude(destination.longitude);
                            onLocationChanged(lo);
                            if (location != null) {
                                // Logic to handle location object
                            }
                        }
                    });
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
//Showing Current Location Marker on Map
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(new Criteria(), true);
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location locations = locationManager.getLastKnownLocation(provider);
        List<String> providerList = locationManager.getAllProviders();
        if (null != locations && null != providerList && providerList.size() > 0) {
            double longitude = locations.getLongitude();
            double latitude = locations.getLatitude();
            Geocoder geocoder = new Geocoder(getApplicationContext(),
                    Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(latitude,
                        longitude, 1);
                if (null != listAddresses && listAddresses.size() > 0) {
                    String state = listAddresses.get(0).getAdminArea();
                    String country = listAddresses.get(0).getCountryName();
                    String subLocality = listAddresses.get(0).getSubLocality();
                    markerOptions.title("" + latLng + "," + subLocality + "," + state
                            + "," + country);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mCurrLocationMarker = mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        ErrorAlert.showErrorDialog(this,"Warning","Please check your internet connection!");
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    public void showPath(View view) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setContentView(R.layout.dialogmap);

        MapView mMapView = dialog.findViewById(R.id.mapView);
        MapsInitializer.initialize(this);
        mMapView.onCreate(dialog.onSaveInstanceState());
        mMapView.onResume();

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
                setupGoogleMapScreenSettings(googleMap);
                DirectionsResult results = getDirectionsDetails(origin,destination, TravelMode.DRIVING);
                if (results != null) {
                    addPolyline(results, googleMap);
                    positionCamera(results.routes[overview], googleMap);
                    addMarkersToMap(results, googleMap);
                }

            }
        });

        Button dialogButton = dialog.findViewById(R.id.btn_back);
// if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private DirectionsResult getDirectionsDetails(LatLng origin,LatLng destination,TravelMode mode) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext())
                    .mode(mode)
                    .origin(new com.google.maps.model.LatLng(origin.latitude, origin.longitude))
                    .destination(new com.google.maps.model.LatLng(destination.latitude, destination.longitude))
                    .departureTime(now)
                    .await();

        } catch (ApiException e) {
            e.printStackTrace();
            return null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void setupGoogleMapScreenSettings(GoogleMap mMap) {
        UiSettings mUiSettings = mMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(true);
        mUiSettings.setCompassEnabled(true);
        mUiSettings.setMyLocationButtonEnabled(true);
        mUiSettings.setScrollGesturesEnabled(true);
        mUiSettings.setZoomGesturesEnabled(true);
        mUiSettings.setTiltGesturesEnabled(true);
        mUiSettings.setRotateGesturesEnabled(true);
    }

    private void addMarkersToMap(DirectionsResult results, GoogleMap mMap) {
        mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[overview].legs[overview]
                .startLocation.lat,results.routes[overview].legs[overview].startLocation.lng)).title(results.routes[overview]
                .legs[overview].startAddress));
        mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[overview].legs[overview]
                .endLocation.lat,results.routes[overview].legs[overview].endLocation.lng)).title(results.routes[overview]
                .legs[overview].startAddress)
                .snippet(getEndLocationTitle(results)));
    }

    private void positionCamera(DirectionsRoute route, GoogleMap mMap) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(route.legs[overview].startLocation.lat,
                route.legs[overview].startLocation.lng), 15));
    }

    private void addPolyline(DirectionsResult results, GoogleMap mMap) {
        List<LatLng> decodedPath = PolyUtil.decode(results.routes[overview].overviewPolyline.getEncodedPath());
        mMap.addPolyline(new PolylineOptions().addAll(decodedPath)).setColor(Color.BLUE);
    }

    private String getEndLocationTitle(DirectionsResult results){
        return  "Time :"+ results.routes[overview].legs[overview].duration.humanReadable + " Distance :" +
                results.routes[overview].legs[overview].distance.humanReadable;
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext
                .setQueryRateLimit(3)
                .setApiKey(getString(R.string.google_maps_key))
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
    }

    public void showRestaurant(View view) {
        Intent intent = new Intent(getApplicationContext(), RestaurantInfoActivity.class);
        intent.putExtra(Constants.CHOSEN_RESTAURANT, Parcels.wrap(restaurant));
        startActivity(intent);
    }
}
