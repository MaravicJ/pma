package ftn.uns.ac.rs.foodmateapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class OtherImageDTO {
    private long id;
    private String image;
    private String path;
    private String cachedPath;
}
