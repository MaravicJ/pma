package ftn.uns.ac.rs.foodmateapp.models.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.entities.ChosenRestaurant;

@Dao
public interface ChosenRestaurantDao {

    @Transaction
    @Query("SELECT * FROM ChosenRestaurant where :userId = userId")
    LiveData<List<ChosenRestaurant>> getChosenRestaurantsByUserId(long userId);


    @Query("SELECT restaurantId FROM ChosenRestaurant " +
            "INNER JOIN User on ChosenRestaurant.userId=User.userId " +
            "WHERE :username = username")
    List<Long> getChosenRestaurantIds(String username);

    @Transaction
    @Query("SELECT * FROM ChosenRestaurant where :id = restaurantId")
    LiveData<ChosenRestaurant> getChosenRestaurantById(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ChosenRestaurant... chosenRestaurants);

    @Delete
    void delete(ChosenRestaurant chosenRestaurants);
}
