package ftn.uns.ac.rs.foodmateapp.models.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import org.jetbrains.annotations.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category {

    @PrimaryKey
    @NotNull
    private String categoryCode;
    private String name;
}
