package ftn.uns.ac.rs.foodmateapp.dto.sync;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SyncRequestDTO {
    private String city;
    private Date lastUpdate;
}
