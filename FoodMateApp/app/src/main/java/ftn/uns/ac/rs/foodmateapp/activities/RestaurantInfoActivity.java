package ftn.uns.ac.rs.foodmateapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.thefuntasty.hauler.HaulerView;

import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.adapters.SliderAdapter;
import ftn.uns.ac.rs.foodmateapp.asynctasks.AsyncResponse;
import ftn.uns.ac.rs.foodmateapp.asynctasks.GenericAsyncTask;
import ftn.uns.ac.rs.foodmateapp.asynctasks.LoadRestaurantOtherImages;
import ftn.uns.ac.rs.foodmateapp.commons.Constants;
import ftn.uns.ac.rs.foodmateapp.commons.ImageAction;
import ftn.uns.ac.rs.foodmateapp.commons.OnSwipeTouchListener;
import ftn.uns.ac.rs.foodmateapp.dto.OtherImageDTO;
import ftn.uns.ac.rs.foodmateapp.dto.RatingDTO;
import ftn.uns.ac.rs.foodmateapp.dto.RestaurantRatingDTO;
import ftn.uns.ac.rs.foodmateapp.fragments.WorkingHoursFragment;
import ftn.uns.ac.rs.foodmateapp.models.SliderItem;
import ftn.uns.ac.rs.foodmateapp.models.entities.Image;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.RestaurantFoodPreference;
import ftn.uns.ac.rs.foodmateapp.sync.Authenticator;
import ftn.uns.ac.rs.foodmateapp.viewmodel.ImageViewModel;
import ftn.uns.ac.rs.foodmateapp.viewmodel.RestaurantViewModel;
import ftn.uns.ac.rs.foodmateapp.viewmodel.WorkingDayViewModel;

import org.parceler.Parcels;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class RestaurantInfoActivity extends AppCompatActivity implements AsyncResponse {
    private SharedRef sharedRef;
    private Restaurant restaurant;

    private ViewPager2 viewPager2;
    private Handler sliderHandler = new Handler();

    private WorkingDayViewModel workingDayViewModel;
    private RestaurantViewModel restaurantViewModel;
    private ImageViewModel imageViewModel;

    private ImageView menuImageView;
    private LinearLayout mainLayout;
    private ProgressDialog progressDialog;

    private Account mAccount;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_info);

        mAccount = Authenticator.CreateSyncAccount(getApplicationContext());

        sharedRef = new SharedRef(this);
        viewPager2 = findViewById(R.id.viewPagerImageSlider);

        restaurantViewModel = ViewModelProviders.of(this).get(RestaurantViewModel.class);
        workingDayViewModel = ViewModelProviders.of(this).get(WorkingDayViewModel.class);
        imageViewModel = ViewModelProviders.of(this).get(ImageViewModel.class);

        restaurant = Parcels.unwrap(getIntent().getExtras().getParcelable(Constants.CHOSEN_RESTAURANT));

        menuImageView = findViewById(R.id.menuImageView);
        mainLayout = findViewById(R.id.mainLayout);

        setupImagesObserver();
        setRestaurantInfo();
        setRatingBarListener();
        setWorkingDayButton();
        getRating();

        dismissOnDragDown();

        fadeOutAndHideImage();
        setUpSwipeNavigation();

    }

    private void getRating() {
        final String url = getString(R.string.get_rating);

        GenericAsyncTask<String, Double> asyncTask = new GenericAsyncTask<>(this,
                Double.class, this);
        asyncTask.setObjectToSend(restaurant.getName());
        asyncTask.execute(url, sharedRef.getDataString("token", null));
    }

     private void setupImagesObserver() {
        if (imageViewModel.getLiveDataImages(restaurant.getRestaurantId()).hasObservers()) {
            return;
        }
        imageViewModel.getLiveDataImages(restaurant.getRestaurantId()).observe(this, images -> {
           getRestaurantOtherImages(images);
        });
    }

    private void getRestaurantOtherImages(List<Image> images) {
        if(!areRestaurantOtherImagesReadFromCache(images)) {
            LoadRestaurantOtherImages loadRestaurantOtherImages = new LoadRestaurantOtherImages(images,this, this);
            loadRestaurantOtherImages.setRestaurantId(restaurant.getRestaurantId());
            final String url = getApplicationContext().getString(R.string.get_other_images);
            loadRestaurantOtherImages.execute(url);
        }else{
            progressDialog.dismiss();

        }
    }

    private void openDialog() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        WorkingHoursFragment newFragment = new WorkingHoursFragment();
        workingDayViewModel.getWorkingDayByRestaurantId(restaurant.getRestaurantId()).observe(this, workingDays -> {
            newFragment.setData(workingDays);
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
        });
    }

    public void setWorkingDayButton() {
        MaterialButton button = findViewById(R.id.button);
        button.setOnClickListener(v -> openDialog());
    }

    private Runnable sliderRunnable = new Runnable() {
        @Override
        public void run() {
            viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1);
        }
    };

    @Override
    protected void onPause() {
        super.onPause();

        sliderHandler.removeCallbacks(sliderRunnable);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sliderHandler.postDelayed(sliderRunnable, 3000);
    }

    private void setRatingBarListener() {
        RatingBar ratingBar = findViewById(R.id.ratingBar);
        ratingBar.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                float touchPositionX = event.getX();
                float width = ratingBar.getWidth();
                float starsf = (touchPositionX / width) * 5.0f;
                int stars = (int)starsf + 1;
                ratingBar.setRating(stars);
                addRating(stars);
                v.setPressed(false);
            }
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                v.setPressed(true);
            }

            if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                v.setPressed(false);
            }
            return true;
        });
    }

    private void addRating(float rating) {
        final String url = getString(R.string.add_rating);

        GenericAsyncTask<RatingDTO, Double> asyncTask = new GenericAsyncTask<>(this, double.class, this);
        asyncTask.setObjectToSend(new RatingDTO(sharedRef.getDataString("username", null), restaurant.getName(), rating));
        asyncTask.execute(url, sharedRef.getDataString("token", null));

        //restaurant.setRating(rating);
        //restaurantViewModel.insert(restaurant);
    }

    private String getFoodPreferencesString(List<RestaurantFoodPreference> foodPreferences) {
        String foodPreferencesString = "";
        for(RestaurantFoodPreference fp: foodPreferences) {
            foodPreferencesString = foodPreferencesString + fp.getFoodPreference() + ", ";
        }

        String foodPreferenceSubString = foodPreferencesString.substring(0, foodPreferencesString.length() - 2);
        return foodPreferenceSubString;
    }

    private void setRestaurantInfo() {
        TextView restaurantName = findViewById(R.id.restaurantName);
        restaurantName.setText(restaurant.getName());
        TextView location = findViewById(R.id.location);
        location.setText(restaurant.getLocation().getAddress() + ", " + restaurant.getLocation().getCity() + ", " +
                restaurant.getLocation().getCountry());
        TextView phone = findViewById(R.id.phone);
        phone.setText(restaurant.getPhoneNumber());
        TextView description = findViewById(R.id.description);
        description.setText(restaurant.getDescription());
        RatingBar ratingBar = findViewById(R.id.ratingBar);
        TextView rating = findViewById(R.id.rating);

        TextView foodType = findViewById(R.id.foodType);
        restaurantViewModel.getRestaurantFoodPreferences(restaurant.getRestaurantId()).observe(this, restaurantFoodPreferences -> {
                    foodType.setText(getFoodPreferencesString(restaurantFoodPreferences));
                    ratingBar.setRating((float) restaurant.getRating());
                    rating.setText(String.valueOf(restaurant.getRating()));
                }
        );
    }

    public void reservation(View view) {
        Intent intent = new Intent(this, ReservationActivity.class);
        intent.putExtra(Constants.CHOSEN_RESTAURANT,  Parcels.wrap(restaurant));
        startActivity(intent);
    }

    private void dismissOnDragDown() {
        HaulerView haulerView = findViewById(R.id.haulerView);
        haulerView.setOnDragDismissedListener(dragDirection -> finish());
    }

    @Override
    public void onTaskCompleted(Object result) {
        if(result instanceof List) {
            List<OtherImageDTO> otherImageDTOs = (List<OtherImageDTO>) result;

            List<Bitmap> bitmaps = new ArrayList<>();
            if(otherImageDTOs.isEmpty()) {
                Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.loading);
                bitmaps.add(image);
                bitmaps.add(image);
                bitmaps.add(image);
            } else {
                bitmaps = convertStringImagesToBitmap(otherImageDTOs);
            }

            setRestaurantSlider(bitmaps);
            if(progressDialog != null){
                progressDialog.dismiss();
            }

        } else if (result instanceof Double) {
            Double returnedRating = (double) result;
            //restaurant.setRating(returnedRating);
            RatingBar ratingBar = findViewById(R.id.ratingBar);
            TextView rating = findViewById(R.id.rating);

            Double doubleValue = Double.parseDouble((String) rating.getText());
            if(returnedRating.doubleValue() == doubleValue.doubleValue()) {
                return;
            }

            ratingBar.setRating(returnedRating.shortValue());
            rating.setText(String.valueOf(returnedRating.shortValue()));
        }else if(result == null){
            if(progressDialog != null){
                progressDialog.dismiss();
            }
        }
    }

    private List<Bitmap> convertStringImagesToBitmap(List<OtherImageDTO> otherImageDTOs) {
        List<Bitmap> bitmaps = new ArrayList<>();
        for(OtherImageDTO item: otherImageDTOs) {
            Bitmap image = ImageAction.stringToBitMap(item.getImage());
            bitmaps.add(image);
        }
        return bitmaps;
    }

    private boolean areRestaurantOtherImagesReadFromCache(List<Image> images) {
        List<Bitmap> bitmaps = new ArrayList<>();
        for(Image item: images) {
            String path =  "restaurant_" + restaurant.getRestaurantId() + "_image_" + item.getImageId();
            Bitmap image = ImageAction.loadFromCache(path, this);
            if(image == null) {
                return false;
            }
            bitmaps.add(image);
        }
        setRestaurantSlider(bitmaps);
        return true;
    }

    private void setRestaurantSlider(List<Bitmap> images) {
        List<SliderItem> sliderItems = new ArrayList<>();
        for(Bitmap i: images) {
            sliderItems.add(new SliderItem(i));
        }

        viewPager2.setAdapter(new SliderAdapter(sliderItems, viewPager2));
        viewPager2.setClipToPadding(false);
        viewPager2.setClipChildren(false);
        viewPager2.setOffscreenPageLimit(3);
        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(40));
        compositePageTransformer.addTransformer((page, position) -> {
            float r = 1 - Math.abs(position);
            page.setScaleY(0.85f + r * 0.15f);
        });

        viewPager2.setPageTransformer(compositePageTransformer);
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                sliderHandler.removeCallbacks(sliderRunnable);
                sliderHandler.postDelayed(sliderRunnable, 3000);
            }
        });
    }

    private void fadeOutAndHideImage() {
        Boolean seen = sharedRef.getDataBoolean(Constants.MENU_ANIMATION, false);
        if(seen) {
            menuImageView.setVisibility(View.GONE);
            mainLayout.setVisibility(View.VISIBLE);
            progressDialog = ProgressDialog.show(this,"Loading","Please wait...",
                    false,false);
            return;
        }

        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(2000);

        fadeOut.setAnimationListener(new Animation.AnimationListener()
        {
            public void onAnimationEnd(Animation animation)
            {
                menuImageView.setVisibility(View.GONE);
                mainLayout.setVisibility(View.VISIBLE);
                sharedRef.saveDataBoolean(Constants.MENU_ANIMATION, true);
                progressDialog = ProgressDialog.show(RestaurantInfoActivity.this,"Loading","Please wait...",
                        false,false);

            }
            public void onAnimationRepeat(Animation animation) {}
            public void onAnimationStart(Animation animation) {}
        });

        menuImageView.startAnimation(fadeOut);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    public void closeRestaurantInfoView(View view) {
        finish();
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    public void setUpSwipeNavigation(){
        NestedScrollView view = findViewById(R.id.nested_scroll_view);
        view.setOnTouchListener(new OnSwipeTouchListener(this){
            public void onSwipeRight() {
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                intent.putExtra(Constants.CHOSEN_RESTAURANT, Parcels.wrap(restaurant));
                Bundle bndlAnimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.left_to_right,0).toBundle();
                startActivity(intent,bndlAnimation);
            }
            public void onSwipeDown(){

            }

        });
    }
/*
    @Override
    public void onDestroy() {
        super.onDestroy();
        //runSync();
    }
*/
    private void runSync() {
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        //settingsBundle.putBoolean(
        //        ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(mAccount, Authenticator.AUTHORITY, settingsBundle);
    }

}