package ftn.uns.ac.rs.foodmateapp.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.r0adkll.slidr.Slidr;

import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.asynctasks.AsyncResponse;
import ftn.uns.ac.rs.foodmateapp.asynctasks.GenericAsyncTask;
import ftn.uns.ac.rs.foodmateapp.dto.UserDataDTO;

public class EditProfileActivity extends AppCompatActivity implements AsyncResponse {
    private SharedRef sharedRef;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        sharedRef = new SharedRef(this);

        setTitle("Edit profile");
        setData();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Slidr.attach(this);
    }

    private void setData() {
        final String url = getString(R.string.get_user_url);

        GenericAsyncTask<String, UserDataDTO> asyncTask = new GenericAsyncTask<>(this, UserDataDTO.class, this);
        asyncTask.setObjectToSend(sharedRef.getDataString("username", "null"));
        asyncTask.execute(url, sharedRef.getDataString("token", null));
    }

    public void editProfile(View v) {
        progressDialog = ProgressDialog.show(this,"Saving data","Please wait...",
                false,false);
        UserDataDTO userDataDTO = getEditUserDTO();
        final String url = getString(R.string.edit_profile_url);

        GenericAsyncTask<UserDataDTO, String> asyncTask = new GenericAsyncTask<>(this, String.class, this);
        asyncTask.setObjectToSend(userDataDTO);
        SharedRef sharedRef = new SharedRef(EditProfileActivity.this);
        String token = sharedRef.getDataString("token",null);
        asyncTask.execute(url, token);
    }

    private UserDataDTO getEditUserDTO() {
        EditText usernameInput = findViewById(R.id.username);
        String username = usernameInput.getText().toString();
        EditText passwordInput = findViewById(R.id.password);
        String password = passwordInput.getText().toString();
        EditText emailInput = findViewById(R.id.email);
        String email = emailInput.getText().toString();
        EditText phoneInput = findViewById(R.id.phone);
        String phoneNumber = phoneInput.getText().toString();
        EditText firstNameInput = findViewById(R.id.firstName);
        String firstName = firstNameInput.getText().toString();
        EditText lastNameInput = findViewById(R.id.lastName);
        String lastName = lastNameInput.getText().toString();
        return new UserDataDTO(username, password, firstName, lastName, phoneNumber, email);
    }

    @Override
    public void onTaskCompleted(Object result) {
        System.out.println("Result " + result);

        if(result instanceof UserDataDTO) {
            EditText usernameInput = findViewById(R.id.username);
            usernameInput.setText(((UserDataDTO) result).getUsername());
            EditText emailInput = findViewById(R.id.email);
            emailInput.setText(((UserDataDTO) result).getEmail());
            EditText phoneInput = findViewById(R.id.phone);
            phoneInput.setText(((UserDataDTO) result).getPhoneNumber());
            EditText firstNameInput = findViewById(R.id.firstName);
            firstNameInput.setText(((UserDataDTO) result).getFirstName());
            EditText lastNameInput = findViewById(R.id.lastName);
            lastNameInput.setText(((UserDataDTO) result).getLastName());
        } else {
            if(progressDialog != null){
                progressDialog.dismiss();
            }
            Toast.makeText(this, String.valueOf(result) , Toast.LENGTH_SHORT).show();
        }

    }
}
