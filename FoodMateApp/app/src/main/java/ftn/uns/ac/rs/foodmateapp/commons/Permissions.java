package ftn.uns.ac.rs.foodmateapp.commons;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.activities.LoginActivity;

public class Permissions {

    Context context;

    public Permissions(Context context){
        this.context = context;
    }

    public boolean checkPermission(String permission){
        int check = ContextCompat.checkSelfPermission(context,permission);
        return (check == PackageManager.PERMISSION_GRANTED);
    }

    public void checkIfLogin(){
        SharedRef sharedRef = new SharedRef(context);
        String check_login = sharedRef.getDataString("login", null);
        String check_token = sharedRef.getDataString("token", null);

        if(check_login.equals("false")|| check_login == null || check_token.equals("") || check_token == null){
            Intent intent = new Intent(context, LoginActivity.class);
            context.startActivity(intent);
            Toast.makeText(context,context.getString(R.string.error_log_in),Toast.LENGTH_LONG).show();
        }
    }

}
