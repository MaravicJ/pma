package ftn.uns.ac.rs.foodmateapp.dto.sync;

import ftn.uns.ac.rs.foodmateapp.models.entities.MenuItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuItemSyncDTO {
    private long menuItemId;
    private String name;
    private String description;
    private double cost;
    private long menuItemRestaurantId;
    private String menuItemCategoryCode;
    private long menuItemImageId;

    public MenuItem makeMenuItem() {
        MenuItem menuItem = new MenuItem();
        menuItem.setMenuItemId(menuItemId);
        menuItem.setName(name);
        menuItem.setDescription(description);
        menuItem.setCost(cost);
        menuItem.setMenuItemRestaurantId(menuItemRestaurantId);
        menuItem.setMenuItemCategoryCode(menuItemCategoryCode);
        menuItem.setMenuItemImageId(menuItemImageId);
        return menuItem;
    }
}
