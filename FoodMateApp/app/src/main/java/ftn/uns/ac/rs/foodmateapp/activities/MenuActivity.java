package ftn.uns.ac.rs.foodmateapp.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.r0adkll.slidr.Slidr;
import org.parceler.Parcels;
import java.util.List;
import java.util.stream.Collectors;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.asynctasks.AsyncResponse;
import ftn.uns.ac.rs.foodmateapp.asynctasks.LoadMenuItemImage;
import ftn.uns.ac.rs.foodmateapp.commons.Constants;
import ftn.uns.ac.rs.foodmateapp.commons.ErrorAlert;
import ftn.uns.ac.rs.foodmateapp.commons.MenuAdapter;
import ftn.uns.ac.rs.foodmateapp.commons.Permissions;
import ftn.uns.ac.rs.foodmateapp.dto.MenuItemDTO;
import ftn.uns.ac.rs.foodmateapp.models.entities.MenuItem;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.viewmodel.ImageViewModel;
import ftn.uns.ac.rs.foodmateapp.viewmodel.MenuItemViewModel;

public class MenuActivity extends AppCompatActivity implements AsyncResponse {

    Permissions permissions = new Permissions(this);
    Restaurant restaurant;
    MenuItemViewModel menuItemViewModel;
    ImageViewModel imageViewModel;
    private ProgressDialog progressDialog;
    TextView breakfast;
    TextView lunch;
    TextView dessert;
    TextView drinks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        permissions.checkIfLogin();
        setLoading();
        setActionBar();
        setData();
        Slidr.attach(this);

    }

    public void setLoading(){
        breakfast = findViewById(R.id.breakfast_title);
        breakfast.setVisibility(View.GONE);
        lunch = findViewById(R.id.lunch_title);
        lunch.setVisibility(View.GONE);
        dessert = findViewById(R.id.dessert_title);
        dessert.setVisibility(View.GONE);
        drinks = findViewById(R.id.drink_title);
        drinks.setVisibility(View.GONE);
        progressDialog = ProgressDialog.show(this,"Loading menu","Please wait...",false,false);

    }
    public void setActionBar(){
        setTitle("Menu");
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public void setData(){
        menuItemViewModel = ViewModelProviders.of(this).get(MenuItemViewModel.class);
        imageViewModel = ViewModelProviders.of(this).get(ImageViewModel.class);
        restaurant = Parcels.unwrap(getIntent().getParcelableExtra(Constants.CHOSEN_RESTAURANT));
        menuItemViewModel.getMenuItems(restaurant.getRestaurantId()).observe(this, new Observer<List<MenuItem>>() {
            @Override
            public void onChanged(List<MenuItem> menuItems) {
                LoadMenuItemImage loadMenuItemImage = new LoadMenuItemImage(menuItems,MenuActivity.this, MenuActivity.this);
                final String url = getApplicationContext().getString(R.string.get_menu_item_image_url);
                loadMenuItemImage.execute(url);
            }
        });
    }

    @Override
    public void onTaskCompleted(Object result) {
        List<MenuItemDTO> menuItemDTO = (List<MenuItemDTO>) result;
        if(menuItemDTO.isEmpty()){
            progressDialog.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Message");
            builder.setMessage("Please check your internet connection!");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    onBackPressed();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();

        }else{
            List<MenuItemDTO> breakfast_list = menuItemDTO.stream()
                    .filter(p -> p.getMenuItem().getMenuItemCategoryCode().equals("breakfast")).collect(Collectors.toList());

            List<MenuItemDTO> lunch_list =  menuItemDTO.stream()
                    .filter(p -> p.getMenuItem().getMenuItemCategoryCode().equals("main_course")).collect(Collectors.toList());

            List<MenuItemDTO> dessert_list =  menuItemDTO.stream()
                    .filter(p -> p.getMenuItem().getMenuItemCategoryCode().equals("desert")).collect(Collectors.toList());

            List<MenuItemDTO> drink_list =  menuItemDTO.stream()
                    .filter(p -> p.getMenuItem().getMenuItemCategoryCode().equals("drink")).collect(Collectors.toList());

            RecyclerView rvContacts = findViewById(R.id.rvContacts);
            MenuAdapter menuAdapter = new MenuAdapter(breakfast_list);
            rvContacts.setAdapter(menuAdapter);
            rvContacts.setLayoutManager(new LinearLayoutManager(MenuActivity.this));

            RecyclerView lunchContacts = findViewById(R.id.lunchList);
            MenuAdapter menuAdapterLunch = new MenuAdapter(lunch_list);
            lunchContacts.setAdapter(menuAdapterLunch );
            lunchContacts.setLayoutManager(new LinearLayoutManager(MenuActivity.this));

            RecyclerView dessertContacts = findViewById(R.id.dessertList);
            MenuAdapter menuAdapterDessert = new MenuAdapter(dessert_list);
            dessertContacts.setAdapter(menuAdapterDessert);
            dessertContacts.setLayoutManager(new LinearLayoutManager(MenuActivity.this));

            RecyclerView drinkContacts = findViewById(R.id.drinkList);
            MenuAdapter menuAdapterDrink = new MenuAdapter(drink_list);
            drinkContacts.setAdapter(menuAdapterDrink);
            drinkContacts.setLayoutManager(new LinearLayoutManager(MenuActivity.this));
            breakfast.setVisibility(View.VISIBLE);
            lunch.setVisibility(View.VISIBLE);
            dessert.setVisibility(View.VISIBLE);
            drinks.setVisibility(View.VISIBLE);
            progressDialog.dismiss();
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
