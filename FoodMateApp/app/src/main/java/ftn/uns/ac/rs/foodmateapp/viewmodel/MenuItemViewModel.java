package ftn.uns.ac.rs.foodmateapp.viewmodel;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import java.util.List;
import ftn.uns.ac.rs.foodmateapp.models.entities.MenuItem;
import ftn.uns.ac.rs.foodmateapp.models.repository.MenuItemRepository;

public class MenuItemViewModel extends AndroidViewModel {

    private MenuItemRepository menuItemRepository;

    public MenuItemViewModel(@NonNull Application application) {
        super(application);
        menuItemRepository = new MenuItemRepository(application);
    }

    public LiveData<List<MenuItem>> getMenuItems(long restaurant_id){
        return menuItemRepository.getMenuItems(restaurant_id);
    }
}
