package ftn.uns.ac.rs.foodmateapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.entities.Image;
import ftn.uns.ac.rs.foodmateapp.models.repository.ImageRepository;

public class ImageViewModel extends AndroidViewModel {
    private ImageRepository imageRepository;
    private LiveData<List<Image>> images = null;

    public ImageViewModel(@NonNull Application application) {
        super(application);
        imageRepository = new ImageRepository(application);
    }

    private void initImages(long restaurantId) {
        images = imageRepository.getImages(restaurantId);
    }

    public LiveData<List<Image>> getLiveDataImages(long restaurantId){
        if (images == null) {
            initImages(restaurantId);
        }
        return images;
    }

    public LiveData<Image> getImage(long imageId) {
        return imageRepository.getImage(imageId);
    }

    public void insert(Image image) {
        imageRepository.insert(image);
    }

}
