package ftn.uns.ac.rs.foodmateapp.activities;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.r0adkll.slidr.Slidr;

import java.util.HashSet;
import java.util.Set;

import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.models.entities.FoodPreference;

public class SettingsActivity extends AppCompatActivity {
    private SeekBar seekBar;
    private TextView distance_num;
    private SharedRef sharedRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setTitle("Settings");

        this.sharedRef = new SharedRef(this);
        distance_num = findViewById(R.id.distance_num);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        setDistance();
        setFoodPreferences();

        Slidr.attach(this);
    }

    private void setFoodPreferences() {
        Set<String> checkedValues = sharedRef.getDataStringSet("foodPreferences", getDefaultFoodPreferences());
        doCheckOnFoodPreferences(checkedValues);
    }

    public static Set<String> getDefaultFoodPreferences() {
        Set<String> defaultFoodPreferences = new HashSet<>();
        defaultFoodPreferences.add("regular");
        return defaultFoodPreferences;
    }

    private void doCheckOnFoodPreferences(Set<String> checkedValues) {
        LinearLayout layout = findViewById(R.id.foodPreferencesLayout);
        boolean checked = false;
        for(FoodPreference foodPreference: FoodPreference.values()) {
            for(String fd: checkedValues) {
                if(fd.toLowerCase().equals(foodPreference.getCode().toLowerCase())) {
                    checked = true;
                    break;
                }
            }

            CheckBox checkBox = new CheckBox(getApplicationContext());
            checkBox.setText(capitaliseFirstLetter(foodPreference.getCode()));
            int states[][] = {{android.R.attr.state_checked}, {}};
            int colors[] = {Color.parseColor("#FFC107"), Color.parseColor("#555555")};
            checkBox.setButtonTintList(new ColorStateList(states, colors));
            checkBox.setTextColor(Color.parseColor("#FFCCCCCC"));
            checkBox.setOnClickListener(v -> updateFoodPreferences());
            checkBox.setChecked(checked);
            layout.addView(checkBox);

            checked = false;
        }
    }

    private void updateFoodPreferences() {
        Set<String> foodPreferences = getCheckedValues();
        sharedRef.saveSetData("foodPreferences", foodPreferences);
        sharedRef.saveDataBoolean("needsRefresh", true);
    }

    private Set<String> getCheckedValues() {
        LinearLayout layout = findViewById(R.id.foodPreferencesLayout);
        Set<String> foodPreferences = new HashSet<>();

        for(int i=0; i<layout.getChildCount(); i++) {

            View nextChild = layout.getChildAt(i);
            if(nextChild instanceof CheckBox) {

                CheckBox check = (CheckBox) nextChild;
                if (check.isChecked()) {
                    foodPreferences.add(String.valueOf(check.getText()).toLowerCase());
                }
            }
        }
        return foodPreferences;
    }

    private void setDistance() {
        String distance = setSeekBar();
        distance_num.setText(distance + "km");
    }

    private String setSeekBar() {
        seekBar = findViewById(R.id.seekBar);
        seekBar.incrementProgressBy(2);
        seekBar.setMax(50);
        String distance = sharedRef.getDataString("distance", String.valueOf(20));
        seekBar.setProgress(Integer.parseInt(distance));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                distance_num.setText(progress + "km");
                sharedRef.saveDataString("distance", String.valueOf(progress));
                sharedRef.saveDataBoolean("needsRefresh", true);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        return distance;
    }

    static String capitaliseFirstLetter(String name){
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }
}

