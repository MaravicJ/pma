package ftn.uns.ac.rs.foodmateapp.models.entities;

import android.graphics.Bitmap;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Image {
    @PrimaryKey(autoGenerate = true)
    private long imageId;
    private String path;

    private long imageRestaurantId;

    private String cachedPath;

    @Ignore
    private Bitmap image;
}
