package ftn.uns.ac.rs.foodmateapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NewUserChosenRestaurantDTO {
    private String username;
    private String restaurantName;
}
