package ftn.uns.ac.rs.foodmateapp.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import com.r0adkll.slidr.Slidr;
import org.parceler.Parcels;
import java.util.ArrayList;
import java.util.Arrays;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.commons.CustomAdapter;
import ftn.uns.ac.rs.foodmateapp.commons.ErrorAlert;

public class InviteFriendsActivity extends AppCompatActivity {

    SearchView mySearchView;
    ArrayList<ArrayList<String>> phoneNumbers;
    ListView simpleList;
    CustomAdapter customAdapter;
    ArrayList<ArrayList<String>> clickedNumbers = new ArrayList<>();
    TextView numberOfSelected;
    String numberSelected;
    ArrayList<ArrayList<String>> contact_extra;
    boolean check_contact_clicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);

        getExtra();
        setUpActionBar();
        setUpList();
        setUpSearch();
        Slidr.attach(this);
    }

    public void getExtra(){
        contact_extra = Parcels.unwrap(getIntent().getParcelableExtra("contact_invite"));
        if(contact_extra.size()!= 0){
            check_contact_clicked = true;
        }
    }

    public void setUpActionBar(){
        setTitle("Invite friends");
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public void setUpList(){
        phoneNumbers = getPhoneNumbers();
        simpleList = findViewById(R.id.simpleListView);
        customAdapter = new CustomAdapter(this,phoneNumbers);
        simpleList.setAdapter(customAdapter);
        numberOfSelected = findViewById(R.id.number_of_selected);

        if(check_contact_clicked){
            for (ArrayList item: contact_extra) {
                customAdapter.setColorClicked(item.get(0).toString());
            }
            simpleList.setAdapter(customAdapter);
            numberSelected = String.valueOf(contact_extra.size());
            numberOfSelected.setText(numberSelected);
            clickedNumbers = contact_extra;
        }else{
            numberSelected = numberOfSelected.getText().toString();
        }

        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<String> number = customAdapter.getItem(position);
                customAdapter.setColorClicked(number.get(0));
                if(clickedNumbers.contains(number)){
                    clickedNumbers.remove(number);
                    numberSelected = String.valueOf(Integer.valueOf(numberSelected) - 1);
                    numberOfSelected.setText(numberSelected);
                }else{
                    clickedNumbers.add(number);
                    numberSelected = String.valueOf(Integer.valueOf(numberSelected) + 1);
                    numberOfSelected.setText(numberSelected);
                }
                simpleList.setAdapter(customAdapter);
            }
        });
    }

    public void setUpSearch(){
        mySearchView = findViewById(R.id.searchId);
        mySearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(!newText.equals("")){
                    customAdapter.setPhoneNumbers(phoneNumbers);
                    customAdapter.filter(newText);
                    simpleList.setAdapter(customAdapter);
                }else{
                    customAdapter.setPhoneNumbers(phoneNumbers);
                    simpleList.setAdapter(customAdapter);
                }
                return false;
            }
        });
    }

    private ArrayList<ArrayList<String>> getPhoneNumbers() {

        ArrayList<ArrayList<String>> namePhoneMap = new ArrayList<ArrayList<String>>();
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        while (phones.moveToNext()) {

            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            // Cleanup the phone number
            phoneNumber = phoneNumber.replaceAll("[()\\s-]+", "");
            // Enter Into Hash Map
            if(!namePhoneMap.contains(new ArrayList<>(Arrays.asList(name,phoneNumber)))){
                namePhoneMap.add(new ArrayList<>(Arrays.asList(name,phoneNumber)));
            }
        }
        phones.close();
        return namePhoneMap;
    }

    public void sendInvite(View view) {
        if(clickedNumbers.size() == 0){
            ErrorAlert.showErrorDialog(this,"Message",getString(R.string.message_error_invite));
        }else{
            Intent intent = new Intent();
            intent.putExtra("contact_invite", Parcels.wrap(clickedNumbers));
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
