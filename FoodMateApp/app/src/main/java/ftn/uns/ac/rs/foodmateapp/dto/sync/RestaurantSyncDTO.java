package ftn.uns.ac.rs.foodmateapp.dto.sync;

import ftn.uns.ac.rs.foodmateapp.models.entities.FoodPreference;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.RestaurantFoodPreference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantSyncDTO {
    private long restaurantId;
    private String name;
    private String description;
    private String email;
    private String phoneNumber;
    private float rating;
    private LocationDTO location;
    private long mainImageId;
    private List<String> foodPreferences;

    public Restaurant makeRestaurant() {
        Restaurant restaurant = new Restaurant();
        restaurant.setRestaurantId(restaurantId);
        restaurant.setName(name);
        restaurant.setEmail(email);
        restaurant.setPhoneNumber(phoneNumber);
        restaurant.setDescription(description);
        restaurant.setRating(rating);
        restaurant.setLocation(location.makeLocation());
        restaurant.setMainImageId(mainImageId);

        return restaurant;
    }

    public RestaurantFoodPreference[] getRestaurantFoodPreferences() {
        RestaurantFoodPreference[] restaurantFoodPreferences = new RestaurantFoodPreference[foodPreferences.size()];
        for(int i=0; i<foodPreferences.size(); i++) {
            restaurantFoodPreferences[i] =
                    new RestaurantFoodPreference(restaurantId, FoodPreference.valueOfCode(foodPreferences.get(i)));
        }
        return restaurantFoodPreferences;
    }
}
