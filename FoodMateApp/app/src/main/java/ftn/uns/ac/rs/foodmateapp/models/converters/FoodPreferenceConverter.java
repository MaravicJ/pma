package ftn.uns.ac.rs.foodmateapp.models.converters;

import androidx.room.TypeConverter;
import ftn.uns.ac.rs.foodmateapp.models.entities.FoodPreference;

public class FoodPreferenceConverter {
    @TypeConverter
    public String fromFoodPreference(FoodPreference foodPreference) {
        return foodPreference.getCode();
    }

    @TypeConverter
    public FoodPreference toFoodPreference(String code) {
        return FoodPreference.valueOfCode(code);
    }
}
