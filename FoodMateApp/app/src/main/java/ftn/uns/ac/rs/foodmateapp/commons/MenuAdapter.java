package ftn.uns.ac.rs.foodmateapp.commons;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.activities.MenuActivity;
import ftn.uns.ac.rs.foodmateapp.dto.MenuItemDTO;
import ftn.uns.ac.rs.foodmateapp.models.entities.Image;
import ftn.uns.ac.rs.foodmateapp.models.entities.MenuItem;
import ftn.uns.ac.rs.foodmateapp.viewmodel.ImageViewModel;


public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView item_name;
        public TextView item_cost;
        public TextView item_description;
        public CircleImageView item_picture;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            item_name = itemView.findViewById(R.id.item_name);
            item_cost = itemView.findViewById(R.id.item_cost);
            item_description = itemView.findViewById(R.id.item_description);
            item_picture = itemView.findViewById(R.id.item_picture);
        }
    }

    private List<MenuItemDTO> menuItems = new ArrayList<>();

    // Pass in the contact array into the constructor
    public MenuAdapter(List<MenuItemDTO> items) {
        menuItems = items;
    }
    @NonNull
    @Override
    public MenuAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.menu_item, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.ViewHolder holder, int position) {
        MenuItem menuItem = menuItems.get(position).getMenuItem();
        Image image = menuItems.get(position).getImage();

        // Set item views based on your views and data model
        TextView itemName = holder.item_name;
        itemName.setText(menuItem.getName());
        TextView itemCost = holder.item_cost;
        itemCost.setText(String.valueOf(menuItem.getCost()) + " RSD");
        TextView itemDesc = holder.item_description;
        itemDesc.setText("\"" + menuItem.getDescription() +"\"");
        CircleImageView imageView = holder.item_picture;
        imageView.setImageBitmap(image.getImage());

    }

    @Override
    public int getItemCount() {
        return menuItems.size();
    }
}
