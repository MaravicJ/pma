package ftn.uns.ac.rs.foodmateapp.asynctasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import com.squareup.okhttp.OkHttpClient;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.OkHttpClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import ftn.uns.ac.rs.foodmateapp.commons.ErrorAlert;
import ftn.uns.ac.rs.foodmateapp.security.SSL;

// T1 -> server send type
// T2 -> server return type
public class GenericAsyncTask<T1, T2> extends AsyncTask<String, Void, ResponseEntity<T2>> {

    private Context context;
    private ProgressBar spinner;
    private Class<T2> responseType;
    private T1 objectToSend;
    private String errorMessage;

    public AsyncResponse<T2> asyncResponse;

    public GenericAsyncTask(Context context, Class<T2> type, AsyncResponse asyncResponse) {
        this.context = context;
        this.responseType = type;
        this.spinner = ((Activity)context).findViewById(ftn.uns.ac.rs.foodmateapp.R.id.progressBar);
        this.asyncResponse = asyncResponse;
    }

    public GenericAsyncTask(Context context, Class<T2> type, AsyncResponse asyncResponse, boolean serviceContext) {
        this.context = context;
        this.responseType = type;
        this.spinner = null;
        this.asyncResponse = asyncResponse;
    }

    @Override
    protected ResponseEntity<T2> doInBackground(String... params) {
        final String url = params[0];
        final String token = params[1];

        HttpHeaders headers = new HttpHeaders();
        if(token != null){
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);
        }

        OkHttpClient client = SSL.createClient(context);
        ClientHttpRequestFactory requestFactory = new OkHttpClientHttpRequestFactory(client);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(requestFactory);

        HttpEntity<T1> entity = new HttpEntity<T1>(objectToSend,headers);

        ResponseEntity<T2> response = null;

        try {
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            response = restTemplate.postForEntity(url, entity, responseType);
            return response;
        } catch (HttpClientErrorException ex) {
            errorMessage = ex.getResponseBodyAsString();
            return null;
        }catch(ResourceAccessException ex){
            errorMessage = "Something went wrong! Please check your Internet connection!";
            ex.printStackTrace();
            return null;
        }
    }

    public void setObjectToSend(T1 objectToSend) {
        this.objectToSend = objectToSend;
    }

    @Override
    protected void onPostExecute(ResponseEntity<T2> result) {
        if(result == null) {
            if(spinner != null) spinner.setVisibility(View.GONE);
            ErrorAlert.showErrorDialog(context, "Message", errorMessage);
            asyncResponse.onTaskCompleted(null);
            return;
        }

        T2 returnedResult = result.getBody();
        asyncResponse.onTaskCompleted(returnedResult);
    }
}