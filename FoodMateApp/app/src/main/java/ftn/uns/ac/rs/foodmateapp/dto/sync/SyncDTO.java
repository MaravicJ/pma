package ftn.uns.ac.rs.foodmateapp.dto.sync;

import ftn.uns.ac.rs.foodmateapp.models.entities.Image;
import ftn.uns.ac.rs.foodmateapp.models.entities.MenuItem;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.WorkingDay;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SyncDTO {
    private List<RestaurantSyncDTO> restaurants;
    private List<MenuItemSyncDTO> menuItems;
    private List<ImageSyncDTO> images;
    private List<WorkingDaySyncDTO> workingDays;
    private List<RestaurantRatingSyncDTO> ratings;
}

