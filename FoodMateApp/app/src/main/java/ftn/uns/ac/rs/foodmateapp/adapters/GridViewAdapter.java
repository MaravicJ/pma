package ftn.uns.ac.rs.foodmateapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.models.Item;

public class GridViewAdapter extends ArrayAdapter<Item> {
    private Context mContext;
    private int resourceId;
    private ArrayList<Item> data;

    public GridViewAdapter(Context context, int layoutResourceId, ArrayList<Item> data) {
        super(context, layoutResourceId, data);
        this.mContext = context;
        this.resourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        System.out.println("converVi: " + convertView);
        ViewHolder holder = null;

        if (itemView == null) {
            final LayoutInflater layoutInflater =
                    (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = layoutInflater.inflate(resourceId, parent, false);

            holder = new ViewHolder();
            holder.image = itemView.findViewById(R.id.imgItem);
            holder.restaurantName = itemView.findViewById(R.id.restaurantName);
            holder.distance = itemView.findViewById(R.id.distance);
            itemView.setTag(holder);
        } else {
            holder = (ViewHolder) itemView.getTag();
        }

        Item item = getItem(position);
        holder.image.setImageBitmap(item.getImage());
        holder.restaurantName.setText(data.get(position).getTitle());
        holder.distance.setText(data.get(position).getLocation());

        itemView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 500));

        return itemView;
    }

    static class ViewHolder {
        ImageView image;
        TextView restaurantName;
        TextView distance;
    }

}
