package ftn.uns.ac.rs.foodmateapp.models.entities;

import androidx.room.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity(primaryKeys = {"userId", "restaurantId"})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChosenRestaurant {
    long userId;
    long restaurantId;
}
