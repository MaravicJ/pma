package ftn.uns.ac.rs.foodmateapp.dto;

import ftn.uns.ac.rs.foodmateapp.models.entities.Location;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class LocationDTO {
    private double longitude;
    private double latitude;
    private String city;
    private String country;
    private String address;

    public LocationDTO(Location location) {
        longitude = location.getLongitude();
        latitude = location.getLatitude();
        city = location.getCity();
        country = location.getCountry();
        address = location.getAddress();
    }

}
