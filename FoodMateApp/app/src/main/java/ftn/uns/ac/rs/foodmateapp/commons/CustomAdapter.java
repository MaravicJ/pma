package ftn.uns.ac.rs.foodmateapp.commons;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import ftn.uns.ac.rs.foodmateapp.R;

public class CustomAdapter extends BaseAdapter {

    Context context;
    ArrayList<ArrayList<String>> phoneNumbers;
    LayoutInflater inflter;
    Map<String,Boolean> isClicked;

    public CustomAdapter(Context applicationContext, ArrayList<ArrayList<String>> phoneNumbers) {
        this.context = applicationContext;
        this.phoneNumbers = phoneNumbers;
        inflter = (LayoutInflater.from(applicationContext));
        isClicked = new HashMap<>();
    }

    public void filter(String text){
        ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
        for (ArrayList list: phoneNumbers) {
            if(list.get(0).toString().toLowerCase().startsWith(text.toLowerCase())){
                result.add(list);
            }
        }
        phoneNumbers = result;
     }

     public void setPhoneNumbers(ArrayList<ArrayList<String>> numbers){
        this.phoneNumbers = numbers;
     }

     public void setColorClicked(String name){
        if(isClicked.containsKey(name)){
            isClicked.replace(name,!isClicked.get(name));
        }else{
            isClicked.put(name,true);
        }
     }

    @Override
    public int getCount() {
        return phoneNumbers.size();
    }

    @Override
    public ArrayList<String> getItem(int position) {
        return phoneNumbers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.listview, parent,false);

        ImageView imageView = convertView.findViewById(R.id.close_image);
        if(parent.getId() == R.id.simpleListView){
            imageView.setVisibility(View.INVISIBLE);
        }
        TextView name = convertView .findViewById(R.id.contact_name);
        name.setText(phoneNumbers.get(position).get(0));
        TextView number = convertView .findViewById(R.id.contact_number);
        number.setText(phoneNumbers.get(position).get(1));

        LinearLayout contact = convertView.findViewById(R.id.one_contact);

        if(isClicked.containsKey(phoneNumbers.get(position).get(0))){
            if (isClicked.get(phoneNumbers.get(position).get(0)))
                contact.setBackgroundColor(Color.rgb(244, 67, 54));
        }else{
            isClicked.put(phoneNumbers.get(position).get(0), false);
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneNumbers.remove(phoneNumbers.get(position));
                notifyDataSetChanged();
            }
        });

        return convertView;
    }
}
