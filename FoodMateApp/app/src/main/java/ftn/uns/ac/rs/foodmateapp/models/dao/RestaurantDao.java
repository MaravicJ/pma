package ftn.uns.ac.rs.foodmateapp.models.dao;

import androidx.lifecycle.LiveData;
import androidx.room.*;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.RestaurantFoodPreference;
import ftn.uns.ac.rs.foodmateapp.models.relations.RestaurantAndImage;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Dao
public interface RestaurantDao {

    @Query("SELECT COUNT(*) FROM Restaurant")
    int count();

    @Query("SELECT * FROM Restaurant")
    List<Restaurant> getAll();

    @Query("SELECT * FROM Restaurant WHERE :name = name")
    Restaurant getRestaurant(String name);

    @Query("SELECT * FROM Restaurant WHERE :restaurantId = restaurantId")
    LiveData<Restaurant> getRestaurantById(long restaurantId);

    @Query("SELECT * FROM Restaurant WHERE (:userCosLat * cosLatitude * " +
            "(:userCosLong * cosLongitude + :userSinLong * sinLongitude) + " +
            ":userSinLat * sinLatitude) > :distanceCosScaled")
    List<Restaurant> getAllWithinDistance(double userCosLat, double userSinLat,
                                                           double userCosLong, double userSinLong,
                                                           double distanceCosScaled);

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Transaction
    @Query("SELECT * FROM Image " +
            "INNER JOIN Restaurant ON mainImageId=imageId " +
            "WHERE (:userCosLat * cosLatitude * " +
            "(:userCosLong * cosLongitude + :userSinLong * sinLongitude) + " +
            ":userSinLat * sinLatitude) > :distanceCosScaled ")
    List<RestaurantAndImage> getAllWithImageWithinDistance(double userCosLat, double userSinLat,
                                                                             double userCosLong, double userSinLong,
                                                                             double distanceCosScaled);

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Transaction
    @Query("SELECT * FROM Image " +
            "INNER JOIN Restaurant ON mainImageId=imageId " +
            "INNER JOIN RestaurantFoodPreference ON Restaurant.restaurantId=RestaurantFoodPreference.restaurantId " +
            "WHERE (:userCosLat * cosLatitude * " +
            "(:userCosLong * cosLongitude + :userSinLong * sinLongitude) + " +
            ":userSinLat * sinLatitude) > :distanceCosScaled " +
            "AND Restaurant.restaurantId NOT IN (:alreadySuggested) " +
            "AND foodPreference IN (:foodPreferences) " +
            "GROUP BY Restaurant.restaurantId " +
            "ORDER BY RANDOM() LIMIT :count"
    )
    List<RestaurantAndImage> getAllWithImageWithinDistanceRandom(double userCosLat, double userSinLat,
                                                                  double userCosLong, double userSinLong,
                                                                  double distanceCosScaled, int count,
                                                                  List<Long> alreadySuggested, List<String> foodPreferences);

    @Transaction
    @Query("SELECT * FROM RestaurantFoodPreference " +
            "WHERE restaurantId = :restaurantId")
    LiveData<List<RestaurantFoodPreference>> getRestaurantFoodPreferences(Long restaurantId);

    @Query("UPDATE Restaurant SET rating = :rating WHERE restaurantId = :restaurantId")
    int updateRating(long restaurantId, float rating);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Restaurant... restaurants);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Restaurant[] restaurants);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFoodPreferences(RestaurantFoodPreference[] restaurantFoodPreferences);

    @Update
    void update(Restaurant... restaurants);

    @Delete
    void delete(Restaurant... restaurants);

}





