package ftn.uns.ac.rs.foodmateapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.RestaurantFoodPreference;
import ftn.uns.ac.rs.foodmateapp.models.repository.RestaurantRepository;

public class RestaurantViewModel extends AndroidViewModel {
    private RestaurantRepository restaurantRepository;

    public RestaurantViewModel(@NonNull Application application) {
        super(application);
        restaurantRepository = new RestaurantRepository(application);
    }

    public LiveData<Restaurant> getRestaurantById(long restaurantId) {
        return restaurantRepository.getRestaurantById(restaurantId);
    }

    public LiveData<List<RestaurantFoodPreference>> getRestaurantFoodPreferences(long restaurantId) {
        return restaurantRepository.getRestaurantFoodPreferences(restaurantId);
    }

    public void insert(Restaurant restaurant) {
        restaurantRepository.insert(restaurant);
    }
}
