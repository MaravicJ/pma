package ftn.uns.ac.rs.foodmateapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.asynctasks.AsyncResponse;
import ftn.uns.ac.rs.foodmateapp.asynctasks.GenericAsyncTask;
import ftn.uns.ac.rs.foodmateapp.commons.ErrorAlert;
import ftn.uns.ac.rs.foodmateapp.dto.AuthenticationResponseDTO;
import ftn.uns.ac.rs.foodmateapp.dto.UserDTO;
import ftn.uns.ac.rs.foodmateapp.models.entities.User;
import ftn.uns.ac.rs.foodmateapp.viewmodel.UserViewModel;

public class LoginActivity extends AppCompatActivity implements AsyncResponse {

    UserDTO user = new UserDTO();
    ProgressBar spinner;
    UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        checkLogIn();
        setUpData();

    }

    public void checkLogIn(){
        SharedRef sharedRef = new SharedRef(this);
        String v = sharedRef.getDataString("login", null);
        if(v != null && !v.equals("false")){
            Intent intent = new Intent(LoginActivity.this, SuggestionsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    public void setUpData(){
        TextView textView = findViewById(R.id.sign_up);
        textView.setText(getString(R.string.register));
        TextView textView2 = findViewById(R.id.forget_password);
        textView2.setText(getString(R.string.question_password));

        spinner = findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
    }

    public void signIn(View view){
        spinner.setVisibility(View.VISIBLE);
        EditText usernameInput = findViewById(R.id.username);
        EditText passwordInput = findViewById(R.id.password);

        user.setPassword(passwordInput.getText().toString());
        user.setUsername(usernameInput.getText().toString());

        final String uri = getString(R.string.login_url);
        GenericAsyncTask<UserDTO, AuthenticationResponseDTO> asyncTask =
                new GenericAsyncTask<>(this, AuthenticationResponseDTO.class, this);
        asyncTask.setObjectToSend(user);
        asyncTask.execute(uri, null);

    }

    @Override
    public void onTaskCompleted(Object result) {

        if (result == null){
            return;
        }

        AuthenticationResponseDTO returnedResult = (AuthenticationResponseDTO) result;
        SharedRef sharedRef = new SharedRef(LoginActivity.this);
        sharedRef.saveDataString("username",  returnedResult.getUsername());
        sharedRef.saveDataString("token",  returnedResult.getToken());
        sharedRef.saveDataString("login",  "true");

        User user = new User();
        user.setUsername(returnedResult.getUsername());
        userViewModel.insert(user);

        Intent intent = new Intent(LoginActivity.this, SuggestionsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        String message = getString(R.string.successfully_login);
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

    }

    public void forgetPassword(View view){
        TextView textView = findViewById(R.id.forget_password);
        String text = textView.getText().toString();
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, text.length(),0);
        textView.setText(content);

        Intent intent = new Intent(this, ResetPasswordActivity.class);
        startActivity(intent);
    }

    public void registration(View view){
        TextView textView = findViewById(R.id.sign_up);
        String text = textView.getText().toString();
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, text.length(),0);
        textView.setText(content);
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }
}
