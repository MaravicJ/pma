package ftn.uns.ac.rs.foodmateapp.models.relations;

import androidx.room.Embedded;
import androidx.room.Relation;
import ftn.uns.ac.rs.foodmateapp.models.entities.Image;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;

public class RestaurantAndImage {
    @Embedded public Image image;
    @Relation(
            parentColumn = "imageId",
            entityColumn = "mainImageId"
    )
    public Restaurant restaurant;

    public long getRestaurantId() {
        return restaurant.getRestaurantId();
    }
}
