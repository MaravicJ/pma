package ftn.uns.ac.rs.foodmateapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.entities.WorkingDay;
import ftn.uns.ac.rs.foodmateapp.models.repository.WorkingDayRepository;

public class WorkingDayViewModel extends AndroidViewModel {
    private WorkingDayRepository workingDayRepository;

    public WorkingDayViewModel(@NonNull Application application) {
        super(application);
        workingDayRepository = new WorkingDayRepository(application);
    }

    public LiveData<List<WorkingDay>> getWorkingDayByRestaurantId(long restaurantId) {
        return workingDayRepository.getWorkingDayByRestaurantId(restaurantId);
    }
}
