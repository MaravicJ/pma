package ftn.uns.ac.rs.foodmateapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.asynctasks.AsyncResponse;
import ftn.uns.ac.rs.foodmateapp.asynctasks.GenericAsyncTask;
import ftn.uns.ac.rs.foodmateapp.asynctasks.SendMail;
import ftn.uns.ac.rs.foodmateapp.dto.ResetPasswordResponseDTO;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

public class ResetPasswordActivity extends AppCompatActivity implements AsyncResponse {

    String email;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        setUpProgressBar();

    }

    public void setUpProgressBar(){
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
    }

    public void resetPassword(View view) {

        EditText emailInput = findViewById(R.id.email);
        email = emailInput.getText().toString();

        final String uri = getString(R.string.reset_password_url);
        GenericAsyncTask<String, ResetPasswordResponseDTO> asyncTask =
                new GenericAsyncTask<>(this, ResetPasswordResponseDTO.class, this);
        asyncTask.setObjectToSend(email);
        asyncTask.execute(uri, null);

    }

    @Override
    public void onTaskCompleted(Object result) {

        String subject = getString(R.string.mail_subject);
        ResetPasswordResponseDTO responseDTO = (ResetPasswordResponseDTO) result;
        String body = "<div><img src=\"cid:image\" style=\"width: 300px; max-width: 600px; height: auto; margin: auto; " +
                "display: block;background-color:red\"></div>" +
                "<h1 align=center>FoodMate</h1><p align=center>Hey " + responseDTO.getUsername() + "!</p>" +
                "<p align=center>Your password is successfully reset!</p>" +
                "<p align=center>Your new password is: " + responseDTO.getNew_password() + "</p>" +
                "<p align=center>FoodMate Team</p>";
        SendMail sm = new SendMail(this, email,subject,body);
        sm.execute();

    }
}
