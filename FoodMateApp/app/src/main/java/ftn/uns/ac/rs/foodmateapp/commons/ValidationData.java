package ftn.uns.ac.rs.foodmateapp.commons;

import android.content.Context;

import org.joda.time.DateTimeComparator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ValidationData {

    public boolean checkInput(String text){
        if(text.equals("")){
            return true;
        }
        return false;
    }

    public boolean checkDate(String date) throws ParseException {
        Date date_reservation = new SimpleDateFormat("d MMM yyyy").parse(date);
        Date now = new Date();
        if(date_reservation.after(now)){
            return false;
        }
        DateTimeComparator dateTimeComparator = DateTimeComparator.getDateOnlyInstance();
        int retVal = dateTimeComparator.compare(date_reservation, now);
        if(retVal == 0){
            return false;
        }
        return true;
    }

    public boolean checkTime(String date, String time) throws ParseException {
        String dateTime = date + " " + time;
        Date date_reservation = new SimpleDateFormat("d MMM yyyy HH:mm").parse(dateTime);
        Date now = new Date();
        if(date_reservation.after(now)){
            return false;
        }
        return true;
    }
}
