package ftn.uns.ac.rs.foodmateapp.asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.activities.SettingsActivity;
import ftn.uns.ac.rs.foodmateapp.activities.SuggestionsActivity;
import ftn.uns.ac.rs.foodmateapp.models.dao.ChosenRestaurantDao;
import ftn.uns.ac.rs.foodmateapp.models.dao.RestaurantDao;
import ftn.uns.ac.rs.foodmateapp.models.database.AppDatabase;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.RestaurantFoodPreference;
import ftn.uns.ac.rs.foodmateapp.models.relations.RestaurantAndImage;

import java.lang.ref.WeakReference;
import java.util.*;
import java.util.stream.Collectors;

public class LoadSuggestionsTask extends AsyncTask<Double, Void, List<RestaurantAndImage>> {

    SuggestionsLoadedListener suggestionsLoadedListener;
    WeakReference<Context> contextWeakReference;
    List<RestaurantAndImage> suggestions;
    RestaurantDao restaurantDao;
    ChosenRestaurantDao chosenRestaurantDao;
    List<String> foodPreferences;
    String username;
    SharedRef sharedRef;
    int loadCount;

    public LoadSuggestionsTask(Context context, List<RestaurantAndImage> suggestions,
                               int loadCount, SuggestionsLoadedListener suggestionsLoadedListener) {
        this.suggestionsLoadedListener = suggestionsLoadedListener;
        this.contextWeakReference = new WeakReference<>(context);

        this.restaurantDao = AppDatabase.getInstance(context).restaurantDao();
        this.chosenRestaurantDao = AppDatabase.getInstance(context).chosenRestaurantDao();
        this.sharedRef = new SharedRef(context);
        this.foodPreferences = new LinkedList<>();
        this.foodPreferences.addAll(sharedRef.getDataStringSet("foodPreferences", SettingsActivity.getDefaultFoodPreferences()));
        Log.d("LOAD_SUGG", "Selected food preferences: " + this.foodPreferences.toString());
        this.username = sharedRef.getDataString("username", null);
        this.suggestions = suggestions;
        this.loadCount = loadCount;
    }



    @Override
    protected List<RestaurantAndImage> doInBackground(Double... params) {
        double cosLatitude = params[0];
        double sinLatitude = params[1];
        double cosLongitude = params[2];
        double sinLongitude = params[3];
        double distance = params[4];

        List<Long> alreadySuggested = new ArrayList<>();
        alreadySuggested.addAll(SuggestionsActivity.alreadySuggested);
        List<Long> chosenIds = getChosenIds();
        Log.d("LOAD_SUGG", "Chosen ids: " + chosenIds.toString());
        alreadySuggested.addAll(chosenIds);
                //suggestions.stream().map(RestaurantAndImage::getRestaurantId).collect(Collectors.toList());
        Log.d("LOAD_SUGG", "Already suggested: " + alreadySuggested.toString());
        /*
        Log.d("LOAD_SUGG", "Selected food preferences: " + foodPreferences.toString());
        Log.d("LOAD_SUGG", "Restaurant 1 food preferences: " + restaurantDao.getRestaurantFoodPreferences(Arrays.asList(1L)));
        Log.d("LOAD_SUGG", "Restaurant 2 food preferences: " + restaurantDao.getRestaurantFoodPreferences(Arrays.asList(2L)));
        Log.d("LOAD_SUGG", "Restaurant 3 food preferences: " + restaurantDao.getRestaurantFoodPreferences(Arrays.asList(3L)));
        */
        return restaurantDao.getAllWithImageWithinDistanceRandom(
                cosLatitude, sinLatitude, cosLongitude, sinLongitude, distance, loadCount, alreadySuggested, foodPreferences
        );
    }


    @Override
    protected void onPostExecute(List<RestaurantAndImage> loadedSuggestions) {
        super.onPostExecute(loadedSuggestions);
        suggestionsLoadedListener.onSuggestionsLoaded(loadedSuggestions);
    }

    private List<Long> getChosenIds(){
        return chosenRestaurantDao.getChosenRestaurantIds(username);
    }
}
