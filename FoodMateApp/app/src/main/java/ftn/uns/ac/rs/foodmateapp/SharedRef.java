package ftn.uns.ac.rs.foodmateapp;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Date;
import java.util.Set;

public class SharedRef {

    SharedPreferences sharedRef;

    public SharedRef(Context context){
        sharedRef = context.getSharedPreferences(context.getString(R.string.preferences), Context.MODE_PRIVATE);
    }

    public void saveDataString(String key, String value){
        SharedPreferences.Editor editor = sharedRef.edit();
        editor.putString(key,value);
        editor.commit();
    }

    public void saveDataBoolean(String key, boolean value){
        SharedPreferences.Editor editor = sharedRef.edit();
        editor.putBoolean(key,value);
        editor.commit();
    }

    public void saveDataLong(String key, long value) {
        SharedPreferences.Editor editor = sharedRef.edit();
        editor.putLong(key,value);
        editor.commit();
    }

    public void saveDataDate(String key, Date date) {
        saveDataLong(key, date.getTime());
    }

    public void saveSetData(String key, Set<String> values) {
        SharedPreferences.Editor editor = sharedRef.edit();
        editor.putStringSet(key, values);
        editor.commit();
    }

    public String getDataString(String name, String default_value){
        String result = sharedRef.getString(name,default_value);
        return result;
    }

    public long getDataLong(String key, long default_value){
        long result = sharedRef.getLong(key,default_value);
        return result;
    }

    public Date getDataDate(String key, Date default_date) {
        long timestamp = getDataLong(key, 0);
        if(timestamp == 0) return default_date;
        else return new Date(timestamp);
    }

    public boolean getDataBoolean(String name,boolean default_value){
        boolean result = sharedRef.getBoolean(name,default_value);
        return result;
    }

    public Set<String> getDataStringSet(String name, Set<String> default_value){
        Set<String> result = sharedRef.getStringSet(name, default_value);
        return result;
    }

}
