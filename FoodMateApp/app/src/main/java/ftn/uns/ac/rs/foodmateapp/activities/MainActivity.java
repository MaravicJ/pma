package ftn.uns.ac.rs.foodmateapp.activities;

import android.os.Bundle;

import ftn.uns.ac.rs.foodmateapp.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

}
