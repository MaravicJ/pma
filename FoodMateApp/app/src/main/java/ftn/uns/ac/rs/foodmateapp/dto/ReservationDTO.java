package ftn.uns.ac.rs.foodmateapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReservationDTO {
    private long restaurant_id;
    private String username;
    private String date;
    private String time;
    private int number_persons;
}
