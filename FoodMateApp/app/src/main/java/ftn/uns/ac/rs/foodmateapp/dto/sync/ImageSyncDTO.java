package ftn.uns.ac.rs.foodmateapp.dto.sync;


import ftn.uns.ac.rs.foodmateapp.models.entities.Image;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImageSyncDTO {
    private long imageId;
    private String path;
    private long imageRestaurantId;

    public Image makeImage() {
        Image image = new Image();
        image.setImageId(imageId);
        image.setPath(path);
        image.setImageRestaurantId(imageRestaurantId);
        return image;
    }
}
