package ftn.uns.ac.rs.foodmateapp.security;

import android.content.Context;
import com.squareup.okhttp.OkHttpClient;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;
import ftn.uns.ac.rs.foodmateapp.R;

public class SSL {

    public static OkHttpClient createClient(Context context){

        OkHttpClient client = new OkHttpClient();
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream cert = context.getResources().openRawResource(R.raw.foodmateandroid); // Place your 'your_cert.crt' file in `res/raw`

            Certificate ca = cf.generateCertificate(cert);
            cert.close();

            InputStream certTrust = context.getResources().openRawResource(R.raw.foodmate); // Place your 'your_cert.crt' file in `res/raw`

            Certificate caTrust = cf.generateCertificate(certTrust);
            certTrust.close();

            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("foodmateandroid", ca);
            keyStore.setCertificateEntry("foodmate", caTrust);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(keyStore);
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(keyStore, "foodmate".toCharArray());
            sslContext.init(keyManagerFactory.getKeyManagers(),trustManagerFactory.getTrustManagers(), null);
            client.setSslSocketFactory(sslContext.getSocketFactory());

            client.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            client.setConnectTimeout(7000, TimeUnit.SECONDS);

        }catch (IOException | KeyStoreException |NoSuchAlgorithmException | UnrecoverableKeyException | KeyManagementException | CertificateException e){
            e.printStackTrace();
        }

        return client;
    }

}
