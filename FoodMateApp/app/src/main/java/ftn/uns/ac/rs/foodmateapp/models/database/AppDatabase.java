package ftn.uns.ac.rs.foodmateapp.models.database;

import android.content.Context;
import androidx.annotation.VisibleForTesting;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import androidx.room.TypeConverters;
import ftn.uns.ac.rs.foodmateapp.models.converters.FoodPreferenceConverter;
import ftn.uns.ac.rs.foodmateapp.models.dao.*;
import ftn.uns.ac.rs.foodmateapp.models.dao.ChosenRestaurantDao;
import ftn.uns.ac.rs.foodmateapp.models.dao.ImageDao;
import ftn.uns.ac.rs.foodmateapp.models.dao.RestaurantDao;
import ftn.uns.ac.rs.foodmateapp.models.dao.UserDao;

import ftn.uns.ac.rs.foodmateapp.models.entities.*;

@Database(entities = {
                Category.class,
                Image.class,
                MenuItem.class,
                Restaurant.class,
                WorkingDay.class,
                User.class,
                ChosenRestaurant.class,
                RestaurantFoodPreference.class
        }, version = 7)
@TypeConverters(FoodPreferenceConverter.class)
public abstract class AppDatabase extends RoomDatabase {

    public abstract RestaurantDao restaurantDao();

    public abstract ChosenRestaurantDao chosenRestaurantDao();

    public abstract UserDao userDao();

    public abstract MenuItemDao menuItemDao();

    public abstract ImageDao imageDao();

    public abstract WorkingDayDao workingDayDao();

    private static AppDatabase databaseInstance;

    public static synchronized AppDatabase getInstance(Context context) {
        if(databaseInstance == null) {
            databaseInstance = Room
                    .databaseBuilder(context.getApplicationContext(), AppDatabase.class, "foodmate-db")
                    .fallbackToDestructiveMigration()
                    .build();
            //databaseInstance.populateInitialData();
        }
        return databaseInstance;
    }

    @VisibleForTesting
    public static void switchToInMemory(Context context) {
        databaseInstance = Room.inMemoryDatabaseBuilder(context.getApplicationContext(), AppDatabase.class).build();
    }

    private void populateInitialData() {
        if(restaurantDao().count() == 0){
            runInTransaction(new Runnable() {
                @Override
                public void run() {
                    // Insert initial data into db
                }
            });
        }

    }


}
