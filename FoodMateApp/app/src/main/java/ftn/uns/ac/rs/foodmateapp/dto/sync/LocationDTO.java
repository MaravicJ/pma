package ftn.uns.ac.rs.foodmateapp.dto.sync;


import ftn.uns.ac.rs.foodmateapp.commons.LocationUtils;
import ftn.uns.ac.rs.foodmateapp.models.entities.Location;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class LocationDTO {
    private double longitude;
    private double latitude;
    private String city;
    private String country;
    private String address;

    public Location makeLocation() {
        Location location = new Location();
        location.setLongitude(longitude);
        location.setLatitude(latitude);
        location.setCity(city);
        location.setCountry(country);
        location.setAddress(address);

        location.setCosLatitude(LocationUtils.calculateCosLatitude(latitude));
        location.setCosLongitude(LocationUtils.calculateCosLongitude(longitude));
        location.setSinLatitude(LocationUtils.calculateSinLatitude(latitude));
        location.setSinLongitude(LocationUtils.calculateSinLongitude(longitude));

        return location;
    }
}
