package ftn.uns.ac.rs.foodmateapp.asynctasks;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.OkHttpClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.commons.ErrorAlert;
import ftn.uns.ac.rs.foodmateapp.commons.ImageAction;
import ftn.uns.ac.rs.foodmateapp.dto.MenuItemDTO;
import ftn.uns.ac.rs.foodmateapp.dto.MenuItemResponseDTO;
import ftn.uns.ac.rs.foodmateapp.models.entities.Image;
import ftn.uns.ac.rs.foodmateapp.models.entities.MenuItem;
import ftn.uns.ac.rs.foodmateapp.security.SSL;

public class LoadMenuItemImage extends AsyncTask<String, Void, List<MenuItemDTO>> {
    private List<MenuItem> menuItems;
    private final WeakReference<Context> contextWeakReference;
    public AsyncResponse<List<MenuItemDTO>> asyncResponse;
    public List<MenuItemDTO> finalList = new ArrayList<>();


    public static final String IMAGE_LOADED_BROADCAST =  "ftn.uns.ac.rs.foodmateapp.broadcasts.IMAGE_LOADED";

    public LoadMenuItemImage(List<MenuItem> menuItem, Context context,AsyncResponse asyncResponse) {
        this.menuItems = menuItem;
        this.contextWeakReference = new WeakReference<>(context);
        this.asyncResponse = asyncResponse;
    }

    @Override
    protected List<MenuItemDTO> doInBackground(String... params) {
        final String url = params[0];
        Context context = contextWeakReference.get();
        if(context == null)
            return null;
        Bitmap image;
        int check = checkCacheImage(context);
        if(!(check == 0)) {
            finalList.clear();
            List<MenuItemResponseDTO> list = sendImageRequest(url, menuItems);
            if(list!= null){
                for (MenuItemResponseDTO i:list) {
                    image = ImageAction.stringToBitMap(i.getImage());
                    ImageAction.saveToCache(image, i.getMenuItem().getCachedImageFilename(), context);
                    insertInList(i.getMenuItem(),image);
                }
            }
        }

        return finalList;
    }

    private int checkCacheImage(Context context){
        int result = 0;
        for (MenuItem item:menuItems) {
            Bitmap image = ImageAction.loadFromCache(item.getCachedImageFilename(), context);
            if(image == null){
                result++;
            }else{
                insertInList(item,image);
            }
        }
        return result;
    }

    private void insertInList(MenuItem item,Bitmap image){
        MenuItemDTO menuItemDTO = new MenuItemDTO();
        Image i = new Image();
        i.setImage(image);
        menuItemDTO.setImage(i);
        menuItemDTO.setMenuItem(item);
        finalList.add(menuItemDTO);
    }

    private List<MenuItemResponseDTO> sendImageRequest(String url,List<MenuItem> items) {
        Context context = contextWeakReference.get();
        if (context == null)
            return null;

        SharedRef sharedRef = new SharedRef(context);

        String token = sharedRef.getDataString("token", null);
        HttpHeaders headers = new HttpHeaders();
        if(token != null){
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);
        }

        OkHttpClient client = SSL.createClient(context);
        ClientHttpRequestFactory requestFactory = new OkHttpClientHttpRequestFactory(client);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(requestFactory);

        ResponseEntity<MenuItemResponseDTO[]> response;
        HttpEntity<List<MenuItem>> entity = new HttpEntity<List<MenuItem>>(menuItems,headers);

        try {
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            response = restTemplate.postForEntity(url,entity, MenuItemResponseDTO[].class);
            List<MenuItemResponseDTO> list = Arrays.asList(response.getBody());
            return list;
        } catch (HttpClientErrorException ex) {
            Log.e("LOAD_IMAGE", ex.getResponseBodyAsString() + "; ImageID: "
                    + menuItems.get(0).getMenuItemImageId());
            return null;
        }catch(ResourceAccessException ex){
            Log.e("LOAD_IMAGE","Something went wrong! Please try again! ImageID: "
                    + menuItems.get(0).getMenuItemImageId());
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<MenuItemDTO> result) {
        System.out.println("IMAGEEEEE  " + result);
        asyncResponse.onTaskCompleted(result);
    }
}
