package ftn.uns.ac.rs.foodmateapp.commons;

import java.util.Locale;

public class LocationUtils {

    public static double calculateCosLatitude(double latitude) {
        return Math.cos(Math.toRadians(latitude));
    }

    public static double calculateSinLatitude(double latitude) {
        return Math.sin(Math.toRadians(latitude));
    }

    public static double calculateCosLongitude(double longitude) {
        return Math.cos(Math.toRadians(longitude));
    }

    public static double calculateSinLongitude(double longitude) {
        return Math.sin(Math.toRadians(longitude));
    }

    public static double calculateCosScaledDistance(double distanceInKilometers) {
        return Math.cos(distanceInKilometers / 6371.0);
    }

    public static String buildDistanceWhereClause(double latitude, double longitude, double distanceInKilometers) {

        final double coslat = calculateCosLatitude(latitude);
        final double sinlat = calculateSinLatitude(latitude);
        final double coslng = calculateCosLongitude(longitude);
        final double sinlng = calculateSinLongitude(longitude);

        final String format = "(%1$s * %2$s * (%3$s * %4$s + %5$s * %6$s) + %7$s * %8$s) > %9$s";
        return String.format(format,
                coslat, "cosLatitude",
                coslng, "cosLongitude",
                sinlng, "sinLongitude",
                sinlat, "sinLatitude",
                calculateCosScaledDistance(distanceInKilometers)
        );
    }

    public static String getDistanceString(double distance) {
        if(distance>1000) {
            return String.format(Locale.getDefault() ,"%.1fkm", distance/1000);
        } else {
            return String.format(Locale.getDefault() ,"%.0fm", distance);
        }
    }
}
