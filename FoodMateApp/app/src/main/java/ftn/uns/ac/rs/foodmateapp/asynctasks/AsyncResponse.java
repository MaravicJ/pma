package ftn.uns.ac.rs.foodmateapp.asynctasks;

public interface AsyncResponse<T> {
    void onTaskCompleted(T result);
}
