package ftn.uns.ac.rs.foodmateapp.sync;

import android.accounts.Account;
import android.content.*;
import android.os.Bundle;
import android.util.Log;
import com.squareup.okhttp.OkHttpClient;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.dto.sync.*;
import ftn.uns.ac.rs.foodmateapp.models.dao.ImageDao;
import ftn.uns.ac.rs.foodmateapp.models.dao.MenuItemDao;
import ftn.uns.ac.rs.foodmateapp.models.dao.RestaurantDao;
import ftn.uns.ac.rs.foodmateapp.models.dao.WorkingDayDao;
import ftn.uns.ac.rs.foodmateapp.models.database.AppDatabase;
import ftn.uns.ac.rs.foodmateapp.models.entities.Image;
import ftn.uns.ac.rs.foodmateapp.models.entities.MenuItem;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.WorkingDay;
import ftn.uns.ac.rs.foodmateapp.security.SSL;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.OkHttpClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class SyncAdapter extends AbstractThreadedSyncAdapter {

    public static final String SYNC_FINISHED_BROADCAST = "ftn.uns.ac.rs.foodmateapp.broadcasts.SYNC_FINISHED_BROADCAST";

    private SharedRef sharedRef;
    ContentResolver contentResolver;
    private int syncEntityCount;


    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        contentResolver = context.getContentResolver();
    }

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        contentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {

        Log.d("SYNC", "Syncing...");
        long syncRequestStartTime = System.currentTimeMillis();
        sharedRef = new SharedRef(this.getContext());

        SyncDTO syncResponse = sendSyncRequest(createSyncRequest());
        long syncRequestDuration = System.currentTimeMillis() - syncRequestStartTime;
        long syncProcessingStartTime = System.currentTimeMillis();
        syncEntityCount = 0;
        if(syncResponse != null){
            updateRestaurants(syncResponse.getRestaurants());
            updateMenuItems(syncResponse.getMenuItems());
            updateImages(syncResponse.getImages());
            updateWorkingDays(syncResponse.getWorkingDays());
            updateRatings(syncResponse.getRatings());
            Log.d("SYNC", "Sync finished. " + syncEntityCount + " entities updated.");
            sharedRef.saveDataDate(getContext().getString(R.string.last_update_key), new Date());
        } else {
            Log.d("SYNC", "Sync failed");
        }
        long syncProcessingDuration = System.currentTimeMillis() - syncProcessingStartTime;
        Log.d("SYNC", "Network request: " + syncRequestDuration +
                "ms; Processing: " + syncProcessingDuration + "ms.");

        Intent intent = new Intent(SYNC_FINISHED_BROADCAST);
        getContext().sendBroadcast(intent);

    }

    private void updateRestaurants(List<RestaurantSyncDTO> restaurantDTOs) {
        Restaurant[] restaurants = new Restaurant[restaurantDTOs.size()];
        RestaurantDao restaurantDao = AppDatabase.getInstance(getContext()).restaurantDao();
        for(int i=0; i<restaurantDTOs.size(); i++) {
            restaurants[i] = restaurantDTOs.get(i).makeRestaurant();
            restaurantDao.insertFoodPreferences(restaurantDTOs.get(i).getRestaurantFoodPreferences());
        }
        syncEntityCount += restaurantDTOs.size();
        System.out.println("RESTAURANT " + restaurants.length);
        restaurantDao.insertAll(restaurants);
    }

    private void updateMenuItems(List<MenuItemSyncDTO> menuItemDTOs) {
        MenuItem[] menuItems = new MenuItem[menuItemDTOs.size()];
        MenuItemDao menuItemDao = AppDatabase.getInstance(getContext()).menuItemDao();
        for(int i=0; i<menuItemDTOs.size(); i++) {
            menuItems[i] = menuItemDTOs.get(i).makeMenuItem();
        }
        syncEntityCount += menuItemDTOs.size();
        menuItemDao.insertAll(menuItems);
    }

    private void updateImages(List<ImageSyncDTO> imageDTOs) {
        Image[] images = new Image[imageDTOs.size()];
        ImageDao imageDao = AppDatabase.getInstance(getContext()).imageDao();
        for(int i=0; i<imageDTOs.size(); i++) {
            images[i] = imageDTOs.get(i).makeImage();
        }
        syncEntityCount += imageDTOs.size();
        imageDao.insertAll(images);
    }

    private void updateWorkingDays(List<WorkingDaySyncDTO> workingDayDTOs) {
        WorkingDay[] workingDays = new WorkingDay[workingDayDTOs.size()];
        WorkingDayDao workingDayDao = AppDatabase.getInstance(getContext()).workingDayDao();
        for(int i=0; i<workingDayDTOs.size(); i++) {
            workingDays[i] = workingDayDTOs.get(i).makeWorkingDay();
        }
        syncEntityCount += workingDayDTOs.size();
        workingDayDao.insertAll(workingDays);
    }

    private void updateRatings(List<RestaurantRatingSyncDTO> ratingDTOs) {
        RestaurantDao restaurantDao = AppDatabase.getInstance(getContext()).restaurantDao();
        for(RestaurantRatingSyncDTO ratingDTO: ratingDTOs) {
            restaurantDao.updateRating(ratingDTO.getRestaurantId(), ratingDTO.getRating());
        }
    }

    private SyncRequestDTO createSyncRequest() {
        String city = "Novi Sad";
        Date lastUpdate = sharedRef.getDataDate(getContext().getString(R.string.last_update_key), new Date(0));
        return new SyncRequestDTO(city, lastUpdate);
    }

    private SyncDTO sendSyncRequest(SyncRequestDTO request) {
        String token = sharedRef.getDataString("token", null);
        HttpHeaders headers = new HttpHeaders();
        if(token != null){
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);
        }

        OkHttpClient client = SSL.createClient(getContext());
        ClientHttpRequestFactory requestFactory = new OkHttpClientHttpRequestFactory(client);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(requestFactory);

        HttpEntity<SyncRequestDTO> entity = new HttpEntity<>(request, headers);
        ResponseEntity<SyncDTO> response;
        final String url = getContext().getString(R.string.sync_url);

        try {
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            response = restTemplate.postForEntity(url, entity, SyncDTO.class);
            return response.getBody();
        } catch (HttpClientErrorException ex) {
            Log.e("SYNC", ex.getResponseBodyAsString());
            return null;
        }catch(ResourceAccessException ex){
            Log.e("SYNC","Something went wrong! Please try again!");
            return null;
        }
    }


}
