package ftn.uns.ac.rs.foodmateapp.models.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantFoodPreference {
    @PrimaryKey(autoGenerate = true)
    private long foodPreferenceId;
    private long restaurantId;
    private FoodPreference foodPreference;

    public RestaurantFoodPreference(long restaurantId, FoodPreference foodPreference) {
        this.restaurantId = restaurantId;
        this.foodPreference = foodPreference;
    }
}
