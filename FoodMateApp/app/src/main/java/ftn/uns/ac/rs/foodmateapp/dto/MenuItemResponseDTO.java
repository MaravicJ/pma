package ftn.uns.ac.rs.foodmateapp.dto;

import ftn.uns.ac.rs.foodmateapp.models.entities.MenuItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuItemResponseDTO {
    private MenuItem menuItem;
    private String image;
}
