package ftn.uns.ac.rs.foodmateapp.models.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.entities.WorkingDay;

@Dao
public interface WorkingDayDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(WorkingDay[] workingDays);

    @Transaction
    @Query("SELECT * FROM WorkingDay where :workingDayRestaurantId = workingDayRestaurantId")
    LiveData<List<WorkingDay>> getWorkingDayByRestaurantId(long workingDayRestaurantId);

}
