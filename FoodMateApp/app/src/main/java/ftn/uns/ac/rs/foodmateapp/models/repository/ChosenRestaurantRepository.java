package ftn.uns.ac.rs.foodmateapp.models.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.dao.ChosenRestaurantDao;
import ftn.uns.ac.rs.foodmateapp.models.database.AppDatabase;
import ftn.uns.ac.rs.foodmateapp.models.entities.ChosenRestaurant;

public class ChosenRestaurantRepository {
    private ChosenRestaurantDao chosenRestaurantDao;

    public ChosenRestaurantRepository(Application application) {
        AppDatabase appDatabase = AppDatabase.getInstance(application);
        chosenRestaurantDao = appDatabase.chosenRestaurantDao();
    }

    public LiveData<List<ChosenRestaurant>> getChosenRestaurantsByUserId(long userId) {
        return chosenRestaurantDao.getChosenRestaurantsByUserId(userId);
    }

    public LiveData<ChosenRestaurant> getChosenRestaurantById(long id) {
        return chosenRestaurantDao.getChosenRestaurantById(id);
    }

    public void insert(ChosenRestaurant chosenRestaurant) {
        new ChosenRestaurantInsertAsyncTask(chosenRestaurantDao).execute(chosenRestaurant);
    }

    public void delete(ChosenRestaurant chosenRestaurant) {
        new ChosenRestaurantDeleteAsyncTask(chosenRestaurantDao).execute(chosenRestaurant);
    }

    private static class ChosenRestaurantInsertAsyncTask extends AsyncTask<ChosenRestaurant, Void, Void> {
        private ChosenRestaurantDao chosenRestaurantDao;

        ChosenRestaurantInsertAsyncTask(ChosenRestaurantDao chosenRestaurantDao) {
            this.chosenRestaurantDao = chosenRestaurantDao;
        }

        @Override
        protected Void doInBackground(ChosenRestaurant... restaurants) {
            chosenRestaurantDao.insert(restaurants[0]);
            return null;
        }
    }

    private static class ChosenRestaurantDeleteAsyncTask extends AsyncTask<ChosenRestaurant, Void, Void> {
        private ChosenRestaurantDao chosenRestaurantDao;

        ChosenRestaurantDeleteAsyncTask(ChosenRestaurantDao chosenRestaurantDao) {
            this.chosenRestaurantDao = chosenRestaurantDao;
        }

        @Override
        protected Void doInBackground(ChosenRestaurant... restaurants) {
            chosenRestaurantDao.delete(restaurants[0]);
            return null;
        }
    }
}




