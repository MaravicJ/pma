package ftn.uns.ac.rs.foodmateapp.models.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.entities.MenuItem;

@Dao
public interface MenuItemDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll(MenuItem[] menuItems);

    @Query("SELECT * FROM MenuItem WHERE :menuItemRestaurantId = menuItemRestaurantId")
    LiveData<List<MenuItem>> getMenuItems(long menuItemRestaurantId);
}
