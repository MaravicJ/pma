package ftn.uns.ac.rs.foodmateapp.models.entities;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import ftn.uns.ac.rs.foodmateapp.dto.ImageDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.parceler.Parcel;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Parcel(Parcel.Serialization.BEAN)
public class Restaurant {
    @PrimaryKey(autoGenerate = true)
    private long restaurantId;

    private String name;
    private String description;
    private String email;
    private String phoneNumber;
    private double rating;

    @Embedded private Location location;
    private long mainImageId;

    public Restaurant(String name, String description, String email, String phoneNumber, double rating, Location location) {
        this.name = name;
        this.description = description;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.rating = rating;
        this.location = location;
    }

    public String getCachedImageFilename() {
        return "restaurant_" + restaurantId;
    }
}
