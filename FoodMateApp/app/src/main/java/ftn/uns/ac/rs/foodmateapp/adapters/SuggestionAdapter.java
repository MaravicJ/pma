package ftn.uns.ac.rs.foodmateapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.relations.RestaurantAndImage;

import java.util.ArrayList;

public class SuggestionAdapter extends ArrayAdapter<RestaurantAndImage> {

    public SuggestionAdapter(@NonNull Context context, ArrayList<RestaurantAndImage> suggestions) {
        super(context, 0, suggestions);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.suggested_item, parent, false);

        RestaurantAndImage suggestion = getItem(position);

        ImageView imageView = convertView.findViewById(R.id.suggestedImageView);

        if(suggestion!=null)
            imageView.setImageBitmap(suggestion.image.getImage());

        return convertView;
    }
}
