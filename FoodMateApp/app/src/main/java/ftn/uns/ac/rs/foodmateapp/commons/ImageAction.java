package ftn.uns.ac.rs.foodmateapp.commons;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import java.io.*;
import java.lang.ref.WeakReference;
import java.util.Random;

public class ImageAction {

    public static Bitmap stringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void saveToCache(Bitmap image, String filename, Context context) {
        SaveToCacheTask task = new SaveToCacheTask(image, filename, context);
        task.execute();
    }

    private static class SaveToCacheTask extends AsyncTask<Void, Void, Void> {
        private Bitmap image;
        private final String filename;
        private final WeakReference<Context> contextWeakReference;

        public SaveToCacheTask(Bitmap image, String filename, Context context) {
            this.image = image;
            this.filename = filename;
            this.contextWeakReference = new WeakReference<>(context);
        }

        protected Void doInBackground(Void... params) {
            Context context = contextWeakReference.get();
            if(context == null)
                return null;
            try {
                long time = System.currentTimeMillis();
                image = resize(image, 512, 512);
                File file = new File(context.getCacheDir(), filename);
                OutputStream outputStream = new FileOutputStream(file);
                image.compress(Bitmap.CompressFormat.JPEG, 85, outputStream);
                outputStream.close();
                Log.d("IMAGE_CACHE", "Saved " + filename +
                        " to cache in " + (System.currentTimeMillis() - time) + "ms");
            } catch (Exception e) {
                Log.e("IMAGE_CACHE", "Saving to cache failed. " + e.getMessage());
            }
            return null;
        }
    }


    public static Bitmap loadFromCache(String filename, Context context) {
        long time = System.currentTimeMillis();
        File file = new File(context.getCacheDir(), filename);
        if(file.exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            Log.d("IMAGE_CACHE", "Loaded " + filename +
                    " from cache in " + (System.currentTimeMillis() - time) + "ms");
            return bitmap;
        } else{
            Log.d("IMAGE_CACHE", "Image " + filename + " is not cached.");
            return null;
        }
    }

    private static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    public static String saveImage(Bitmap finalBitmap) {
        System.out.println(finalBitmap);
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/restaurants");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fileName = "Image-"+ n +".jpg";
        File file = new File(myDir, fileName);
        if (file.exists()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            return fileName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }

    public static Bitmap loadImageFromStorage(String path)
    {
        try {
            //path null
            System.out.println(Environment.getExternalStorageDirectory().toString() + "/restaurants/" +  path);
            File f = new File(Environment.getExternalStorageDirectory().toString() + "/restaurants/" +  path);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
