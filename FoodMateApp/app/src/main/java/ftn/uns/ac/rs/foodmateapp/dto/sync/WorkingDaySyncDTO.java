package ftn.uns.ac.rs.foodmateapp.dto.sync;

import ftn.uns.ac.rs.foodmateapp.models.entities.WorkingDay;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkingDaySyncDTO {
    private long workingDayId;
    private String day;
    private String timeFrom;
    private String timeTo;
    private long workingDayRestaurantId;

    public WorkingDay makeWorkingDay() {
        WorkingDay workingDay = new WorkingDay();
        workingDay.setWorkingDayId(workingDayId);
        workingDay.setDay(day);
        workingDay.setTimeFrom(timeFrom);
        workingDay.setTimeTo(timeTo);
        workingDay.setWorkingDayRestaurantId(workingDayRestaurantId);
        return  workingDay;
    }

}
