package ftn.uns.ac.rs.foodmateapp.models.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkingDay {
    @PrimaryKey(autoGenerate = true)
    private long workingDayId;

    private String day;
    private String timeFrom;
    private String timeTo;

    private long workingDayRestaurantId;

    public WorkingDay(String day, String timeFrom, String timeTo) {
        this.day = day;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
    }
}
