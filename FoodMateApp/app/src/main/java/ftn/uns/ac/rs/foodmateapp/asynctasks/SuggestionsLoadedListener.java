package ftn.uns.ac.rs.foodmateapp.asynctasks;

import ftn.uns.ac.rs.foodmateapp.models.relations.RestaurantAndImage;

import java.util.List;

public interface SuggestionsLoadedListener {
    void onSuggestionsLoaded(List<RestaurantAndImage> loadedSuggestions);
}
