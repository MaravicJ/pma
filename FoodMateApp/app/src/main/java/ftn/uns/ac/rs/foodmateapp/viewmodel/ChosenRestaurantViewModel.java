package ftn.uns.ac.rs.foodmateapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.entities.ChosenRestaurant;
import ftn.uns.ac.rs.foodmateapp.models.repository.ChosenRestaurantRepository;

public class ChosenRestaurantViewModel extends AndroidViewModel {
    private ChosenRestaurantRepository chosenRestaurantRepository;

    public ChosenRestaurantViewModel(@NonNull Application application) {
        super(application);
        chosenRestaurantRepository = new ChosenRestaurantRepository(application);
    }

    public LiveData<List<ChosenRestaurant>> getChosenRestaurants(long userId) {
        return chosenRestaurantRepository.getChosenRestaurantsByUserId(userId);
    }

    public LiveData<ChosenRestaurant> getChosenRestaurantById(long id) {
        return chosenRestaurantRepository.getChosenRestaurantById(id);
    }

    public void insert(ChosenRestaurant chosenRestaurant) {
        chosenRestaurantRepository.insert(chosenRestaurant);
    }

    public void delete(ChosenRestaurant chosenRestaurant) {
        chosenRestaurantRepository.delete(chosenRestaurant);
    }
}
