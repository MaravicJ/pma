package ftn.uns.ac.rs.foodmateapp.models.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.dao.ImageDao;
import ftn.uns.ac.rs.foodmateapp.models.database.AppDatabase;
import ftn.uns.ac.rs.foodmateapp.models.entities.Image;

public class ImageRepository {
    private ImageDao imageDao;

    public ImageRepository(Application application) {
        AppDatabase appDatabase = AppDatabase.getInstance(application);
        imageDao = appDatabase.imageDao();
    }

    public LiveData<Image> getImage(long imageId) {
        return imageDao.getImage(imageId);
    }

    public LiveData<List<Image>> getImages(long restaurantId) {
        return imageDao.getImages(restaurantId);
    }

    public void insert(Image image) {
        new ImageInsertAsyncTask(imageDao).execute(image);
    }

    private static class ImageInsertAsyncTask extends AsyncTask<Image, Void, Void> {
        private ImageDao imageDao;

        ImageInsertAsyncTask(ImageDao imageDao) {
            this.imageDao = imageDao;
        }

        @Override
        protected Void doInBackground(Image... images) {
            imageDao.insert(images[0]);
            return null;
        }
    }

}




