package ftn.uns.ac.rs.foodmateapp.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.parceler.Parcel;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Parcel(Parcel.Serialization.BEAN)
public class Location {
    private double longitude;
    private double latitude;
    private String city;
    private String country;
    private String address;

    // Faster distance calculation
    private double sinLongitude;
    private double sinLatitude;
    private double cosLongitude;
    private double cosLatitude;

    public Location(double longitude, double latitude, String city, String country, String address) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.city = city;
        this.country = country;
        this.sinLongitude = Math.sin(Math.toRadians(longitude));
        this.sinLatitude = Math.sin(Math.toRadians(latitude));
        this.cosLongitude = Math.cos(Math.toRadians(longitude));
        this.cosLatitude = Math.cos(Math.toRadians(latitude));
        this.address = address;
    }
}