package ftn.uns.ac.rs.foodmateapp.commons;

public class Constants {
    public static final String CHOSEN_RESTAURANT_IMAGE = "chosen_restaurant_image";
    public static String CHOSEN_RESTAURANT = "chosen_restaurant";
    public static String MENU_ANIMATION = "menu_animation_seen";
}
