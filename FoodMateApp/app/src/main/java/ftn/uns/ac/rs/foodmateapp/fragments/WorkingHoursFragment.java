package ftn.uns.ac.rs.foodmateapp.fragments;

import android.graphics.Typeface;
import android.os.Bundle;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.models.entities.WorkingDay;

public class WorkingHoursFragment extends DialogFragment {

    public static final String TAG = "example_dialog";

    private Toolbar toolbar;
    private LinearLayout linearLayout;
    private List<WorkingDay> data;

    public static WorkingHoursFragment display(FragmentManager fragmentManager) {
        WorkingHoursFragment exampleDialog = new WorkingHoursFragment();
        exampleDialog.show(fragmentManager, TAG);
        return exampleDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_working_hours, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(v -> dismiss());
        toolbar.setTitle("Working hours");
        toolbar.setOnMenuItemClickListener(item -> {
            dismiss();
            return true;
        });
        setLayout(view);

    }

    public void setLayout(View view) {
        linearLayout = view.findViewById(R.id.workingHoursLayout);

        GridLayout header = getGridLayout(view, "Day", "Time from", "Time to", true);
        linearLayout.addView(header);

        for(WorkingDay wd: data) {
            GridLayout gridLayout = getGridLayout(view, wd.getDay(), wd.getTimeFrom(), wd.getTimeTo(), false);
            linearLayout.addView(gridLayout);
        }

    }

    private GridLayout getGridLayout(View view, String day, String timeFrom, String timeTo, boolean header) {
        GridLayout gl = new GridLayout(view.getContext());
        gl.setColumnCount(3);
        gl.setPadding(160, 0, 0, 0);
        gl.addView(getTextView(view, day, header));
        gl.addView(getTextView(view, timeFrom, header));
        gl.addView(getTextView(view, timeTo, header));
        return gl;
    }

    private TextView getTextView(View view, String text, boolean header) {
        TextView tv = new TextView(view.getContext());
        tv.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        tv.setText(text);
        tv.setWidth(250);
        tv.setHeight(150);
        if(header) {
            tv.setTypeface(null, Typeface.BOLD);
            tv.setTextSize(15);
        }
        tv.setTextColor(getResources().getColor(R.color.black));
        return tv;
    }

    public List<WorkingDay> getData() {
        return data;
    }

    public void setData(List<WorkingDay> data) {
        this.data = data;
    }
}