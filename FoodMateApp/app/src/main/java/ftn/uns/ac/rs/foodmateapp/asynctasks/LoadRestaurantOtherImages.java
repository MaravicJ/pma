package ftn.uns.ac.rs.foodmateapp.asynctasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.OkHttpClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.commons.ImageAction;
import ftn.uns.ac.rs.foodmateapp.dto.OtherImageDTO;
import ftn.uns.ac.rs.foodmateapp.models.entities.Image;
import ftn.uns.ac.rs.foodmateapp.security.SSL;

public class LoadRestaurantOtherImages extends AsyncTask<String, Void, List<OtherImageDTO>> {
    private List<Image> images;
    private final WeakReference<Context> contextWeakReference;
    public AsyncResponse<List<OtherImageDTO>> asyncResponse;
    private long restaurantId;

    public static final String IMAGE_LOADED_BROADCAST =  "ftn.uns.ac.rs.foodmateapp.broadcasts.IMAGE_LOADED";

    public LoadRestaurantOtherImages(List<Image> images, Context context, AsyncResponse asyncResponse) {
        this.images = images;
        this.contextWeakReference = new WeakReference<>(context);
        this.asyncResponse = asyncResponse;
    }

    @Override
    protected List<OtherImageDTO> doInBackground(String... params) {
        final String url = params[0];
        List<OtherImageDTO> list = null;
        Context context = contextWeakReference.get();
        if(context == null)
            return null;
        Bitmap image;
        int check = checkCacheImage(context);
        if(!(check == 0)) {
            list = sendImageRequest(url);
            if(list == null) {
                return new ArrayList<>();
            }
            for (OtherImageDTO i:list) {
                image = ImageAction.stringToBitMap(i.getImage());
                String cachedFileName = "restaurant_" + restaurantId + "_image_" + i.getId();
                ImageAction.saveToCache(image, cachedFileName, context);
               i.setCachedPath(cachedFileName);
            }
        }
        return list;
    }

    private int checkCacheImage(Context context){
        int result = 0;
        for (Image item: images) {
            System.out.println(item);
            if(item.getCachedPath() == null) {
                result++;
                continue;
            }
            Bitmap image = ImageAction.loadFromCache(item.getCachedPath(), context);
            if(image == null){
                result++;
            }
        }
        return result;
    }


    private List<OtherImageDTO> sendImageRequest(String url) {
        Context context = contextWeakReference.get();
        if (context == null)
            return null;

        SharedRef sharedRef = new SharedRef(context);

        String token = sharedRef.getDataString("token", null);
        HttpHeaders headers = new HttpHeaders();
        if(token != null){
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+ token);
        }

        OkHttpClient client = SSL.createClient(context);
        ClientHttpRequestFactory requestFactory = new OkHttpClientHttpRequestFactory(client);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(requestFactory);

        ResponseEntity<OtherImageDTO[]> response;
        HttpEntity<Long> entity = new HttpEntity<Long>(headers);
        final String urlWithRestaurantId = url + "/" + restaurantId;

        try {
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            response = restTemplate.exchange(urlWithRestaurantId, HttpMethod.GET, entity, OtherImageDTO[].class);
            List<OtherImageDTO> list = Arrays.asList(response.getBody());
            return list;
        } catch (HttpClientErrorException ex) {
            Log.e("LOAD_IMAGES", ex.getResponseBodyAsString() + "; Restaurant id: "
                    + restaurantId);
            return null;
        }catch(ResourceAccessException ex){
            Log.e("LOAD_IMAGES","Something went wrong! Please try again! Restaurant id: "
                    + restaurantId);
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<OtherImageDTO> result) {
        System.out.println("ssssssssss  " + result);
        asyncResponse.onTaskCompleted(result);
    }

    public long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(long restaurantId) {
        this.restaurantId = restaurantId;
    }
}
