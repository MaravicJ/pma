package ftn.uns.ac.rs.foodmateapp.models.relations;

import androidx.room.Embedded;
import androidx.room.Relation;
import ftn.uns.ac.rs.foodmateapp.models.entities.Category;
import ftn.uns.ac.rs.foodmateapp.models.entities.MenuItem;

import java.util.List;

public class CategoryWithMenuItems {
    @Embedded public Category category;
    @Relation(
            parentColumn = "categoryCode",
            entityColumn = "menuItemCategoryCode"
    )
    public List<MenuItem> menuItems;
}
