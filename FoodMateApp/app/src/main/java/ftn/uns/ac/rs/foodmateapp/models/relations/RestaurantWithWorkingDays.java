package ftn.uns.ac.rs.foodmateapp.models.relations;

import androidx.room.Embedded;
import androidx.room.Relation;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.WorkingDay;

import java.util.List;

public class RestaurantWithWorkingDays {
    @Embedded public Restaurant restaurant;
    @Relation(
            parentColumn = "restaurantId",
            entityColumn = "workingDayRestaurantId"
    )
    public List<WorkingDay> workingDays;
}
