package ftn.uns.ac.rs.foodmateapp.models.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.dao.WorkingDayDao;
import ftn.uns.ac.rs.foodmateapp.models.database.AppDatabase;
import ftn.uns.ac.rs.foodmateapp.models.entities.WorkingDay;

public class WorkingDayRepository {
    private WorkingDayDao workingDayDao;

    public WorkingDayRepository(Application application) {
        AppDatabase appDatabase = AppDatabase.getInstance(application);
        workingDayDao = appDatabase.workingDayDao();
    }

    public LiveData<List<WorkingDay>> getWorkingDayByRestaurantId(long restaurantId) {
        return workingDayDao.getWorkingDayByRestaurantId(restaurantId);
    }
}




