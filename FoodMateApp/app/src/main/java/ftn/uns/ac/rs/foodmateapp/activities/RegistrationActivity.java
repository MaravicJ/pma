package ftn.uns.ac.rs.foodmateapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Random;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.asynctasks.AsyncResponse;
import ftn.uns.ac.rs.foodmateapp.asynctasks.GenericAsyncTask;
import ftn.uns.ac.rs.foodmateapp.commons.Permissions;
import ftn.uns.ac.rs.foodmateapp.dto.RegistrationDTO;
import ftn.uns.ac.rs.foodmateapp.dto.SMS;

public class RegistrationActivity extends AppCompatActivity implements AsyncResponse {

    RegistrationDTO user;
    String phoneNumber;
    ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        setUpData();
        checkPermissionPhone();

    }

    public void setUpData(){
        TextView textView = findViewById(R.id.sign_in);
        textView.setText(getString(R.string.login));

        spinner = findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);
    }

    public void checkPermissionPhone(){
        Permissions permissions = new Permissions(this);
        if (!permissions.checkPermission(Manifest.permission.SEND_SMS) && !permissions.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE}, 123);
        }

        final EditText phoneNumberInput = findViewById(R.id.phone);
        TelephonyManager tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(RegistrationActivity.this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegistrationActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE}, 123);
        }else{
            String PhoneNumber = tel.getLine1Number();
            phoneNumberInput.setText(PhoneNumber);
        }
    }

    public void registration(View view) {

        String message = "";

        EditText usernameInput = findViewById(R.id.username);
        String username = usernameInput.getText().toString();
        if(checkInput(username)){
            message = getString(R.string.error_username_empty);
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            return;
        }

        EditText passwordInput = findViewById(R.id.password);
        String password = passwordInput.getText().toString();
        if(checkInput(password)){
            message = getString(R.string.error_password_empty);
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            return;
        }

        EditText emailInput = findViewById(R.id.email);
        String email = emailInput.getText().toString();
        if(checkInput(email)){
            message = getString(R.string.error_email_empty);
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            return;
        }

        EditText phoneNumberInput = findViewById(R.id.phone);
        phoneNumber = phoneNumberInput.getText().toString();
        if(checkInput(phoneNumber)){
            message = getString(R.string.error_phone_number_empty);
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            return;
        }

        EditText firstNameInput = findViewById(R.id.firstName);
        EditText lastNameInput = findViewById(R.id.lastName);
        String firstName = firstNameInput.getText().toString();
        String lastName = lastNameInput.getText().toString();

        user = new RegistrationDTO(username, password, email, phoneNumber, firstName, lastName);

        showAlertPhoneNumber(phoneNumber);
    }

    private void showAlertPhoneNumber(final String phoneNumber){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Confirm");
        builder.setMessage("Are you sure your phone number is " + phoneNumber + " ?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                spinner.setVisibility(View.VISIBLE);
                final String uri = getString(R.string.singup_url);
                GenericAsyncTask<RegistrationDTO, String> asyncTask =
                        new GenericAsyncTask<>(RegistrationActivity.this, String.class, RegistrationActivity.this);
                asyncTask.setObjectToSend(user);
                asyncTask.execute(uri, null);
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void onTaskCompleted(Object result) {
        final int[] code = new int[1];
        if(result == null){
            return;
        }
        if(result.equals("Successfully registered!")){
            Permissions permissions = new Permissions(this);
            if (permissions.checkPermission(Manifest.permission.SEND_SMS)) {
                try {
                    Random rand = new Random();
                    code[0] = 1000 + rand.nextInt(9000);
                    String text = getString(R.string.message) + code[0];
//                SmsManager smgr = SmsManager.getDefault();
//                smgr.sendTextMessage(phoneNumber, null, text, null, null);
//                Toast.makeText(RegistrationActivity.this, getString(R.string.sms_success), Toast.LENGTH_SHORT).show();

                    GenericAsyncTask<SMS, String> asyTask = new GenericAsyncTask<SMS,String>(
                            RegistrationActivity.this, String.class, RegistrationActivity.this);
                    asyTask.setObjectToSend(new SMS(phoneNumber,text));
                    String uri = getString(R.string.send_message_url);
                    asyTask.execute(uri, null);


                } catch (Exception e) {
                    Toast.makeText(RegistrationActivity.this, getString(R.string.sms_fail), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(RegistrationActivity.this, getString(R.string.permission_denied), Toast.LENGTH_SHORT).show();

            }
            Intent intent = new Intent(RegistrationActivity.this, VerificationCodeActivity.class);
            intent.putExtra("code", String.valueOf(code[0]));
            intent.putExtra("username", user.getUsername());
            intent.putExtra("password", user.getPassword());
            startActivity(intent);
        }

    }

    private boolean checkInput(String checkItem){
        if(checkItem.equals("")){
            return true;
        }
        return false;
    }

    public void signIn(View view){
        TextView textView = findViewById(R.id.sign_in);
        String text = textView.getText().toString();
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, text.length(),0);
        textView.setText(content);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

}
