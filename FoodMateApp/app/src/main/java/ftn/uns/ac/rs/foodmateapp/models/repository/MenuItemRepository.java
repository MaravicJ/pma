package ftn.uns.ac.rs.foodmateapp.models.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.dao.MenuItemDao;
import ftn.uns.ac.rs.foodmateapp.models.database.AppDatabase;
import ftn.uns.ac.rs.foodmateapp.models.entities.MenuItem;

public class MenuItemRepository {

    private MenuItemDao menuItemDao;

    public MenuItemRepository(Application application) {
        AppDatabase appDatabase = AppDatabase.getInstance(application);
        menuItemDao = appDatabase.menuItemDao();
    }

    public LiveData<List<MenuItem>> getMenuItems(long restaurant_id){
        return menuItemDao.getMenuItems(restaurant_id);
    }

}
