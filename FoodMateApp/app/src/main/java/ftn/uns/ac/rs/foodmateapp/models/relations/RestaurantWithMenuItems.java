package ftn.uns.ac.rs.foodmateapp.models.relations;

import androidx.room.Embedded;
import androidx.room.Relation;
import ftn.uns.ac.rs.foodmateapp.models.entities.MenuItem;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;

import java.util.List;

public class RestaurantWithMenuItems {
    @Embedded public Restaurant restaurant;
    @Relation(
            parentColumn = "restaurantId",
            entityColumn = "menuItemRestaurantId"
    )
    public List<MenuItem> menuItems;
}
