package ftn.uns.ac.rs.foodmateapp.models;

import android.graphics.Bitmap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Item {
    long restaurantId;
    String title;
    Bitmap image;
    String location;
    boolean selected;
}
