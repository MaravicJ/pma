package ftn.uns.ac.rs.foodmateapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.asynctasks.AsyncResponse;
import ftn.uns.ac.rs.foodmateapp.asynctasks.GenericAsyncTask;
import ftn.uns.ac.rs.foodmateapp.commons.Permissions;
import ftn.uns.ac.rs.foodmateapp.dto.AuthenticationResponseDTO;
import ftn.uns.ac.rs.foodmateapp.dto.UserDTO;
import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class VerificationCodeActivity extends AppCompatActivity implements AsyncResponse {

    String code;
    UserDTO user = new UserDTO();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_code);

        focusDigits();
        checkPermissionPhone();

    }

    public void checkPermissionPhone(){
        Permissions permissions = new Permissions(this);
        if(!permissions.checkPermission(Manifest.permission.RECEIVE_SMS)){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECEIVE_SMS}, 123);
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(receiver, filter);

        Bundle b = getIntent().getExtras();
        code = b.getString("code");
        user.setUsername(b.getString("username"));
        user.setPassword(b.getString("password"));
    }

    @Override
    protected void onPause(){
        super.onPause();
        unregisterReceiver(receiver);
    }

    BroadcastReceiver receiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
                Bundle bundle = intent.getExtras();
                SmsMessage[] msgs = null;
                if(bundle!= null){
                    Object[] pdu_Objects = (Object[]) bundle.get("pdus");
                    if (pdu_Objects != null) {

                        for (Object aObject : pdu_Objects) {
                            SmsMessage currentSMS = getIncomingMessage(aObject, bundle);
                            String message = currentSMS.getDisplayMessageBody();
                            String[] tokens = message.split(":");

                            fillVerificationCode(tokens[1].trim());
                        }
                        this.abortBroadcast();
                    }
                }
            }
        }
    };

    private void fillVerificationCode(String code){

        EditText number1 = findViewById(R.id.number1);
        number1.setText(Character.toString(code.charAt(0)));

        EditText number2 = findViewById(R.id.number2);
        number2.setText(Character.toString(code.charAt(1)));

        EditText number3 = findViewById(R.id.number3);
        number3.setText(Character.toString(code.charAt(2)));

        EditText number4 = findViewById(R.id.number4);
        number4.setText(Character.toString(code.charAt(3)));

        Button button = findViewById(R.id.verify_button);
        button.callOnClick();

    }

    private SmsMessage getIncomingMessage(Object aObject, Bundle bundle) {
        SmsMessage currentSMS;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String format = bundle.getString("format");
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject, format);
        } else {
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject);
        }
        return currentSMS;
    }

    private void focusDigits(){

        final EditText number1 = findViewById(R.id.number1);
        final EditText number2 = findViewById(R.id.number2);
        final EditText number3 = findViewById(R.id.number3);
        final EditText number4 = findViewById(R.id.number4);

        number1.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==1) {
                    number2.requestFocus();
                }else{
                    number1.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        number2.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==1) {
                    number3.requestFocus();
                }else{
                    number1.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        number3.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==1) {
                    number4.requestFocus();
                }else{
                    number2.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

        number4.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (s.length() ==0) {
                    number3.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
        });

    }

    public void verifyAccount(View view) {
        String message = "";

        EditText number1Input = findViewById(R.id.number1);
        EditText number2Input = findViewById(R.id.number2);
        EditText number3Input = findViewById(R.id.number3);
        EditText number4Input = findViewById(R.id.number4);

        String number1 = number1Input.getText().toString();
        String number2 = number2Input.getText().toString();
        String number3 = number3Input.getText().toString();
        String number4 = number4Input.getText().toString();

        if(number1.equals(Character.toString(code.charAt(0))) && number2.equals(Character.toString(code.charAt(1)))
                && number3.equals(Character.toString(code.charAt(2))) && number4.equals(Character.toString(code.charAt(3)))){
            final String uri = getString(R.string.verify_url);
            GenericAsyncTask<UserDTO, AuthenticationResponseDTO> asyncTask =
                    new GenericAsyncTask<>(this, AuthenticationResponseDTO.class, this);
            asyncTask.setObjectToSend(user);
            asyncTask.execute(uri, null);

        }else{
            message = getString(R.string.error_verify);
            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onTaskCompleted(Object result) {

        AuthenticationResponseDTO returnedResult = (AuthenticationResponseDTO) result;
        SharedRef sharedRef = new SharedRef(VerificationCodeActivity.this);
        sharedRef.saveDataString("username",  returnedResult.getUsername());
        sharedRef.saveDataString("token",  returnedResult.getToken());
        sharedRef.saveDataString("login",  "true");

        Intent intent = new Intent(VerificationCodeActivity.this, TutorialActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        String message = getString(R.string.successfully_registered);
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

    }

}
