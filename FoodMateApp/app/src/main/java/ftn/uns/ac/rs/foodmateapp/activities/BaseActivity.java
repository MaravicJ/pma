package ftn.uns.ac.rs.foodmateapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;

public class BaseActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.editProfile:
                System.out.println("editProfile");
                intent = new Intent(getApplicationContext(), EditProfileActivity.class);
                startActivity(intent);
                return true;
            case R.id.settings:
                System.out.println("settings");
                intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.chosenRestaurants:
                System.out.println("chosenRestaurants");
                intent = new Intent(getApplicationContext(), ChosenRestaurantsActivity.class);
                startActivity(intent);
                return true;
            case R.id.logout:
                SharedRef sharedRef = new SharedRef(this);
                sharedRef.saveDataString("login","false");
                sharedRef.saveDataString("token","");
                intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                Toast.makeText(this,"Successfully logout!",Toast.LENGTH_LONG).show();
                return true;
        }
        return false;
    }
}
