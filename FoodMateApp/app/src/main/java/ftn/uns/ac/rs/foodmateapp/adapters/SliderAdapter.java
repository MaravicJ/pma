package ftn.uns.ac.rs.foodmateapp.adapters;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.models.SliderItem;
import lombok.NonNull;

public class SliderAdapter extends RecyclerView.Adapter<SliderAdapter.SliderViewAdapter>{

    private List<SliderItem> sliderItems;
    private ViewPager2 viewPager2;

    public SliderAdapter(List<SliderItem> sliderItems, ViewPager2 viewPager2) {
        this.sliderItems = sliderItems;
        this.viewPager2 = viewPager2;
    }

    @androidx.annotation.NonNull
    @Override
    public SliderViewAdapter onCreateViewHolder(@androidx.annotation.NonNull ViewGroup parent, int viewType) {
        return new SliderViewAdapter(LayoutInflater.from(parent.getContext()).inflate(R.layout.slide_item_container, parent, false));
    }

    @Override
    public void onBindViewHolder(@androidx.annotation.NonNull SliderViewAdapter holder, int position) {
        holder.setImageView(sliderItems.get(position));
        if(position == sliderItems.size() - 2) {
            viewPager2.post(runnable);
        }
    }

    @Override
    public int getItemCount() {
        return sliderItems.size();
    }

    class SliderViewAdapter extends RecyclerView.ViewHolder{
        private RoundedImageView imageView;

        SliderViewAdapter(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageSlide);
        }

        public void setImageView(SliderItem sliderItem) {
            this.imageView.setImageBitmap(sliderItem.getImage());
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            sliderItems.addAll(sliderItems);
            notifyDataSetChanged();
        }
    };
}
