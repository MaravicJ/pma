package ftn.uns.ac.rs.foodmateapp.models.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MenuItem {
    @PrimaryKey(autoGenerate = true)
    private long menuItemId;

    private String name;
    private String description;
    private double cost;

    private long menuItemRestaurantId;
    private String menuItemCategoryCode;
    private long menuItemImageId;

    public String getCachedImageFilename() {

        return "menu_item_image_" + menuItemImageId;
    }
}
