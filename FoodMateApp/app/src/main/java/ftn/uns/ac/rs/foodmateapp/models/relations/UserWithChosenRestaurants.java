package ftn.uns.ac.rs.foodmateapp.models.relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.entities.ChosenRestaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.User;

public class UserWithChosenRestaurants {
    @Embedded public User user;
    @Relation(
            parentColumn = "userId",
            entityColumn = "restaurantId"
    )
    public List<ChosenRestaurant> chosenRestaurants;
}
