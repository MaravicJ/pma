package ftn.uns.ac.rs.foodmateapp.models.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.parceler.Parcel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Parcel(Parcel.Serialization.BEAN)
public class User {
    @PrimaryKey(autoGenerate = true)
    private long userId;
    private String username;
}
