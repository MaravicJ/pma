package ftn.uns.ac.rs.foodmateapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import ftn.uns.ac.rs.foodmateapp.models.entities.User;
import ftn.uns.ac.rs.foodmateapp.models.repository.UserRepository;

public class UserViewModel extends AndroidViewModel {
    private UserRepository userRepository;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(application);
    }

    public LiveData<User> getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    public void insert(User user) {
        userRepository.insert(user);
    }
}
