package ftn.uns.ac.rs.foodmateapp.models.relations;

import androidx.room.Embedded;
import androidx.room.Relation;
import ftn.uns.ac.rs.foodmateapp.models.entities.Image;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;

import java.util.List;

public class RestaurantWithImages {
    @Embedded public Restaurant restaurant;
    @Relation(
            parentColumn = "restaurantId",
            entityColumn = "imageRestaurantId"
    )
    public List<Image> images;
}
