package ftn.uns.ac.rs.foodmateapp.models.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.dao.RestaurantDao;
import ftn.uns.ac.rs.foodmateapp.models.database.AppDatabase;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.RestaurantFoodPreference;

public class RestaurantRepository {
    private RestaurantDao restaurantDao;

    public RestaurantRepository(Application application) {
        AppDatabase appDatabase = AppDatabase.getInstance(application);
        restaurantDao = appDatabase.restaurantDao();
    }

    public Restaurant getRestaurant(String name) {
        return restaurantDao.getRestaurant(name);
    }

    public LiveData<Restaurant> getRestaurantById(long restaurantId) {
        return restaurantDao.getRestaurantById(restaurantId);
    }

    public LiveData<List<RestaurantFoodPreference>> getRestaurantFoodPreferences(long restaurantId) {
        return restaurantDao.getRestaurantFoodPreferences(restaurantId);
    }

    public void insert(Restaurant restaurant) {
        new RestaurantInsertAsyncTask(restaurantDao).execute(restaurant);
    }

    public void insertAll(Restaurant[] restaurants) {
        new RestaurantInsertAllAsyncTask(restaurantDao).execute(restaurants);
    }

    public void insertFoodPreferences(RestaurantFoodPreference[] restaurantFoodPreferences) {
        new RestaurantInsertFoodPreferencesAsyncTask(restaurantDao).execute(restaurantFoodPreferences);
    }

    private static class RestaurantInsertAsyncTask extends AsyncTask<Restaurant, Void, Void> {
        private RestaurantDao restaurantDao;

        RestaurantInsertAsyncTask(RestaurantDao restaurantDao) {
            this.restaurantDao = restaurantDao;
        }

        @Override
        protected Void doInBackground(Restaurant... restaurants) {
            restaurantDao.insert(restaurants[0]);
            return null;
        }
    }

    private static class RestaurantInsertAllAsyncTask extends AsyncTask<Restaurant[], Void, Void> {
        private RestaurantDao restaurantDao;

        RestaurantInsertAllAsyncTask(RestaurantDao restaurantDao) {
            this.restaurantDao = restaurantDao;
        }

        @Override
        protected Void doInBackground(Restaurant[]... restaurants) {
            restaurantDao.insertAll(restaurants[0]);
            return null;
        }
    }

    private static class RestaurantInsertFoodPreferencesAsyncTask extends AsyncTask<RestaurantFoodPreference[], Void, Void> {
        private RestaurantDao restaurantDao;

        RestaurantInsertFoodPreferencesAsyncTask(RestaurantDao restaurantDao) {
            this.restaurantDao = restaurantDao;
        }

        @Override
        protected Void doInBackground(RestaurantFoodPreference[]... restaurants) {
            restaurantDao.insertFoodPreferences(restaurants[0]);
            return null;
        }
    }
}




