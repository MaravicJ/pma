package ftn.uns.ac.rs.foodmateapp.models.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import ftn.uns.ac.rs.foodmateapp.models.dao.UserDao;
import ftn.uns.ac.rs.foodmateapp.models.database.AppDatabase;
import ftn.uns.ac.rs.foodmateapp.models.entities.User;

public class UserRepository {
    private UserDao userDao;
    public User user;

    public UserRepository(Application application) {
        AppDatabase appDatabase = AppDatabase.getInstance(application);
        userDao = appDatabase.userDao();
    }

    public void insert(User user) {
        new UserInsertAsyncTask(userDao).execute(user);
    }

    public LiveData<User> getUserByUsername(String username) {
        return userDao.getUserByUsername(username);
    }

    private static class UserInsertAsyncTask extends AsyncTask<User, Void, User> {
        private UserDao userDao;

        UserInsertAsyncTask(UserDao userDao) {
            this.userDao = userDao;
        }

        @Override
        protected User doInBackground(User... user) {
            userDao.insert(user[0]);
            return null;
        }
    }
}




