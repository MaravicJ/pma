package ftn.uns.ac.rs.foodmateapp.exceptions;

import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

public class ResponseErrorHandler implements org.springframework.web.client.ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        System.out.println("hasError");
        return false;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        System.out.println("handleError");
    }
}
