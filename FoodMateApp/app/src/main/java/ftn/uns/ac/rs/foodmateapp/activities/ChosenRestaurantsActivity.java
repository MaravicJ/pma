package ftn.uns.ac.rs.foodmateapp.activities;

import android.Manifest;
import android.accounts.Account;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProviders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.r0adkll.slidr.Slidr;

import ftn.uns.ac.rs.foodmateapp.sync.Authenticator;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ftn.uns.ac.rs.foodmateapp.R;
import ftn.uns.ac.rs.foodmateapp.SharedRef;
import ftn.uns.ac.rs.foodmateapp.adapters.GridViewAdapter;
import ftn.uns.ac.rs.foodmateapp.asynctasks.AsyncResponse;
import ftn.uns.ac.rs.foodmateapp.asynctasks.GenericAsyncTask;
import ftn.uns.ac.rs.foodmateapp.commons.Constants;
import ftn.uns.ac.rs.foodmateapp.commons.ImageAction;
import ftn.uns.ac.rs.foodmateapp.commons.LocationUtils;
import ftn.uns.ac.rs.foodmateapp.commons.Permissions;
import ftn.uns.ac.rs.foodmateapp.dto.NewUserChosenRestaurantDTO;
import ftn.uns.ac.rs.foodmateapp.dto.RestaurantDTO;
import ftn.uns.ac.rs.foodmateapp.models.Item;
import ftn.uns.ac.rs.foodmateapp.models.entities.ChosenRestaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.Restaurant;
import ftn.uns.ac.rs.foodmateapp.models.entities.User;
import ftn.uns.ac.rs.foodmateapp.viewmodel.ChosenRestaurantViewModel;
import ftn.uns.ac.rs.foodmateapp.viewmodel.ImageViewModel;
import ftn.uns.ac.rs.foodmateapp.viewmodel.RestaurantViewModel;
import ftn.uns.ac.rs.foodmateapp.viewmodel.UserViewModel;

public class ChosenRestaurantsActivity extends AppCompatActivity implements AsyncResponse {
    private SharedRef sharedRef;

    private GridView gridview;
    private GridViewAdapter gridviewAdapter;
    private ArrayList<Item> data = new ArrayList<>();

    private UserViewModel userViewModel;

    private ImageViewModel imageViewModel;
    private RestaurantViewModel restaurantViewModel;
    private ChosenRestaurantViewModel chosenRestaurantsViewModel;

    private android.location.Location userLocation;
    private FusedLocationProviderClient fusedLocationClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedRef = new SharedRef(this);

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        restaurantViewModel = ViewModelProviders.of(this).get(RestaurantViewModel.class);
        chosenRestaurantsViewModel = ViewModelProviders.of(this).get(ChosenRestaurantViewModel.class);
        imageViewModel = ViewModelProviders.of(this).get(ImageViewModel.class);

        setContentView(R.layout.activity_chosen_restaurants);
        setTitle("Chosen restaurants");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        setUpPermissions();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLocation();
        showChosenRestaurants();

        Slidr.attach(this);
    }


    public void setUpPermissions() {
        Permissions permissions = new Permissions(this);
        if (!permissions.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        }
    }

    private void getLocation() {
        fusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
            if (location != null) {
                userLocation = location;
            }
        });
    }

    private void showChosenRestaurants() {
        userViewModel.getUserByUsername(sharedRef.getDataString("username", "null")).observe(this, user -> {
            chosenRestaurantsViewModel.getChosenRestaurants(user.getUserId()).observe(this, chosenRestaurants -> {
                if(chosenRestaurants.size() == 0) {
                    System.out.println("Sending request...");
                    getRestaurantsData();
                } else {
                    System.out.println("Reading from SQLite db...");
                    readFromSQLite();
                }
            });
        });

    }

    private void getRestaurantsData() {
        final String url = getString(R.string.get_chosen);

        GenericAsyncTask<String, List> asyncTask = new GenericAsyncTask<>(this, List.class, this);
        asyncTask.setObjectToSend(sharedRef.getDataString("username", "null"));
        asyncTask.execute(url, sharedRef.getDataString("token", null));
    }

    @Override
    public void onTaskCompleted(Object result) {
        if(result instanceof String) {
            Toast.makeText(this, (String) result, Toast.LENGTH_SHORT).show();
        } else {
            addRequestedChosenRestaurants(result);
        }

        isDataListEmpty();
    }

    private void isDataListEmpty() {
        if(data.size() == 0) {
            Toast.makeText(this, "Your chosen restaurants list is empty.", Toast.LENGTH_SHORT).show();
        }
    }

    public void setButtonVisibility(boolean visibility) {
        ImageButton deleteButton = findViewById(R.id.deleteButton);
        if(visibility) {
            deleteButton.setVisibility(View.VISIBLE);
        } else {
            deleteButton.setVisibility(View.INVISIBLE);
        }
    }

    private void addRequestedChosenRestaurants(Object result) {
        List<String> convertList = (List<String>) result;
        ObjectMapper mapper = new ObjectMapper();
        List<RestaurantDTO> restaurantDTOs = mapper.convertValue(convertList, new TypeReference<List<RestaurantDTO>>(){});

        for(RestaurantDTO r: restaurantDTOs) {
            Bitmap bitmap = ImageAction.stringToBitMap(r.getMainImage().getImage());
            restaurantViewModel.getRestaurantById(r.getRestaurantId()).observe(this, restaurant -> {
                android.location.Location restaurantLocation = new android.location.Location("restaurantLocation");
                restaurantLocation.setLatitude(restaurant.getLocation().getLatitude());
                restaurantLocation.setLongitude(restaurant.getLocation().getLongitude());
                float distanceToRestaurant = userLocation.distanceTo(restaurantLocation);
                String distance = LocationUtils.getDistanceString(distanceToRestaurant) + " away";
                Item item = new Item(r.getRestaurantId(), r.getName(), bitmap, distance, false);
                data.add(item);
            });
        }

        saveChosenToDB();
        setAdapter();
    }

    private void saveChosenToDB() {
        for(Item item: data) {
            addChosenToSQLite(item.getRestaurantId());
        }
    }

    private void addChosenToSQLite(long restaurantId) {
        String username = sharedRef.getDataString("username", "null");
        userViewModel.getUserByUsername(username).observe(this, user -> {
            chosenRestaurantsViewModel.insert(new ChosenRestaurant(user.getUserId(), restaurantId));});
    }

    private void readFromSQLite() {
        SharedRef sharedRef = new SharedRef(this);
        userViewModel.getUserByUsername(sharedRef.getDataString("username", null)).observe(this, user -> getData(user));
    }

    private void getData(User user) {
        chosenRestaurantsViewModel.getChosenRestaurants(user.getUserId()).observe(this, chosenRestaurants -> {
            data.clear();
            for(ChosenRestaurant cr: chosenRestaurants) {
                getRestaurant(cr.getRestaurantId());
            }
        });
    }

    public void getRestaurant(long restaurantId) {
        restaurantViewModel.getRestaurantById(restaurantId).observe(this, restaurant -> {
            getImage(restaurant);
            setAdapter();
        });
    }

    private void getImage(Restaurant restaurant) {
        Bitmap bitmap = ImageAction.loadFromCache("restaurant_" + restaurant.getRestaurantId(), this);
        android.location.Location restaurantLocation = new android.location.Location("restaurantLocation");
        restaurantLocation.setLatitude(restaurant.getLocation().getLatitude());
        restaurantLocation.setLongitude(restaurant.getLocation().getLongitude());
        float distanceToRestaurant = userLocation.distanceTo(restaurantLocation);
        String distance = LocationUtils.getDistanceString(distanceToRestaurant) + " away";
        Item item = new Item(restaurant.getRestaurantId(), restaurant.getName(), bitmap, distance, false);
        data.add(item);
    }

    public void setDeleteAction(int position, View view) {
        Item selectItem = data.get(position);
        ConstraintLayout constraintLayout = (ConstraintLayout) view;
        if(selectItem.isSelected() == true) {
            selectItem.setSelected(false);
            constraintLayout.setBackgroundColor(getResources().getColor(R.color.white));
        } else {
            selectItem.setSelected(true);
            constraintLayout.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }

        boolean selection = checkSelection();
        setButtonVisibility(selection);
    }

    private void setGridItemClickListener() {
        gridview = findViewById(R.id.gridView);
        gridview.setOnItemClickListener((parent, view, position, id) -> {
            boolean selection = checkSelection();
            if (selection) {
                setDeleteAction(position, view);
                return;
            }

            restaurantViewModel.getRestaurantById(data.get(position).getRestaurantId()).observe(this, restaurant -> {
                Intent intent = new Intent(getApplicationContext(), RestaurantInfoActivity.class);
                intent.putExtra(Constants.CHOSEN_RESTAURANT, Parcels.wrap(restaurant));
                startActivity(intent);
            });
        });

        gridview.setOnItemLongClickListener((parent, view, position, id) -> {
            setDeleteAction(position, view);
            return true;
        });
    }

    private boolean checkSelection() {
        for(Item i: data) {
            if(i.isSelected() == true) {
                return true;
            }
        }
        return false;
    }

    private void setDataAdapter() {
        gridview = findViewById(R.id.gridView);
        gridviewAdapter = new GridViewAdapter(getApplicationContext(), R.layout.chosen_restaurant_card, data);
        gridview.setAdapter(gridviewAdapter);
    }

    private void setAdapter() {
        setDataAdapter();
        setGridItemClickListener();
    }

    public void deleteSelectedRestaurants(View view) {
        deleteSelectedRestaurantsServer();
        deleteSelectedRestaurantsSQLite();
    }

    private void deleteSelectedRestaurantsSQLite() {
        for(Item i: data) {
            if (i.isSelected() == true) {
                chosenRestaurantsViewModel.getChosenRestaurantById(i.getRestaurantId()).observe(this, chosenRestaurant -> {
                    if(chosenRestaurant == null) {
                        return;
                    }
                    chosenRestaurantsViewModel.delete(chosenRestaurant);
                });
            }
        }
    }

    public void deleteSelectedRestaurantsServer() {
        final String url = getString(R.string.delete_chosen);
        List<NewUserChosenRestaurantDTO> newUserChosenRestaurantDTOs = getChosenRestaurantsForDeletion();

        GenericAsyncTask<List, String> asyncTask = new GenericAsyncTask<>(this, String.class, this);
        asyncTask.setObjectToSend(newUserChosenRestaurantDTOs);
        asyncTask.execute(url, sharedRef.getDataString("token", null));
    }

    private List<NewUserChosenRestaurantDTO> getChosenRestaurantsForDeletion() {
        List< NewUserChosenRestaurantDTO> newUserChosenRestaurantDTOs = new ArrayList<>();
        for(Item i: data) {
            if(i.isSelected() == true) {
                restaurantViewModel.getRestaurantById(i.getRestaurantId()).observe(this, restaurant -> {
                    NewUserChosenRestaurantDTO newUserChosenRestaurantDTO = new NewUserChosenRestaurantDTO(sharedRef.getDataString("username", null), restaurant.getName());
                    newUserChosenRestaurantDTOs.add(newUserChosenRestaurantDTO);
                });
            }
        }
        return newUserChosenRestaurantDTOs;
    }

}