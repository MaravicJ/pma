package ftn.uns.ac.rs.foodmateapp.models.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import ftn.uns.ac.rs.foodmateapp.models.entities.Image;

@Dao
public interface ImageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Image[] images);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Image... images);

    @Query("SELECT * FROM Image WHERE :imageId = imageId")
    LiveData<Image> getImage(long imageId);

    @Query("SELECT * FROM Image WHERE :imageRestaurantId = imageRestaurantId")
    LiveData<List<Image>> getImages(long imageRestaurantId);
}
