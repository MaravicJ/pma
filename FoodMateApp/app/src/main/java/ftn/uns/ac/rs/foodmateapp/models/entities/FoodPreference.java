package ftn.uns.ac.rs.foodmateapp.models.entities;


import java.util.HashMap;
import java.util.Map;

public enum FoodPreference {
    regular("regular"),
    vegetarian("vegetarian"),
    vegan("vegan"),
    glutenFree("gluten free"),
    nutFree("nut free"),
    kosher("kosher"),
    halal("halal");

    private String code;

    FoodPreference(String code) {
        this.code = code;
    }

    private static final Map<String, FoodPreference> BY_CODE = new HashMap<>();

    public static FoodPreference valueOfCode(String code){ return BY_CODE.get(code); }

    static {
        for (FoodPreference f: values()) {
            BY_CODE.put(f.code, f);
        }
    }

    public String getCode() {
        return code;
    }
}