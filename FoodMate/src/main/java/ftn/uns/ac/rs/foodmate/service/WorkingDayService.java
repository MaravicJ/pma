package ftn.uns.ac.rs.foodmate.service;

import ftn.uns.ac.rs.foodmate.models.WorkingDay;

import java.util.List;

public interface WorkingDayService {
    void save(List<WorkingDay> workingDay);
}
