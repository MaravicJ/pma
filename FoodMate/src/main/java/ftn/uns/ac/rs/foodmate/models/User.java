package ftn.uns.ac.rs.foodmate.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String username;
    @Column(nullable = false)
    private String password;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column(nullable = false)
    private String phoneNumber;
    @Column
    private boolean verify;
    @Column(nullable = false)
    @Email
    private String email;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @ToString.Exclude
    private List<Reservation> reservations = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name="user_restaurants",
            joinColumns=@JoinColumn(name="user_id"),
            inverseJoinColumns=@JoinColumn(name="restaurant_id"))
    private List<Restaurant> chosenRestaurants = new ArrayList<>();

    @ManyToOne(fetch=FetchType.LAZY)
    private Role role;

    public void addChosenRestaurant(Restaurant restaurant) {
        for(Restaurant r: chosenRestaurants) {
            if(r.getName().equals(restaurant.getName())) {
                return;
            }
        }
        chosenRestaurants.add(restaurant);
    }

    public void deleteChosenRestaurant(Restaurant restaurant) {
        for(int i = 0; i < chosenRestaurants.size(); i++) {
            if(chosenRestaurants.get(i).getName().equals(restaurant.getName())) {
                chosenRestaurants.remove(i);
                return;
            }
        }
    }
}
