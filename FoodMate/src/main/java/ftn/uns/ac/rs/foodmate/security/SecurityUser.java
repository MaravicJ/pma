package ftn.uns.ac.rs.foodmate.security;

import ftn.uns.ac.rs.foodmate.models.User;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Data
//@NoArgsConstructor
public class SecurityUser implements UserDetails {
	private static final long serialVersionUID = -949811899438278427L;

	private Long id;
	private String username;
	private String password;
	private String email;
	private String firstname;
	private String lastname;
	private Collection<? extends GrantedAuthority> authorities;

	public SecurityUser(User user , Collection<? extends GrantedAuthority> authorities) {
		super();
		this.id = user.getId();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.email = user.getEmail();
		this.firstname = user.getFirstName();
		this.lastname = user.getLastName();
		this.authorities = authorities;
	}

	public static SecurityUser build(User user) {
		List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority(user.getRole().getName().name()));

		return new SecurityUser(user,authorities);
	}
	@Override
	public String getUsername() {
		return this.username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getPassword() {
		return this.password;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		SecurityUser user = (SecurityUser) o;
		return Objects.equals(id, user.id);
	}
}
