package ftn.uns.ac.rs.foodmate.service.implementations;

import com.twilio.Twilio;
import ftn.uns.ac.rs.foodmate.commons.TwilioConst;
import ftn.uns.ac.rs.foodmate.dto.ResetPasswordResponseDTO;
import ftn.uns.ac.rs.foodmate.dto.SMS;
import ftn.uns.ac.rs.foodmate.dto.UserDataDTO;
import ftn.uns.ac.rs.foodmate.dto.RegistrationDTO;
import ftn.uns.ac.rs.foodmate.exceptions.MyException;
import ftn.uns.ac.rs.foodmate.models.Role;
import ftn.uns.ac.rs.foodmate.models.User;
import ftn.uns.ac.rs.foodmate.models.UserRole;
import ftn.uns.ac.rs.foodmate.repository.RoleRepository;
import ftn.uns.ac.rs.foodmate.repository.UserRepository;
import ftn.uns.ac.rs.foodmate.security.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import com.twilio.Twilio;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import java.net.URI;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found -> username : " + username));

        return SecurityUser.build(user);
    }

    public Optional<User> findByUsername(String username){
        return userRepository.findByUsername(username);
    }

    public Optional<User> findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public Optional<User> findByPhoneNumber(String phoneNumber){
        return userRepository.findByPhoneNumber(phoneNumber);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User registration(RegistrationDTO userDTO){

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        User user = new User();
        user.setEmail(userDTO.getEmail());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setPassword(encoder.encode(userDTO.getPassword()));
        user.setUsername(userDTO.getUsername());
        user.setPhoneNumber(userDTO.getPhoneNumber());
        user.setVerify(false);

        Role role = roleRepository.findByName(UserRole.USER_ROLE);
        user.setRole(role);

        return userRepository.save(user);
    }

    public void save(User user) {
        userRepository.save(user);
    }

    public User editUser(UserDataDTO userDTO) throws MyException {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        if(!userDTO.getUsername().equals(username) && usernameExists(userDTO.getUsername())) {
            throw new MyException("Username already exists! Choose another one.");
        }

        Optional<User> editUser = userRepository.findByUsername(username);
        editUser.get().setEmail(userDTO.getEmail());
        editUser.get().setFirstName(userDTO.getFirstName());
        editUser.get().setLastName(userDTO.getLastName());
        if(!userDTO.getPassword().equals("")) {
            editUser.get().setPassword(encoder.encode(userDTO.getPassword()));
        }
        editUser.get().setUsername(userDTO.getUsername());
        editUser.get().setPhoneNumber(userDTO.getPhoneNumber());

        return userRepository.save(editUser.get());
    }

    public UserDataDTO getUserDTO(User user) {
        UserDataDTO userDTO = new UserDataDTO();
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setPassword("");
        userDTO.setUsername(user.getUsername());
        userDTO.setPhoneNumber(user.getPhoneNumber());
        return userDTO;
    }

    private boolean usernameExists(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if (user == null) {
            return false;
        }
        return true;
    }
    public String verify(String username){

        Optional<User> user = findByUsername(username);

        if(!user.isPresent()){
            return "";
        }

        user.get().setVerify(true);
        userRepository.save(user.get());
        return "Successful";
    }

    public ResetPasswordResponseDTO resetPassword(String email){

        Optional<User> user = userRepository.findByEmail(email);

        if(!user.isPresent()){
            return null;
        }
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String new_password = randomString(8);
        user.get().setPassword(encoder.encode(new_password));
        userRepository.save(user.get());

        return new ResetPasswordResponseDTO(new_password, user.get().getUsername());

    }

    public String randomString(int length) {
        String characterSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int randomInt = new SecureRandom().nextInt(characterSet.length());
            sb.append(characterSet.substring(randomInt, randomInt + 1));
        }
        return sb.toString();
    }

    public void send(SMS sms) {
        Twilio.init(TwilioConst.ACCOUNT_SID, TwilioConst.AUTH_TOKEN);
        String phoneTo;
        if(sms.getTo().startsWith("0")){
            phoneTo = "+381" + sms.getTo().substring(1, sms.getTo().length());
        }else{
            phoneTo = sms.getTo();
        }

        Message message = Message.creator(new PhoneNumber(phoneTo), new PhoneNumber(TwilioConst.PHONE_NUMBER), sms.getMessage())
                .create();
        System.out.println("here is my id:"+message.getSid());// Unique resource ID created to manage this transaction

    }
}
