package ftn.uns.ac.rs.foodmate.service;

import ftn.uns.ac.rs.foodmate.models.Location;

public interface LocationService {
    void save(Location location);
}
