package ftn.uns.ac.rs.foodmate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResetPasswordResponseDTO {

    private String new_password;
    private String username;
}
