package ftn.uns.ac.rs.foodmate.service.implementations;

import ftn.uns.ac.rs.foodmate.dto.ImageDTO;
import ftn.uns.ac.rs.foodmate.models.Image;
import ftn.uns.ac.rs.foodmate.repository.ImageRepository;
import ftn.uns.ac.rs.foodmate.service.ImageService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    private ImageRepository imageRepository;

    public void save(Image image) {
        imageRepository.save(image);
    }

    public String getImage(String path) throws IOException {
        path = System.getProperty("user.dir") + path;
        File file = new File(path);
        FileInputStream fileInputStream = new FileInputStream(path);
        byte[] bytes = new byte[(int)file.length()];

        fileInputStream.read(bytes);
        String encodeBase64 = Base64.getEncoder().encodeToString(bytes);
        return encodeBase64;
    }

    public ImageDTO getImageDTO(Image image) throws IOException {
        return new ImageDTO(getImage(image.getPath()));
    }

    public List<ImageDTO> getImageDTOs(List<Image> images) throws IOException {
        List<ImageDTO> imageDTOs = new ArrayList<>();
        for(Image i: images) {
            imageDTOs.add(new ImageDTO(getImage(i.getPath())));
        }
        return imageDTOs;
    }

    @Override
    public Image findImage(long id) {
        Optional<Image> image = imageRepository.findById(id);
        if(image.isPresent()){
            return image.get();
        }
        return null;
    }

}
