package ftn.uns.ac.rs.foodmate.service;

import ftn.uns.ac.rs.foodmate.dto.*;
import ftn.uns.ac.rs.foodmate.models.Restaurant;

import java.io.IOException;
import java.util.List;

public interface RestaurantService {
    void save(Restaurant restaurant);
    RestaurantDTO getRestaurant(String name) throws IOException;
    Restaurant getRestaurantEntity(long id);
    List<Restaurant> findAll();
    double addRating(RatingDTO ratingDTO);
    double getRating(String restaurantName);
    List<RestaurantDTO> getUserChosenRestaurants(String username) throws IOException;
    void addUserChosenRestaurant(NewUserChosenRestaurantDTO newUserChosenRestaurantDTO);
    void deleteUserChosenRestaurant(List<NewUserChosenRestaurantDTO> newUserChosenRestaurantDTO);
    List<OtherImageDTO> getOtherImagesDTO(long restaurantId) throws IOException;
}
