package ftn.uns.ac.rs.foodmate.controller;

import ftn.uns.ac.rs.foodmate.dto.sync.SyncDTO;
import ftn.uns.ac.rs.foodmate.dto.sync.SyncRequestDTO;
import ftn.uns.ac.rs.foodmate.service.SyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sync")
public class SyncController {

    @Autowired
    SyncService syncService;

    @PreAuthorize("hasAuthority('USER_ROLE')")
    @PostMapping
    public ResponseEntity<SyncDTO> sync(@RequestBody SyncRequestDTO request) {
        return ResponseEntity.ok(syncService.sync(request));
    }
}
