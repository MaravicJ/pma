package ftn.uns.ac.rs.foodmate.dto.sync;

import ftn.uns.ac.rs.foodmate.models.MenuItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuItemSyncDTO {
    private long menuItemId;
    private String name;
    private String description;
    private double cost;
    private long menuItemRestaurantId;
    private String menuItemCategoryCode;
    private long menuItemImageId;

    public MenuItemSyncDTO(MenuItem menuItem) {
        menuItemId = menuItem.getId();
        name = menuItem.getName();
        description = menuItem.getDescription();
        cost = menuItem.getCost();
        menuItemRestaurantId = menuItem.getMenu().getRestaurant().getId();
        menuItemCategoryCode = menuItem.getCategory().getCode();
        if(menuItem.getImage() != null)
            menuItemImageId = menuItem.getImage().getId();
    }
}
