package ftn.uns.ac.rs.foodmate.dto.sync;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SyncDTO {
    private List<RestaurantSyncDTO> restaurants;
    private List<MenuItemSyncDTO> menuItems;
    private List<ImageSyncDTO> images;
    private List<WorkingDaySyncDTO> workingDays;
    private List<RestaurantRatingSyncDTO> ratings;
}
