package ftn.uns.ac.rs.foodmate.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class MenuItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private Category category;
    @Column
    private String name;
    @Column
    private double cost;
    @Column
    private String description;
    @OneToOne(fetch = FetchType.EAGER)
    @ToString.Exclude
    private Image image;
    @ManyToOne
    @ToString.Exclude
    private Menu menu;

    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    @PrePersist
    @PreUpdate
    protected void onModify() {
        modified = new Date();
    }

    public MenuItem(Category category, String name, double cost, String description, Image image) {
        this.category = category;
        this.name = name;
        this.cost = cost;
        this.description = description;
        this.image = image;
    }
}
