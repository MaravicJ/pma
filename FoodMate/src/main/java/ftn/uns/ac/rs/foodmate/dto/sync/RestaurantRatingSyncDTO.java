package ftn.uns.ac.rs.foodmate.dto.sync;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantRatingSyncDTO {
    private long restaurantId;
    private float rating;
}
