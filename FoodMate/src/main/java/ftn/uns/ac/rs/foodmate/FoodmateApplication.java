package ftn.uns.ac.rs.foodmate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodmateApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodmateApplication.class, args);
    }

}
