package ftn.uns.ac.rs.foodmate.dto;

import ftn.uns.ac.rs.foodmate.models.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuItemTextDTO {

    private String imagePath;
    private Category category;
    private String name;
    private double cost;
    private String description;

}
