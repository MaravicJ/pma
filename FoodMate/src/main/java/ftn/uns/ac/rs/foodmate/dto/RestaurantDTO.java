package ftn.uns.ac.rs.foodmate.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class RestaurantDTO {
    private long restaurantId;
    private String name;
    private String description;
    private String email;
    private String phoneNumber;
    private List<FoodPreferencesDTO> foodPreferences;
    private LocationDTO location;
    private ImageDTO mainImage;
    private List<ImageDTO> images;
    private List<WorkingDayDTO> workingDays;
    private double rating;
}
