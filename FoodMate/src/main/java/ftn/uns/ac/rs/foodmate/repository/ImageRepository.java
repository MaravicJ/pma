package ftn.uns.ac.rs.foodmate.repository;

import ftn.uns.ac.rs.foodmate.models.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
    List<Image> findByModifiedAfterAndRestaurantLocationCity(Date lastUpdate, String city);
}
