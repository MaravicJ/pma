package ftn.uns.ac.rs.foodmate.dto;

import ftn.uns.ac.rs.foodmate.models.Reservation;
import ftn.uns.ac.rs.foodmate.models.Restaurant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReservationInfoDTO {

    private Restaurant restaurant;
    private Reservation reservation;
    private int quantity;
}
