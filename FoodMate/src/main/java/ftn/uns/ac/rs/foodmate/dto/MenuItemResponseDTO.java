package ftn.uns.ac.rs.foodmate.dto;

import ftn.uns.ac.rs.foodmate.models.Image;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuItemResponseDTO {
    private MenuItemAndroid menuItem;
    private String image;
}
