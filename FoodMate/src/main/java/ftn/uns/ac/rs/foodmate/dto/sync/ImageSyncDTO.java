package ftn.uns.ac.rs.foodmate.dto.sync;

import ftn.uns.ac.rs.foodmate.models.Image;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImageSyncDTO {
    private long imageId;
    private String path;
    private long imageRestaurantId;

    public ImageSyncDTO(Image image) {
        imageId = image.getId();
        path = image.getPath();
        if(image.getRestaurant() != null)
            imageRestaurantId = image.getRestaurant().getId();
    }
}
