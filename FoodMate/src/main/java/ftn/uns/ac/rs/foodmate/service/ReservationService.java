package ftn.uns.ac.rs.foodmate.service;

import ftn.uns.ac.rs.foodmate.dto.ReservationDTO;
import ftn.uns.ac.rs.foodmate.dto.ResponseMessageDTO;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.ParseException;

public interface ReservationService {

    ResponseMessageDTO reserve(ReservationDTO reservationDTO) throws ParseException, IOException, MessagingException;
}
