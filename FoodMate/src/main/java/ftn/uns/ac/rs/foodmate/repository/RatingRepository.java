package ftn.uns.ac.rs.foodmate.repository;

import ftn.uns.ac.rs.foodmate.models.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {
    List<Rating> findByRestaurantName(String name);
    List<Rating> findByRestaurantId(Long id);
    List<Rating> findByModifiedAfterAndRestaurantLocationCity(Date lastUpdate, String city);
}

