package ftn.uns.ac.rs.foodmate.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private double rating;
    @ManyToOne
    private User user;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Restaurant restaurant;

    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    public Rating(double rating, User user, Restaurant restaurant, Date date) {
        this.rating = rating;
        this.user = user;
        this.restaurant = restaurant;
        this.modified = date;
    }

    @PrePersist
    @PreUpdate
    protected void onModify() {
        modified = new Date();
    }
}
