package ftn.uns.ac.rs.foodmate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuItemAndroid {
    private long menuItemId;

    private String name;
    private String description;
    private double cost;

    private long menuItemRestaurantId;
    private String menuItemCategoryCode;
    private long menuItemImageId;
}
