package ftn.uns.ac.rs.foodmate.dto.sync;

import ftn.uns.ac.rs.foodmate.dto.LocationDTO;
import ftn.uns.ac.rs.foodmate.models.FoodPreferences;
import ftn.uns.ac.rs.foodmate.models.Restaurant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantSyncDTO {
    private long restaurantId;
    private String name;
    private String description;
    private String email;
    private String phoneNumber;
    private float rating;
    private LocationDTO location;
    private long mainImageId;
    private List<String> foodPreferences;

    public RestaurantSyncDTO(Restaurant restaurant, float rating) {
        restaurantId = restaurant.getId();
        name = restaurant.getName();
        description = restaurant.getDescription();
        email = restaurant.getEmail();
        phoneNumber = restaurant.getPhoneNumber();
        this.rating = rating;
        location = new LocationDTO(restaurant.getLocation());
        if(restaurant.getMainImage() != null)
            mainImageId = restaurant.getMainImage().getId();
        foodPreferences = new ArrayList<>();
        for (FoodPreferences pref: restaurant.getFoodPreferences()) {
            foodPreferences.add(pref.getCode());
        }
    }
}
