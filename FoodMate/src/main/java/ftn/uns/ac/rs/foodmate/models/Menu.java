package ftn.uns.ac.rs.foodmate.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany
    private List<MenuItem> items = new ArrayList<>();
    @OneToOne(mappedBy = "menu")
    @ToString.Exclude
    private Restaurant restaurant;

    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    @PrePersist
    @PreUpdate
    protected void onModify() {
        modified = new Date();
    }
}
