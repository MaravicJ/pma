package ftn.uns.ac.rs.foodmate.service;

import ftn.uns.ac.rs.foodmate.models.Rating;

public interface RatingService {
    double calculateRestaurantRating(String restaurantName);
    double calculateRestaurantRating(Long restaurantId);
    Rating findRating(String username, String restaurantName);
    void save(Rating rating);
}
