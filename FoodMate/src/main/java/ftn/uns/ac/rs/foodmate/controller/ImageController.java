package ftn.uns.ac.rs.foodmate.controller;

import ftn.uns.ac.rs.foodmate.dto.MenuItemAndroid;
import ftn.uns.ac.rs.foodmate.dto.MenuItemResponseDTO;
import ftn.uns.ac.rs.foodmate.service.ImageService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/image")
@Log4j2
public class ImageController {

    @Autowired
    ImageService imageService;

    @PreAuthorize("hasAuthority('USER_ROLE')")
    @PostMapping("/get")
    public ResponseEntity<List<MenuItemResponseDTO>> getImageMenuItem(@RequestBody List<MenuItemAndroid> menuItems) {
        try {
            List<MenuItemResponseDTO> list = new ArrayList<>();
            for (MenuItemAndroid item: menuItems ) {
                String image = imageService.getImage(imageService.findImage(item.getMenuItemImageId()).getPath());
                MenuItemResponseDTO menuItemDTO = new MenuItemResponseDTO();
                menuItemDTO.setMenuItem(item);
                menuItemDTO.setImage(image);
                list.add(menuItemDTO);
            }

            return ResponseEntity.ok(list);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<List<MenuItemResponseDTO>>((List<MenuItemResponseDTO>) null, HttpStatus.CONFLICT);
        }
    }
}
