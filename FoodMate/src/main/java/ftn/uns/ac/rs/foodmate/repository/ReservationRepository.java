package ftn.uns.ac.rs.foodmate.repository;

import ftn.uns.ac.rs.foodmate.models.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation,Long> {
}
