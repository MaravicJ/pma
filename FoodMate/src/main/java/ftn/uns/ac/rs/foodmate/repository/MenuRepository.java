package ftn.uns.ac.rs.foodmate.repository;

import ftn.uns.ac.rs.foodmate.models.Menu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuRepository extends JpaRepository<Menu,Long> {
}
