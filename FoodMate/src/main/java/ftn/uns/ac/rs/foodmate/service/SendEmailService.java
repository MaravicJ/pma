package ftn.uns.ac.rs.foodmate.service;

import ftn.uns.ac.rs.foodmate.dto.ReservationInfoDTO;

import javax.mail.MessagingException;
import java.io.IOException;

public interface SendEmailService {

    void sendEmail(String email, String subject, String type, String username, ReservationInfoDTO reservationInfoDTO) throws MessagingException, IOException;

}
