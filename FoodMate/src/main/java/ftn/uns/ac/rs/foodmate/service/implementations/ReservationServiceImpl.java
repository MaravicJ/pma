package ftn.uns.ac.rs.foodmate.service.implementations;

import ftn.uns.ac.rs.foodmate.dto.ReservationDTO;
import ftn.uns.ac.rs.foodmate.dto.ReservationInfoDTO;
import ftn.uns.ac.rs.foodmate.dto.ResponseMessageDTO;
import ftn.uns.ac.rs.foodmate.models.*;
import ftn.uns.ac.rs.foodmate.repository.ReservationRepository;
import ftn.uns.ac.rs.foodmate.repository.TableRepository;
import ftn.uns.ac.rs.foodmate.service.ReservationService;
import ftn.uns.ac.rs.foodmate.service.RestaurantService;
import ftn.uns.ac.rs.foodmate.service.SendEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    UserDetailsServiceImpl userService;

    @Autowired
    RestaurantService restaurantService;

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    TableRepository tableRepository;

    @Autowired
    SendEmailService sendEmailService;

    @Override
    public ResponseMessageDTO reserve(ReservationDTO reservationDTO) throws ParseException, IOException, MessagingException {

        Restaurant restaurant = restaurantService.getRestaurantEntity(reservationDTO.getRestaurant_id());
        Date date_reservation = new SimpleDateFormat("d MMM yyyy").parse(reservationDTO.getDate());
        LocalTime time_begin = LocalTime.parse(reservationDTO.getTime());
        LocalTime time_to = time_begin.plusHours(3);

        if(restaurant == null){
            return new ResponseMessageDTO("Restaurant doesn't exist!");
        }

//        SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
//        String day = simpleDateformat.format(date_reservation);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date_reservation);
        String day = getDay(calendar.get(Calendar.DAY_OF_WEEK));
        System.out.println(day);
        WorkingDay working_day = restaurant.getWorkingDays().stream()
                .filter(p -> p.getDay().equals(day)).collect(Collectors.toList()).get(0);

        if(working_day.getTimeFrom().isAfter(time_begin)){
            if(working_day.getTimeTo().getHour()< 9){
                if(time_begin.getHour()< working_day.getTimeFrom().getHour()){
                    if(working_day.getTimeTo().isBefore(time_begin)){
                        return new ResponseMessageDTO("Restaurant is closed! Working hours on " + day + " is: " + working_day.getTimeFrom().toString() + "-" + working_day.getTimeTo().toString());

                    }
                }

            }else if(working_day.getTimeTo().isBefore(time_begin)){
                return new ResponseMessageDTO("Restaurant is closed! Working hours on " + day + " is: " + working_day.getTimeFrom().toString() + "-" + working_day.getTimeTo().toString());

            }
        }

        Optional<User> user = userService.findByUsername(reservationDTO.getUsername());
        if(!user.isPresent()){
            return new ResponseMessageDTO("User with username " + reservationDTO.getUsername() + " doesn't exist!");
        }

        List<Table> possible_tables = restaurant.getTables().stream()
                .filter(p -> p.getCapacity() >= reservationDTO.getNumber_persons()).collect(Collectors.toList());

        Collections.sort(possible_tables, Comparator.comparing(Table::getCapacity));

        Table find_table = null;
        for (Table table:possible_tables) {
            List<Reservation> all_reservation = table.getReservations().stream()
                    .filter(p -> p.getDate().compareTo(date_reservation) == 0).collect(Collectors.toList());

            if(all_reservation.size() == 0){
                find_table = table;
            }else{
                List<Reservation> existReservationsInMiddleBegin = all_reservation.stream()
                        .filter(p -> time_begin.isAfter(p.getTimeFrom()) && p.getTimeTo().isAfter(time_begin)).collect(Collectors.toList());
                List<Reservation> existReservationsSameBegin = all_reservation.stream()
                        .filter(p -> time_begin.equals(p.getTimeFrom())).collect(Collectors.toList());
                List<Reservation> existReservationsInMiddleTo = all_reservation.stream()
                        .filter(p -> time_to.isAfter(p.getTimeFrom()) && p.getTimeTo().isAfter(time_to)).collect(Collectors.toList());

                if(existReservationsInMiddleBegin.isEmpty() && existReservationsSameBegin.isEmpty() && existReservationsInMiddleTo.isEmpty()){
                    find_table = table;
                    break;
                }
            }
        }
        if(find_table == null){
            return new ResponseMessageDTO("There isn't any free table for " + reservationDTO.getDate() + " " + reservationDTO.getTime());
        }
        Reservation newReservation = new Reservation();
        newReservation.setDate(date_reservation);
        newReservation.setTimeFrom(time_begin);
        newReservation.setTimeTo(time_to);
        newReservation.setUser(user.get());

        newReservation = reservationRepository.save(newReservation);

        Table table = tableRepository.getOne(find_table.getId());
        table.getReservations().add(newReservation);
        tableRepository.save(table);

        ReservationInfoDTO reservationInfoDTO = new ReservationInfoDTO(restaurant,newReservation,reservationDTO.getNumber_persons());
        sendEmailService.sendEmail(user.get().getEmail(),"FoodMate: Successful reservation in restaurant " + restaurant.getName() +"!",
                "reservation" ,user.get().getUsername(),reservationInfoDTO);

        return new ResponseMessageDTO("Reservation successfully made!");
    }

    public String getDay(int day){

        switch (day){
            case Calendar.MONDAY:
                return "Monday";
            case Calendar.TUESDAY:
                return "Tuesday";
            case Calendar.WEDNESDAY:
                return "Wednesday";
            case Calendar.THURSDAY:
                return "Thursday";
            case Calendar.FRIDAY:
                return "Friday";
            case Calendar.SATURDAY:
                return "Saturday";
            case Calendar.SUNDAY:
                return "Sunday";
        }
        return null;
    }
}
