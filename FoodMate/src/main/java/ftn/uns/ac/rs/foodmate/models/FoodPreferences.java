package ftn.uns.ac.rs.foodmate.models;

public enum FoodPreferences {
    regular("regular"),
    vegetarian("vegetarian"),
    vegan("vegan"),
    glutenFree("gluten free"),
    nutFree("nut free"),
    kosher("kosher"),
    halal("halal");

    private String code;

    FoodPreferences(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
