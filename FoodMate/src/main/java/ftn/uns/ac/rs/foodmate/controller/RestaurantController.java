package ftn.uns.ac.rs.foodmate.controller;

import ftn.uns.ac.rs.foodmate.dto.*;

import ftn.uns.ac.rs.foodmate.dto.RatingDTO;

import ftn.uns.ac.rs.foodmate.service.ImageService;

import ftn.uns.ac.rs.foodmate.service.RestaurantService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/restaurants")
@Log4j2
public class RestaurantController {

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private ImageService imageService;

    @PreAuthorize("hasAuthority('USER_ROLE')")
    @PostMapping("/chosen")
    public ResponseEntity<?> getUserChosenRestaurants(@RequestBody String username) {
        try {
            restaurantService.getUserChosenRestaurants(username);
            return new ResponseEntity(restaurantService.getUserChosenRestaurants(username), HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity("Something went wrong. Please try again later.", HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAuthority('USER_ROLE')")
    @PostMapping("/add/chosen")
    public ResponseEntity<String> addUserChosenRestaurant(@RequestBody NewUserChosenRestaurantDTO newUserChosenRestaurantDTO) {
        restaurantService.addUserChosenRestaurant(newUserChosenRestaurantDTO);
        return new ResponseEntity("Restaurant successfully added to chosen restaurants list.", HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('USER_ROLE')")
    @PostMapping("/delete/chosen")
    public ResponseEntity<String> deleteUserChosenRestaurant(@RequestBody List<NewUserChosenRestaurantDTO> newUserChosenRestaurantDTOs) {
        restaurantService.deleteUserChosenRestaurant(newUserChosenRestaurantDTOs);
        return new ResponseEntity("Successfully deletion from chosen restaurants list.", HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('USER_ROLE')")
    @PostMapping("/get")
    public ResponseEntity<?> getRestaurant(@RequestBody String restaurantName) {
        try {
            return new ResponseEntity(restaurantService.getRestaurant(restaurantName), HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity("Something went wrong. Please try again later.", HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAuthority('USER_ROLE')")
    @PostMapping("/add/rating")
    public ResponseEntity<Double> addRating(@RequestBody RatingDTO ratingDTO) {
        double rating = restaurantService.addRating(ratingDTO);
        return new ResponseEntity(rating, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('USER_ROLE')")
    @PostMapping("/get/rating")
    public ResponseEntity<Double> getRating(@RequestBody String restaurantName) {
        return ResponseEntity.ok(restaurantService.getRating(restaurantName));
    }

    @PreAuthorize("hasAuthority('USER_ROLE')")
    @GetMapping("/image/{restaurantId}")
    public ResponseEntity<String> getImage(@PathVariable long restaurantId) {
        try {
            String image = imageService.getImage(
                    restaurantService.getRestaurantEntity(restaurantId).getMainImage().getPath());

            return ResponseEntity.ok(image);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Something went wrong. Please try again later.", HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAuthority('USER_ROLE')")
    @GetMapping("/images/{restaurantId}")
    public ResponseEntity<List<OtherImageDTO>> getImages(@PathVariable long restaurantId) {
        try {
            return ResponseEntity.ok(restaurantService.getOtherImagesDTO(restaurantId));
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.CONFLICT);
        }
    }
}
