package ftn.uns.ac.rs.foodmate.service.implementations;

import ftn.uns.ac.rs.foodmate.commons.ConfigMail;
import ftn.uns.ac.rs.foodmate.dto.ReservationInfoDTO;
import ftn.uns.ac.rs.foodmate.service.SendEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Service
public class SendEmailServiceImpl implements SendEmailService {

    @Autowired
    private JavaMailSender emailSender;

    @Override
    public void sendEmail(String email, String subject, String type, String username, ReservationInfoDTO reservationInfoDTO) throws MessagingException, IOException {
        MimeMessage mm = emailSender.createMimeMessage();
        mm.setFrom(new InternetAddress(ConfigMail.EMAIL));
        mm.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
        mm.setSubject(subject);

        MimeMultipart multipart = new MimeMultipart("related");
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText("This is message body");

        String htmlText = getText(type,username, reservationInfoDTO);

        messageBodyPart.setContent(htmlText, "text/html");
        multipart.addBodyPart(messageBodyPart);
        messageBodyPart = new MimeBodyPart();
        InputStream inputStream = this.getClass().getResourceAsStream("/images/food_mate_logo.png");
        ByteArrayDataSource ds = new ByteArrayDataSource(inputStream, "image/png");
        messageBodyPart.setDataHandler(new DataHandler(ds));
        messageBodyPart.setHeader("Content-ID", "<image>");
        multipart.addBodyPart(messageBodyPart);
        inputStream.close();

        if(type.equals("reservation")){
            messageBodyPart = new MimeBodyPart();
            String picture_path = reservationInfoDTO.getRestaurant().getImages().get(0).getPath().substring(19,reservationInfoDTO.getRestaurant().getImages().get(0).getPath().length());
            InputStream inputStream2 = this.getClass().getResourceAsStream(picture_path);
            ByteArrayDataSource ds2 = new ByteArrayDataSource(inputStream2, "image/jpg");
            messageBodyPart.setDataHandler(new DataHandler(ds2));
            messageBodyPart.setHeader("Content-ID", "<image2>");
            multipart.addBodyPart(messageBodyPart);
        }
        mm.setContent(multipart);

        emailSender.send(mm);
    }

    public String getText(String type, String username, ReservationInfoDTO reservationInfoDTO){

        String text = "";

        if(type.equals("register")){
            text = "<div><img src=\"cid:image\" style=\"width: 300px; max-width: 600px; height: auto; margin: auto; " +
                    "display: block;background-color:red\"></div>" +
                    "<h1 align=center>FoodMate</h1><p align=center>Hey " +username + "!</p>" +
                    "<p align=center>Your registration is successfully done!</p>" +
                    "<p align=center>Thank you for using our application!</p>" +
                    "<p align=center>FoodMate Team</p>";

        }else if(type.equals("reservation")){
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String date = dateFormat.format(reservationInfoDTO.getReservation().getDate());
            text = "<div><img src=\"cid:image\" style=\"width: 300px; max-width: 600px; height: auto; margin: auto; " +
                    "display: block;background-color:red\"></div>" +
                    "<h1 align=center>FoodMate</h1><p align=center><h1 align=center><b>Reservation successful!</b></h1></p>" +
                    "<h3 align=center>Hey " +username + "!</h3>" +
                    "<p align=center>Your reservation in restaurant " + reservationInfoDTO.getRestaurant().getName() + " is successfully done!</p>" +
                    "<p align=center><i><u>Reservation information</u></i></p>" +
                    "<p align=center><b>Restaurant:</b> <i>" + reservationInfoDTO.getRestaurant().getName() +"</i></p>" +
                    "<p align=center><b>Restaurant location:</b> <i>" + reservationInfoDTO.getRestaurant().getLocation().getAddress() + ", "
                    + reservationInfoDTO.getRestaurant().getLocation().getCity() + "</i></p>" +
                    "<p align=center><b>Date:</b><i>" + date +"</i></p>" +
                    "<p align=center><b>Time:</b><i>" + reservationInfoDTO.getReservation().getTimeFrom() +"</i></p>" +
                    "<p align=center><i> This reservation is for <b>" + reservationInfoDTO.getQuantity() +" people!</b></i></p>" +
                    "<p align=center><i> *Every reservation lasts for 3 hours!</i></p>" +
                    "<br><p align=center>Have a great time and enjoy your stay at <i>" + reservationInfoDTO.getRestaurant().getName() +"!</i></p>" +
                    "<div><img src=\"cid:image2\" style=\"width: 300px; max-width: 600px; height: auto; margin: auto; " +
                    "display: block;\"></div>" +
                    "<p align=center>Thank you for using our application!</p>" +
                    "<p align=center>FoodMate Team</p>";
        }

        return text;
    }
}
