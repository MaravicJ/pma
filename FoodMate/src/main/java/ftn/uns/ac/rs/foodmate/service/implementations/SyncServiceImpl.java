package ftn.uns.ac.rs.foodmate.service.implementations;

import ftn.uns.ac.rs.foodmate.dto.sync.*;
import ftn.uns.ac.rs.foodmate.models.*;
import ftn.uns.ac.rs.foodmate.repository.*;
import ftn.uns.ac.rs.foodmate.service.RatingService;
import ftn.uns.ac.rs.foodmate.service.SyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SyncServiceImpl implements SyncService {

    @Autowired
    RestaurantRepository restaurantRepository;

    @Autowired
    MenuItemRepository menuItemRepository;

    @Autowired
    ImageRepository imageRepository;

    @Autowired
    WorkingDayRepository workingDayRepository;

    @Autowired
    RatingRepository ratingRepository;

    @Autowired
    RatingService ratingService;

    @Override
    public SyncDTO sync(SyncRequestDTO request) {

        if(request.getLastUpdate() == null)
            request.setLastUpdate(new Date((long) 0));

        List<RestaurantSyncDTO> restaurants = new LinkedList<>();
        for(Restaurant restaurant: loadRestaurants(request.getLastUpdate(), request.getCity())) {
            float rating = (float) ratingService.calculateRestaurantRating(restaurant.getName());
            restaurants.add(new RestaurantSyncDTO(restaurant, rating));
        }

        List<ImageSyncDTO> images = new LinkedList<>();
        for(Image image: loadImages(request.getLastUpdate(), request.getCity())) {
            images.add(new ImageSyncDTO(image));
        }

        List<MenuItemSyncDTO> menuItems = new LinkedList<>();

        for(MenuItem menuItem: loadMenuItems(request.getLastUpdate(), request.getCity())){
            if(menuItem.getImage()!=null)
                images.add(new ImageSyncDTO(menuItem.getImage()));
            menuItems.add(new MenuItemSyncDTO(menuItem));
        }

        List<WorkingDaySyncDTO> workingDays = new LinkedList<>();
        for(WorkingDay workingDay: loadWorkingDays(request.getLastUpdate(), request.getCity())) {
            workingDays.add(new WorkingDaySyncDTO(workingDay));
        }

        List<RestaurantRatingSyncDTO> ratings = new LinkedList<>();
        Set<Long> updatedRestaurantIds = new HashSet<>();
        for(Rating rating: loadRatings(request.getLastUpdate(), request.getCity())) {
            updatedRestaurantIds.add(rating.getRestaurant().getId());
        }
        for(Long id: updatedRestaurantIds) {
            ratings.add(new RestaurantRatingSyncDTO(id, (float) ratingService.calculateRestaurantRating(id)));
        }


        SyncDTO syncDTO = new SyncDTO();
        syncDTO.setRestaurants(restaurants);
        syncDTO.setMenuItems(menuItems);
        syncDTO.setImages(images);
        syncDTO.setWorkingDays(workingDays);
        syncDTO.setRatings(ratings);
        return syncDTO;
    }

    private List<Restaurant> loadRestaurants(Date lastUpdate, String city) {
        return restaurantRepository.findByModifiedAfterAndLocationCity(lastUpdate, city);
    }

    private List<MenuItem> loadMenuItems(Date lastUpdate, String city) {
        return menuItemRepository.findByModifiedAfterAndMenuRestaurantLocationCity(lastUpdate, city);
    }

    private List<Image> loadImages(Date lastUpdate, String city) {
        return imageRepository.findByModifiedAfterAndRestaurantLocationCity(lastUpdate, city);
    }

    private List<WorkingDay> loadWorkingDays(Date lastUpdate, String city) {
        return workingDayRepository.findByModifiedAfterAndRestaurantLocationCity(lastUpdate, city);
    }

    private List<Rating> loadRatings(Date lastUpdate, String city) {
        return ratingRepository.findByModifiedAfterAndRestaurantLocationCity(lastUpdate, city);
    }


}
