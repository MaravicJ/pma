package ftn.uns.ac.rs.foodmate.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String name;
    @Column(length = 2000)
    private String description;
    @Column
    @Email
    private String email;
    @Column
    private String phoneNumber;
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "RestaurantFoodPreferences", joinColumns = @JoinColumn(name = "id"))
    @Enumerated(EnumType.STRING)
    private List<FoodPreferences> foodPreferences;
    @OneToOne
    private Location location;
    @OneToOne
    private Menu menu;
    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Table> tables = new ArrayList<>();
    @OneToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    @ToString.Exclude
    private Image mainImage;
    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Image> images;
    @OneToMany(mappedBy = "restaurant", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @ToString.Exclude
    private List<WorkingDay> workingDays;
    @OneToMany(mappedBy = "restaurant", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @ToString.Exclude
    private List<Rating> ratings;

    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    public Restaurant(String name, String description, String email, String phoneNumber, List<FoodPreferences> foodPreferences, Location location) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.foodPreferences = foodPreferences;
        this.location = location;
    }

    @PrePersist
    @PreUpdate
    protected void onModify() {
        modified = new Date();
    }

}
