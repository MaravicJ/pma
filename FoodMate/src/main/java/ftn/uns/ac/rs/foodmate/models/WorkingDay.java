package ftn.uns.ac.rs.foodmate.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class WorkingDay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String day;
    @Column
    private LocalTime timeFrom;
    @Column
    private LocalTime timeTo;
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private Restaurant restaurant;

    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    @PrePersist
    @PreUpdate
    protected void onModify() {
        modified = new Date();
    }
}
