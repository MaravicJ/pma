package ftn.uns.ac.rs.foodmate.exceptions;

public class MyException extends Exception {

    public MyException(String message) {
        super(message);
    }
}
