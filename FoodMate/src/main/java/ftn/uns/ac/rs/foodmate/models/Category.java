package ftn.uns.ac.rs.foodmate.models;

public enum Category {
    breakfast("breakfast"),
    main_course("main_course"),
    desert("desert"),
    drink("drink");

    private String code;

    Category(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
