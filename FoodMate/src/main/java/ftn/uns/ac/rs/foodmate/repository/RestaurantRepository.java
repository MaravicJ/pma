package ftn.uns.ac.rs.foodmate.repository;

import ftn.uns.ac.rs.foodmate.models.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
    Restaurant findByName(String name);

    List<Restaurant> findByModifiedAfterAndLocationCity(Date lastUpdate, String city);
}
