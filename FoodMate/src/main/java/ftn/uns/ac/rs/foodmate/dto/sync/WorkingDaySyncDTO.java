package ftn.uns.ac.rs.foodmate.dto.sync;

import ftn.uns.ac.rs.foodmate.commons.DateFormatter;
import ftn.uns.ac.rs.foodmate.models.WorkingDay;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkingDaySyncDTO {
    private long workingDayId;
    private String day;
    private String timeFrom;
    private String timeTo;
    private long workingDayRestaurantId;

    public WorkingDaySyncDTO(WorkingDay workingDay) {
        workingDayId = workingDay.getId();
        day = workingDay.getDay();
        timeFrom = workingDay.getTimeFrom().format(DateFormatter.dtf);
        timeTo = workingDay.getTimeTo().format(DateFormatter.dtf);
        workingDayRestaurantId = workingDay.getRestaurant().getId();
    }
}
