package ftn.uns.ac.rs.foodmate.service;

import ftn.uns.ac.rs.foodmate.dto.ImageDTO;
import ftn.uns.ac.rs.foodmate.models.Image;

import java.io.IOException;
import java.util.List;

public interface ImageService {
    void save(Image image);
    String getImage(String path) throws IOException;
    ImageDTO getImageDTO(Image image) throws IOException;
    List<ImageDTO> getImageDTOs(List<Image> images) throws IOException;
    Image findImage(long id);
}
