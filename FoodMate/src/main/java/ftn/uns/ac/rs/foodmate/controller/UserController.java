package ftn.uns.ac.rs.foodmate.controller;

import com.twilio.exception.ApiException;
import ftn.uns.ac.rs.foodmate.dto.*;
import ftn.uns.ac.rs.foodmate.exceptions.MyException;
import ftn.uns.ac.rs.foodmate.models.User;
import ftn.uns.ac.rs.foodmate.security.JwtProvider;
import ftn.uns.ac.rs.foodmate.security.SecurityUser;
import ftn.uns.ac.rs.foodmate.service.ReservationService;
import ftn.uns.ac.rs.foodmate.service.SendEmailService;
import ftn.uns.ac.rs.foodmate.service.implementations.UserDetailsServiceImpl;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@RestController
@RequestMapping(value = "/user")
@Log4j2
public class UserController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserDetailsServiceImpl userService;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    SendEmailService sendEmailService;

    @Autowired
    SimpMessagingTemplate webSocket;

    @Autowired
    ReservationService reservationService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody UserDTO loginRequest) {

        Optional<User> u =this.userService.findByUsername(loginRequest.getUsername());

        if(u.isPresent() && !(u.get().isVerify())){
            log.warn("Account not verify!");
            return new ResponseEntity<>("Account not verify!", HttpStatus.CONFLICT);
        }

        AuthenticationResponseDTO authResponse = authenticate(loginRequest);
        if(authResponse == null){
            return new ResponseEntity<>("Wrong username or password!", HttpStatus.BAD_REQUEST);
        }
        log.info("Successfully login");

        return new ResponseEntity<>(authResponse, HttpStatus.OK);
    }

    @PostMapping("/singup")
    public ResponseEntity<?> singUp(@Valid @RequestBody RegistrationDTO userDTO){

        Optional<User> u;

        u = this.userService.findByUsername(userDTO.getUsername());
        if(u.isPresent()){
            log.warn("User with " + userDTO.getUsername() + " username already exist!");
            return new ResponseEntity<>("User with " + userDTO.getUsername() +
                    " username already exist! Please write different one!", HttpStatus.CONFLICT);
        }

        u = this.userService.findByEmail(userDTO.getEmail());
        if(u.isPresent()){
            log.warn("User with " + userDTO.getEmail() + " email already exist!");
            return new ResponseEntity<>("User with " + userDTO.getEmail() +
                    " email already exist! Please write different one!", HttpStatus.CONFLICT);
        }

        u = this.userService.findByPhoneNumber(userDTO.getPhoneNumber());
        if(u.isPresent()){
            log.warn("User with " + userDTO.getPhoneNumber() + " phone number already exist!");
            return new ResponseEntity<>("User with " + userDTO.getPhoneNumber() +
                    " phone number already exist! Please write different one!", HttpStatus.CONFLICT);
        }

        User newUser = userService.registration(userDTO);

        if( newUser == null){
            return new ResponseEntity<>("Something went wrong!",HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>("Successfully registered!",HttpStatus.OK);
    }

    @PostMapping("/verify")
    public ResponseEntity<?> verify(@RequestBody UserDTO loginRequest) throws IOException, MessagingException {

        String mess = userService.verify(loginRequest.getUsername());

        if (mess.equals("")){
            return new ResponseEntity<>("Something went wrong", HttpStatus.CONFLICT);
        }

        AuthenticationResponseDTO authResponse = authenticate(loginRequest);
        if(authResponse == null){
            return new ResponseEntity<>("Wrong username or password!", HttpStatus.BAD_REQUEST);
        }
        log.info("Successfully login");
        sendEmailService.sendEmail(userService.findByUsername(loginRequest.getUsername()).get().getEmail(),
                "FoodMate: Successful registration!", "register", loginRequest.getUsername(),null);


        return new ResponseEntity<>(authResponse, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('USER_ROLE')")
    @PostMapping("/edit")
    public ResponseEntity<String> editUserData(@RequestBody UserDataDTO userDataDTO){
        log.info("Edit function - " + userDataDTO.getUsername());
        try {
            userService.editUser(userDataDTO);
            return new ResponseEntity<>("Successful data update.", HttpStatus.OK);
        } catch (MyException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }

    @PreAuthorize("hasAuthority('USER_ROLE')")
    @PostMapping("/get")
    public ResponseEntity<UserDataDTO> get(@RequestBody String username){
        System.out.println(username + "AAAAAAAaaaaaaaaaaaaa");
        Optional<User> user = userService.findByUsername(username);
        return new ResponseEntity<>(userService.getUserDTO(user.get()), HttpStatus.OK);
    }

    public AuthenticationResponseDTO authenticate(UserDTO loginRequest){
        Authentication authentication = null;
        UsernamePasswordAuthenticationToken t = new UsernamePasswordAuthenticationToken(
                loginRequest.getUsername(), loginRequest.getPassword());
        try {
            authentication = this.authenticationManager.authenticate(t);
        } catch (AuthenticationException e) {
            log.warn(String.format("Invalid login with username: %s",
                    loginRequest.getUsername()));
            return null;
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Reload password post-authentication so we can generate token
        UserDetails userDetails = this.userService.loadUserByUsername(loginRequest.getUsername());

        SecurityUser su = (SecurityUser) userDetails;
        String token = this.jwtProvider.generateToken(userDetails);
        // Return the token
        AuthenticationResponseDTO authResponse = new AuthenticationResponseDTO(token,"Bearer",userDetails.getUsername());

        return authResponse;
    }

    @PostMapping("/reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody String email){

        ResetPasswordResponseDTO new_password = userService.resetPassword(email);

        if (new_password.equals("")){
            return new ResponseEntity<>("Email " + email + " doesn't exist! Please try a different one!", HttpStatus.CONFLICT);
        }

        log.info("Successfully password reset for user " + email);

        return new ResponseEntity<>(new_password, HttpStatus.OK);
    }

//    @PreAuthorize("hasAuthority('USER_ROLE')")
    @PostMapping("/test")
    public ResponseEntity<String> test(@RequestBody String test) throws IOException, MessagingException {
        sendEmailService.sendEmail("bojanacrlc@gmail.com","Successful registration!", "register", "boka",null);
        return new ResponseEntity<>("Uslooo", HttpStatus.OK);

    }

    @PostMapping("/send_message")
    public ResponseEntity<String> sendMessage(@RequestBody SMS sms) throws IOException, MessagingException {
        try{
            userService.send(sms);
        }
        catch(ApiException e){

            webSocket.convertAndSend(sms.getTo(), getTimeStamp() + ": Error sending the SMS: "+e.getMessage());
            throw e;
        }
        webSocket.convertAndSend(sms.getTo(), getTimeStamp() + ": SMS has been sent!: "+sms.getTo());
        return new ResponseEntity<>("Message sent!", HttpStatus.OK);

    }

    private String getTimeStamp() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
    }

    @PostMapping("/reserve")
    public ResponseEntity<ResponseMessageDTO> reserve(@RequestBody ReservationDTO reservationDTO) throws ParseException, IOException, MessagingException {

        ResponseMessageDTO responseMessageDTO = reservationService.reserve(reservationDTO);

        return new ResponseEntity<>(responseMessageDTO, HttpStatus.OK);

    }

}
