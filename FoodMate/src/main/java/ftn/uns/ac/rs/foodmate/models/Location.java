package ftn.uns.ac.rs.foodmate.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private double longitude;
    @Column
    private double latitude;
    @Column
    private String city;
    @Column
    private String country;
    @Column
    private String address;

    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    @PrePersist
    @PreUpdate
    protected void onModify() {
        modified = new Date();
    }
}
