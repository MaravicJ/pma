package ftn.uns.ac.rs.foodmate.service;

import ftn.uns.ac.rs.foodmate.dto.sync.SyncDTO;
import ftn.uns.ac.rs.foodmate.dto.sync.SyncRequestDTO;

public interface SyncService {
    public SyncDTO sync(SyncRequestDTO request);
}
