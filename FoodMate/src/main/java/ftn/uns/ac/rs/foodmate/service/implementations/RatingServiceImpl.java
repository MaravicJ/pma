package ftn.uns.ac.rs.foodmate.service.implementations;

import ftn.uns.ac.rs.foodmate.models.Rating;
import ftn.uns.ac.rs.foodmate.repository.RatingRepository;
import ftn.uns.ac.rs.foodmate.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RatingServiceImpl implements RatingService {

    @Autowired
    private RatingRepository ratingRepository;

    public double calculateRestaurantRating(String restaurantName) {
        List<Rating> ratings = ratingRepository.findByRestaurantName(restaurantName);
        return calculateAverageRating(ratings);
    }

    public double calculateRestaurantRating(Long restaurantId) {
        List<Rating> ratings = ratingRepository.findByRestaurantId(restaurantId);
        return calculateAverageRating(ratings);
    }

    public double calculateAverageRating(List<Rating> ratings) {
        if(ratings.isEmpty()) {
            return 0;
        }

        double sum = 0;
        for(Rating r: ratings) {
            sum = sum + r.getRating();
        }

        return sum/ratings.size();
    }


    public Rating findRating(String username, String restaurantName) {
        List<Rating> ratings = ratingRepository.findByRestaurantName(restaurantName);
        for(Rating r: ratings) {
            if(r.getUser().getUsername().equals(username)) {
                return r;
            }
        }
        return null;
    }

    public void save(Rating rating) {
        ratingRepository.save(rating);
    }

}
