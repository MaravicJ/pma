package ftn.uns.ac.rs.foodmate.service.implementations;

import ftn.uns.ac.rs.foodmate.models.WorkingDay;
import ftn.uns.ac.rs.foodmate.repository.WorkingDayRepository;
import ftn.uns.ac.rs.foodmate.service.WorkingDayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkingDayServiceImpl implements WorkingDayService {

    @Autowired
    private WorkingDayRepository workingDayRepository;

    public void save(List<WorkingDay> workingDays) {
        workingDayRepository.saveAll(workingDays);
    }
}
