package ftn.uns.ac.rs.foodmate.converters;

import ftn.uns.ac.rs.foodmate.models.FoodPreferences;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class FoodPreferencesConverter implements AttributeConverter<FoodPreferences, String> {

    @Override
    public String convertToDatabaseColumn(FoodPreferences foodPreference) {
        if (foodPreference == null) {
            return null;
        }
        return foodPreference.getCode();
    }

    @Override
    public FoodPreferences convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }

        return Stream.of(FoodPreferences.values())
                .filter(c -> c.getCode().equals(code))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}