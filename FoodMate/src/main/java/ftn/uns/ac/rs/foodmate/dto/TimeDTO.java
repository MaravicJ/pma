package ftn.uns.ac.rs.foodmate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeDTO {

    private int hourFrom;
    private int minuteFrom;
    private int hourTo;
    private int minuteTo;
    private long id;
}
