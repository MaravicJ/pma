package ftn.uns.ac.rs.foodmate.service.implementations;

import ftn.uns.ac.rs.foodmate.dto.MenuItemTextDTO;
import ftn.uns.ac.rs.foodmate.dto.RegistrationDTO;
import ftn.uns.ac.rs.foodmate.dto.TimeDTO;
import ftn.uns.ac.rs.foodmate.models.*;
import ftn.uns.ac.rs.foodmate.repository.MenuItemRepository;
import ftn.uns.ac.rs.foodmate.repository.MenuRepository;
import ftn.uns.ac.rs.foodmate.repository.RoleRepository;
import ftn.uns.ac.rs.foodmate.repository.TableRepository;
import ftn.uns.ac.rs.foodmate.service.*;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class AppLoaderService implements ApplicationRunner {

	private static final Logger logger = LoggerFactory.getLogger(AppLoaderService.class);

	private RoleRepository roleRepository;
	private UserDetailsServiceImpl userDetailsService;
	private RestaurantService restaurantService;
	private LocationService locationService;
	private  RatingService ratingService;
	private ImageService imageService;
	private WorkingDayService workingDayService;
	private TableRepository tableRepository;
	private MenuRepository menuRepository;
	private MenuItemRepository menuItemRepository;

	public AppLoaderService(RoleRepository roleRepository, UserDetailsServiceImpl userDetailsService, RestaurantService restaurantService,
                            LocationService locationService, RatingService ratingService, ImageService imageService, WorkingDayService workingDayService,
                            TableRepository tableRepository, MenuRepository menuRepository, MenuItemRepository menuItemRepository) {
		this.roleRepository = roleRepository;
		this.userDetailsService = userDetailsService;
		this.restaurantService = restaurantService;
		this.locationService = locationService;
		this.ratingService = ratingService;
		this.imageService = imageService;
		this.workingDayService = workingDayService;
		this.tableRepository = tableRepository;
		this.menuRepository = menuRepository;
		this.menuItemRepository = menuItemRepository;
	}

	@Override
	public void run(ApplicationArguments args) {

		if (roleRepository.findAll().size() == 0) {

			Role role = new Role();
			role.setName(UserRole.USER_ROLE);
			role = roleRepository.save(role);

			log.info("Role " + role.getName() + " added!");
		}

		if(restaurantService.findAll().size() == 0) {
			int iL = 1;
			int iW = 0;
			// SAVOCA
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.vegan, FoodPreferences.vegetarian), iL++, 19.832980, 45.260100, "Novi Sad",
					"Serbia", "Bulevar Oslobodjenja 41", "Restaurant & Pizzeria Savoca", "If you ask anyone from Novi Sad " +
							"what's the best Italian food place around, " +
							"they will tell you try Savoca, in downtown Novi Sad. Everyone who loves Italy, pizza and pasta, the richness of flavors and smells from the Mediterranean" +
							"does not need to make a trip to Italy. We brought Italy to Novi Sad.\nVisit the Italian cuisine restaurant and pizzeria Savoca and enjoy the authentic aromas " +
							"and the sounds of Italian canzones.", "info@picerijasavoca.com", "021/52-11-11",
					"/src/main/resources/images/restaurants/savoca.jpg", Arrays.asList("/src/main/resources/images/restaurants/savoca-1.jpg",
							"/src/main/resources/images/restaurants/savoca-2.jpg", "/src/main/resources/images/restaurants/savoca-3.jpg",
							"/src/main/resources/images/restaurants/savoca-4.jpg", "/src/main/resources/images/restaurants/savoca-5.jpg",
							"/src/main/resources/images/restaurants/savoca-6.jpg"), Arrays.asList(new TimeDTO(8,0,23,0,iW++),
							new TimeDTO(8,0,23,0, iW++), new TimeDTO(8,0,23,0, iW++),
							new TimeDTO(8,0,23,0, iW++), new TimeDTO(8,0,1,0, iW++),
							new TimeDTO(8,0,1,0, iW++), new TimeDTO(8,0,23,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(new MenuItemTextDTO("/src/main/resources/images/food/omlet2.jpg",Category.breakfast, "Omlet sa šunkom", 350.00,
							"Uzivajte u omletu na gurmanski način! Sastojci: šunka, jaja, sir "),new MenuItemTextDTO("/src/main/resources/images/food/omlet.jpg",
							Category.breakfast,"Omlet sa povrćem", 250.00,"Uzivajte u omletu sa puno povrća! Sastojci: jaja, sir, paprika, rukola " ),
							new MenuItemTextDTO("/src/main/resources/images/food/mexican_bowl.jpg", Category.main_course,"Meksička piletina sa avokadom", 550.00,
									"Meksička piletina vas vodi u Meksiko! Sastojci: piletina, avokado kukuruz i razno povrće "), new MenuItemTextDTO(
									"/src/main/resources/images/food/tortilja_jabuka.jpg", Category.main_course,"Tortilja sa piletinom i jabukom", 450.00,
									"Odlična kombinacija slatkog i slanog! Sastojci: piletina,tortilja, jabuka i razno povrće "),
							new MenuItemTextDTO("/src/main/resources/images/food/cheeese_cake_cherry.jpg", Category.desert, "Cheesecake sa višnjama", 250.00,
									"Odlična kombinacija cheesecake i višanja i to u čaši!"), new MenuItemTextDTO("/src/main/resources/images/food/nutela.png",
									Category.desert,"Nutela torta", 350.00,"Ko voli čokoladu,ovo je idealna torta za njega!"),
							new MenuItemTextDTO("/src/main/resources/images/food/long_coctail.jpeg", Category.drink, "Long Island iced tea", 550.00,
									"Triple sec, Vodka, Rum, Tequila, Gin"), new MenuItemTextDTO("/src/main/resources/images/food/cosmopolitan_drink.jpg", Category.drink,
									"Cosmopolitan", 350.00, "Sveža limeta, vodka, Cointreau, sok od brusnice ") ) );

			// MODENA
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.vegan, FoodPreferences.vegetarian, FoodPreferences.glutenFree),
					iL++, 19.845420, 45.254850, "Novi Sad",
					"Serbia", "Trg Slobode 4", "Modena Caffe Restaurant", "With a view of the central town square, the people of" +
							" Novi Sad in their restlessness and hustle and bustle and " +
							"the clock on the cathedral that inevitably beats the minutes of passing, Modena coffee pizzeria " +
							"will tuck you in its pleasant and modern ambience and stop time. With quiet music and traditional hospitality, " +
							"you will be able to have an important business meeting, but also to enjoy a conversation with your friends..\nVisit " +
							"the Italian cuisine restaurant and pizzeria Savoca and enjoy the authentic aromas " +
							"and the sounds of Italian canzones.", "info@modena.com", "064/803-88-00",
					"/src/main/resources/images/restaurants/modena.jpg", Arrays.asList("/src/main/resources/images/restaurants/modena-1.jpg",
							"/src/main/resources/images/restaurants/modena-2.jpg", "/src/main/resources/images/restaurants/modena-3.jpg",
							"/src/main/resources/images/restaurants/modena-4.jpg", "/src/main/resources/images/restaurants/modena-5.jpg",
							"/src/main/resources/images/restaurants/modena-6.jpg"), Arrays.asList(new TimeDTO(8,0,23,0, iW++),
							new TimeDTO(8,0,23,0, iW++), new TimeDTO(8,0,23,0, iW++),
							new TimeDTO(8,0,23,0, iW++), new TimeDTO(8,0,1,0, iW++),
							new TimeDTO(8,0,1,0, iW++), new TimeDTO(8,0,23,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(new MenuItemTextDTO("/src/main/resources/images/food/kajgana.slanina.jpg",Category.breakfast, "Domaća kajgana", 250.00,
									"Sastojci: Luk, slanina, paprika, jaja  "),new MenuItemTextDTO("/src/main/resources/images/food/benedikt.jpg",
									Category.breakfast,"Benedeks", 235.00,"Ukus koji obećava! Sastojci: Slanina, jaja  " ),
							new MenuItemTextDTO("/src/main/resources/images/food/azijska.jpg", Category.main_course,"Azijska piletina", 640.00,
									"Azijska piletina vas vodi u Aziju! Sastojci: Pileće belo meso, indijski orah, soja sos, povrće, basmati pirinač  "), new MenuItemTextDTO(
									"/src/main/resources/images/food/piletina.gljive.jpg", Category.main_course,"Piletina modena", 750.00,
									"Sastojci: Pileće belo meso u sosu od šampinjona, pomfrit  "),
							new MenuItemTextDTO("/src/main/resources/images/food/sufle.jpg", Category.desert, "Čokoladni sufle(Lava kolač)", 380.00,
									"Mmm, prste da poližeš!"), new MenuItemTextDTO("/src/main/resources/images/food/tiramisu.jpg",
									Category.desert,"Tiramisu sa kokosom", 290.00,"Odlična kombinacija!"),
							new MenuItemTextDTO("/src/main/resources/images/food/garden.tonik.jpg", Category.drink, "Garden tonik", 210.00,
									"paradaiz, sargarepa, cvekla"), new MenuItemTextDTO("/src/main/resources/images/food/grejp.koktel.jpg", Category.drink,
									"Magic elixir", 345.00, "grejpfrut, ananas, banana, djumbir ") ) );

			// MASA
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.vegan, FoodPreferences.glutenFree),
					iL++, 19.846350, 45.255610, "Novi Sad",
					"Serbia", "Zmaj Jovina 4", "Masa Caffe Restaurant", "In the very center of Novi Sad, in Zmaj Jovina Street," +
							" in 2006, one of the most authentic" +
							" restaurants in the city started operating. In a pleasant ambience, you can serve a cup of coffee, a drink," +
							" a treat or a dish from our rich international cuisine.", "info@masa.com", "021/451-241",
					"/src/main/resources/images/restaurants/masa.jpg", Arrays.asList("/src/main/resources/images/restaurants/masa-1.jpg",
							"/src/main/resources/images/restaurants/masa-2.jpg", "/src/main/resources/images/restaurants/masa-3.jpg",
							"/src/main/resources/images/restaurants/masa-4.jpg", "/src/main/resources/images/restaurants/masa-5.jpg",
							"/src/main/resources/images/restaurants/masa-6.jpg"), Arrays.asList(new TimeDTO(8,0,23,0, iW++),
							new TimeDTO(8,0,23,0, iW++), new TimeDTO(8,0,23,0, iW++),
							new TimeDTO(8,0,23,0, iW++), new TimeDTO(8,0,1,0, iW++),
							new TimeDTO(8,0,1,0, iW++), new TimeDTO(8,0,23,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(new MenuItemTextDTO("/src/main/resources/images/food/omlet2.jpg",Category.breakfast, "Omlet sa šunkom", 350.00,
									"Uzivajte u omletu na gurmanski način! Sastojci: šunka, jaja, sir "),new MenuItemTextDTO("/src/main/resources/images/food/omlet.jpg",
									Category.breakfast,"Omlet sa povrćem", 250.00,"Uzivajte u omletu sa puno povrća! Sastojci: jaja, sir, paprika, rukola " ),
							new MenuItemTextDTO("/src/main/resources/images/food/mexican_bowl.jpg", Category.main_course,"Meksička piletina sa avokadom", 550.00,
									"Meksička piletina vas vodi u Meksiko! Sastojci: piletina, avokado kukuruz i razno povrće "), new MenuItemTextDTO(
									"/src/main/resources/images/food/tortilja_jabuka.jpg", Category.main_course,"Tortilja sa piletinom i jabukom", 450.00,
									"Odlična kombinacija slatkog i slanog! Sastojci: piletina,tortilja, jabuka i razno povrće "),
							new MenuItemTextDTO("/src/main/resources/images/food/cheeese_cake_cherry.jpg", Category.desert, "Cheesecake sa višnjama", 250.00,
									"Odlična kombinacija cheesecake i višanja i to u čaši!"), new MenuItemTextDTO("/src/main/resources/images/food/nutela.png",
									Category.desert,"Nutela torta", 350.00,"Ko voli čokoladu,ovo je idealna torta za njega!"),
							new MenuItemTextDTO("/src/main/resources/images/food/long_coctail.jpeg", Category.drink, "Long Island iced tea", 550.00,
									"Triple sec, Vodka, Rum, Tequila, Gin"), new MenuItemTextDTO("/src/main/resources/images/food/cosmopolitan_drink.jpg", Category.drink,
									"Cosmopolitan", 350.00, "Sveža limeta, vodka, Cointreau, sok od brusnice ") ) );

			//GIMI
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.vegetarian),
					iL++, 19.8471684, 45.2565362, "Novi Sad",
					"Serbia", "Dunavska 19", "G.IMI", "G.imi is for sure one of the best" +
							" restaurants you can find in Novi Sad. Food is great, staff is great, atmosphere is really nice so " +
							"this is a place to relax and enjoy. Definitely a restaurant you should visit when you're in Novi Sad.", "info@gimi.rs", "+381641365545",
					"/src/main/resources/images/restaurants/gimi.jpg", Arrays.asList("/src/main/resources/images/restaurants/gimi-1.jpg",
							"/src/main/resources/images/restaurants/gimi-2.jpg", "/src/main/resources/images/restaurants/gimi-3.jpg",
							"/src/main/resources/images/restaurants/gimi-4.jpg", "/src/main/resources/images/restaurants/gimi-5.jpg",
							"/src/main/resources/images/restaurants/gimi-6.jpg"), Arrays.asList(new TimeDTO(12,0,23,0, iW++),
							new TimeDTO(12,0,23,0, iW++), new TimeDTO(12,0,23,0, iW++),
							new TimeDTO(12,0,23,0, iW++), new TimeDTO(12,0,1,0, iW++),
							new TimeDTO(12,0,1,0, iW++), new TimeDTO(12,0,23,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(new MenuItemTextDTO("/src/main/resources/images/food/avokado.salata.jpg",Category.breakfast,
									"Avokado salata", 860.00, "Pored popularnog avokada, u sastav ove salate ulazi:" +
											" brokoli, spanać, mix zelenih salata, terjaki, kikiriki i dekorativni " +
											"jestivi list pirinča uz hrskave momente od hlebnog testa sa sipinim mastilom. "),
							new MenuItemTextDTO("/src/main/resources/images/food/hacapuri.jpg",
									Category.breakfast,"Gruzijski hačapuri", 850.00,"Gruzijski specijalitet hačapuri, sa puterom, jajima i dve vrste sira - za zagrevanje..? " ),
							new MenuItemTextDTO("/src/main/resources/images/food/ribs.jpg", Category.main_course,"Biftek", 1980.00,
									"Odabrali smo za Vas jedno od najpoznatijih i najboljih mesa, BLACK ANGUS. "), new MenuItemTextDTO(
									"/src/main/resources/images/food/burger.jpg", Category.main_course,"Gimi burger", 1000.00,
									"Najbolja kombinacija ukusa! "),
							new MenuItemTextDTO("/src/main/resources/images/food/torta1.jpg", Category.desert, "Medena torta", 550.00,
									"Mmmm, uživajte u savršenom ukusu!"), new MenuItemTextDTO("/src/main/resources/images/food/ferero.jpg",
									Category.desert,"Ferero torta", 550.00,"Super ukusna Ferrero torta, sa mnogo čokoladne kreme, " +
									"čiji intezivan ukus po jedinstvenom receptu oduševljava."),
							new MenuItemTextDTO("/src/main/resources/images/food/royal.jpg", Category.drink, "Raspberry royal", 585.00,
									"Absolut vodka, prosecco i maline. Kako odoleti?! "), new MenuItemTextDTO("/src/main/resources/images/food/aperol.jpg", Category.drink,
									"Aperol Spritz", 350.00, "U slučaju da tražite idealno osveženje nakon kraja radnog dana. ") ) );

			//Plava frajla
			createRestaurant( Arrays.asList(FoodPreferences.regular),
					iL++, 19.8420597, 45.2484195, "Novi Sad",
					"Serbia", "Sutjeska 2", "Plava frajla", "Amazing authentic Serbian food\n" +
							"\n" + "This restaurant is about 10 minute walk out of the city centre but trust me it is worth it! The amount of food you get for a " +
							"ridiculously cheap price. There was no way I could finish the amount of food we ordered and there was two of us. We ordered one dish each " +
							"and a small salad to share. Don’t eat anything all day and be prepared to fill your tummies up with good quality and authentic food! ",
					"info@plavafrajla.rs", "021/66 13 675",
					"/src/main/resources/images/restaurants/plavafrajla.jpg", Arrays.asList("/src/main/resources/images/restaurants/plavafrajla-1.jpg",
							"/src/main/resources/images/restaurants/plavafrajla-2.jpg", "/src/main/resources/images/restaurants/plavafrajla-3.jpg",
							"/src/main/resources/images/restaurants/plavafrajla-4.jpg", "/src/main/resources/images/restaurants/plavafrajla-5.jpg",
							"/src/main/resources/images/restaurants/plavafrajla-6.jpg"), Arrays.asList(new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,1,0, iW++),
							new TimeDTO(9,0,1,0, iW++), new TimeDTO(9,0,22,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(new MenuItemTextDTO("/src/main/resources/images/food/frustuk.jpg",Category.breakfast,
									"Frajlin frustuk", 260.00, "Omlet na naš način!"),
							new MenuItemTextDTO("/src/main/resources/images/food/parma.jpg",
									Category.breakfast,"Parma salata", 450.00,"Lagana salata za dobro jutro ! " ),
							new MenuItemTextDTO("/src/main/resources/images/food/gulas.jpg", Category.main_course,"Gulas", 700.00,
									"Ukus koji Vas vodi u Vojvodinu!"), new MenuItemTextDTO(
									"/src/main/resources/images/food/mlinci.jpg", Category.main_course,"Mlinci sa piletinom", 550.00,
									"Al se nekad dobro jelo! "),
							new MenuItemTextDTO("/src/main/resources/images/food/strudla.jpg", Category.desert, "Strudla sa orasima", 350.00,
									"Naša slatka tradicija!"), new MenuItemTextDTO("/src/main/resources/images/food/gomboce.jpg",
									Category.desert,"Gomboce", 450.00,"Baš kao bakine!"),
							new MenuItemTextDTO("/src/main/resources/images/food/vino.jpg", Category.drink, "Domaće belo vino 3dl", 300.00,
									"Osvežava!"), new MenuItemTextDTO("/src/main/resources/images/food/kafa.jpg", Category.drink,
									"LavAzza kafa", 120.00, "Za dobro jutro!") ) );

			//Cafe Veliki
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.kosher),
					iL++, 19.8453501, 45.2574419, "Novi Sad",
					"Serbia", "Nikole Pašića 24", "Cafe Veliki", "Cafe VELIKI, a type of restaurant not often seen in Europe" +
							" these days, with authentic flavors and recipes dating back to the 19th century. Novi Sad, and Vojvodina is multi-national and multi-ethnic society," +
							" and if you want to sense inspiring smells, and to enjoy delicacies that Serb, German (Austrian), Slovak, Hungarian and Jewish nations, living in Vojvodina" +
							" ate, and still eat, come and visit us in Nikole Pasica street in the very heart of Novi Sad. We guarantee a delightful and relaxing atmosphere, where you " +
							"can enjoy with your family, friends and your business clients. Experience authentic dishes prepared with the old tradition of this region. Excellent" +
							" local cuisine in the charming restaurant. ",
					"info@cafeveliki.rs", "021/553-420",
					"/src/main/resources/images/restaurants/cafeveliki.jpg", Arrays.asList("/src/main/resources/images/restaurants/cafeveliki-1.jpg",
							"/src/main/resources/images/restaurants/cafeveliki-2.jpg", "/src/main/resources/images/restaurants/cafeveliki-3.jpg",
							"/src/main/resources/images/restaurants/cafeveliki-4.jpg", "/src/main/resources/images/restaurants/cafeveliki-5.jpg",
							"/src/main/resources/images/restaurants/cafeveliki-6.jpg"), Arrays.asList(new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,1,0, iW++),
							new TimeDTO(9,0,1,0, iW++), new TimeDTO(9,0,22,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(new MenuItemTextDTO("/src/main/resources/images/food/meze.jpg",Category.breakfast,
									"Meze", 550.00, "Mix suhomesnatih proizvoda!"),
							new MenuItemTextDTO("/src/main/resources/images/food/pahuljice.jpg",
									Category.breakfast,"Pahuljice", 350.00,"Lagani doručak! " ),
							new MenuItemTextDTO("/src/main/resources/images/food/file.jpg", Category.main_course,"Svinjski file u sosu od šampinjona", 850.00,
									"Svinjski file u sosu od šampinjona, sa grilovanim povrćem!"), new MenuItemTextDTO(
									"/src/main/resources/images/food/rebarca.jpg", Category.main_course,"Mix iz raja", 950.00,
									"Mesna kombinacija VELIKI (za 2 gladne osobe),rebarca u roštilj sosu, ćuretina sa višnjama,slovačke kobasice, krmenadle u sosu od jabuka"),
							new MenuItemTextDTO("/src/main/resources/images/food/cokotart.jpg", Category.desert, "Čokoladni tart sa voćem", 350.00,
									"Naša slatka tradicija!"), new MenuItemTextDTO("/src/main/resources/images/food/krem.jpg",
									Category.desert,"Krem zalogaj", 290.00,"Osvežavajući krem u čaši sa voćem i granolom!"),
							new MenuItemTextDTO("/src/main/resources/images/food/limunada.jpg", Category.drink, "Limunada", 180.00,
									"Da te osveži!"), new MenuItemTextDTO("/src/main/resources/images/food/la.jpg", Category.drink,
									"LavAzza kafa", 120.00, "Za dobro jutro!") ) );

			//Gentlemen
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.vegetarian, FoodPreferences.nutFree),
					iL++, 19.8153664, 45.2546867, "Novi Sad",
					"Serbia", "Branka Bajića 9", "Caffe Restoran Gentlemen", "One of the best club restourant that we visit,food" +
							" is just incredible,and the staff is super professional,price are normal,soo if you coming in Novi Sad visit this restourant and you will fully enjoy and taste Novi Sad",
					"info@gentlemen.rs", "021/301 98 37",
					"/src/main/resources/images/restaurants/gentlemen.jpg", Arrays.asList("/src/main/resources/images/restaurants/gentlemen-1.jpg",
							"/src/main/resources/images/restaurants/gentlemen-2.jpg", "/src/main/resources/images/restaurants/gentlemen-3.jpg",
							"/src/main/resources/images/restaurants/gentlemen-4.jpg", "/src/main/resources/images/restaurants/gentlemen-5.jpg",
							"/src/main/resources/images/restaurants/gentlemen-6.jpg"), Arrays.asList(new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,1,0, iW++),
							new TimeDTO(9,0,1,0, iW++), new TimeDTO(9,0,22,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(new MenuItemTextDTO("/src/main/resources/images/food/losos.jpg",Category.breakfast,
									"Losos salata", 650.00, "Odličan spoj avokada i salate!"),
							new MenuItemTextDTO("/src/main/resources/images/food/doo.jpg",
									Category.breakfast,"Doručak Gentlemen", 450.00,"Jaja, kranjska kobasica sa sirom, pikantna kobasica, kačkavalj sir, mix salata " ),
							new MenuItemTextDTO("/src/main/resources/images/food/italijanska.jpg", Category.main_course,"Italijansku butkica", 850.00,
									"Sve je tu: Butkica, bbq sos, med, senf, ren, dinstani kupus, kanu pomfrit!"), new MenuItemTextDTO(
									"/src/main/resources/images/food/buu.jpg", Category.main_course,"Gentlemen Burger", 550.00,
									"Burger koji osvaja!"),
							new MenuItemTextDTO("/src/main/resources/images/food/vocnasalata.jpg", Category.desert, "Voćna salata", 350.00,
									"Kombinacija sezonskog voća!"), new MenuItemTextDTO("/src/main/resources/images/food/tiramisu2.jpg",
									Category.desert,"Tiiramisu", 290.00,"Postoji li sta bolje od domaceg tiramisua,spoj savrsenih sastojaka u " +
									"jednom zalogaju,za sve ljubitelje slatkisa!"),
							new MenuItemTextDTO("/src/main/resources/images/food/smo.jpg", Category.drink, "Smoothie", 380.00,
									"Da te osveži!"), new MenuItemTextDTO("/src/main/resources/images/food/kk.jpg", Category.drink,
									"LavAzza kafa", 120.00, "Za dobro jutro!") ) );

			//Fontana
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.halal),
					iL++, 19.8434491, 45.2586972, "Novi Sad",
					"Serbia", "Nikole Pašića 27", "Restoran Fontana", "When you look from the street it doesn't look appealing. " +
							"But believe me - walk in. Backyard is amazing and like you're entering another world. It's peaceful, relaxing and just different than what you " +
							"expected. Service is great - staff had smiles on their faces all the time, they were helpful and nice. And the food is great - very tasty but also affordable.",
					"info@fontana.rs", "021/66 21 779",
					"/src/main/resources/images/restaurants/fontana.jpg", Arrays.asList("/src/main/resources/images/restaurants/fontana-1.jpg",
							"/src/main/resources/images/restaurants/fontana-2.jpg", "/src/main/resources/images/restaurants/fontana-3.jpg",
							"/src/main/resources/images/restaurants/fontana-4.jpg", "/src/main/resources/images/restaurants/fontana-5.jpg",
							"/src/main/resources/images/restaurants/fontana-6.jpg"), Arrays.asList(new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,1,0, iW++),
							new TimeDTO(9,0,1,0, iW++), new TimeDTO(9,0,22,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(new MenuItemTextDTO("/src/main/resources/images/food/przenice.jpg",Category.breakfast,
									"Prženice", 350.00, "Idealan izbor za doručak!"),
							new MenuItemTextDTO("/src/main/resources/images/food/oml.jpg",
									Category.breakfast,"Omlet sa slaninom", 250.00,"Jaja, slanina i salata " ),
							new MenuItemTextDTO("/src/main/resources/images/food/piletina.visnje.jpg", Category.main_course,"Piletina u sosu od višanja", 650.00,
									"Možda je ideja o mesu u voćnom sosu nesvakidašnja, ali... takav je i njen ukus!"), new MenuItemTextDTO(
									"/src/main/resources/images/food/cevapi.jpg", Category.main_course,"Ćevapi", 750.00,
									"Klasika je u modi!"),
							new MenuItemTextDTO("/src/main/resources/images/food/nutela.palacinka.jpg", Category.desert, "Nutela palačinke", 550.00,
									"Ima li neko da ih ne voli?"), new MenuItemTextDTO("/src/main/resources/images/food/ll.jpg",
									Category.desert,"Lava kolač", 380.00,"Pojedi kolač i opusti se!"),
							new MenuItemTextDTO("/src/main/resources/images/food/zvonko.jpg", Category.drink, "Zvonko Bogdan vino", 3000.00,
									"Da te osveži!"), new MenuItemTextDTO("/src/main/resources/images/food/kkk.png", Category.drink,
									"LavAzza kafa", 120.00, "Za dobro jutro!") ) );

			//Cubo Koncept Bar i Restoran
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.vegetarian, FoodPreferences.vegan),
					iL++, 19.8471223, 45.2402401, "Novi Sad",
					"Serbia", "Strumička 16", "Cubo Concept Bar & Restaurant", "A modern restaurant near the " +
							"Danube. the food is European and Mediterranean. excellent monkfish, perfect local white wine. extraordinary service. pleasant" +
							" atmosphere. many original deserts. all in all, this is probably the best restaurant in town!",
					"info@cubo.rs", "021/421 721",
					"/src/main/resources/images/restaurants/cubo.jpg", Arrays.asList("/src/main/resources/images/restaurants/cubo-1.jpg",
							"/src/main/resources/images/restaurants/cubo-2.jpg", "/src/main/resources/images/restaurants/cubo-3.jpg",
							"/src/main/resources/images/restaurants/cubo-4.jpg", "/src/main/resources/images/restaurants/cubo-5.jpg",
							"/src/main/resources/images/restaurants/cubo-6.jpg"), Arrays.asList(new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,1,0, iW++),
							new TimeDTO(9,0,1,0, iW++), new TimeDTO(9,0,22,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(new MenuItemTextDTO("/src/main/resources/images/food/prsuta.jpg",Category.breakfast,
									"Pršuta sa parmezanom", 890.00, "Idealan kombinacija za doručak!"),
							new MenuItemTextDTO("/src/main/resources/images/food/rr.jpg",
									Category.breakfast,"Ravioli od brancina u sosu od gambora", 1250.00,"Savršena kombinacija ukusa!" ),
							new MenuItemTextDTO("/src/main/resources/images/food/raimstek.jpg", Category.main_course,"Ramstek u sosu od aronije", 1380.00,
									"Odlična kombinacija!"), new MenuItemTextDTO(
									"/src/main/resources/images/food/svinjskarebra.jpg", Category.main_course,"Svinjska rebra", 950.00,
									"Svinjska rebra u pikantnom sosu sa povrćem!"),
							new MenuItemTextDTO("/src/main/resources/images/food/tt.jpg", Category.desert, "Tiramisu", 550.00,
									"Tiramisu iz Kuba, savršen "), new MenuItemTextDTO("/src/main/resources/images/food/slatko.jpg",
									Category.desert,"Slatki zalogaj", 680.00,"Neodoljivi čokoladni listići punjeni musom od bele čokolade " +
									"i indijskog oraha sa sorbeom od borovnice."),
							new MenuItemTextDTO("/src/main/resources/images/food/kovacevic.jpg", Category.drink, "Kovačević belo vino", 3500.00,
									"Da te osveži!"), new MenuItemTextDTO("/src/main/resources/images/food/rose.jpg", Category.drink,
									"Rose vino 3dl", 420.00, "Jedna čaša ide savršeno uz bilo koje jelo!") ) );

			//Zak
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.vegetarian, FoodPreferences.nutFree),
					iL++, 19.8388696, 45.2543464, "Novi Sad",
					"Serbia", "Šafarikova 6", "Zak", "Zak is an elegant restaurant where the staff is really " +
							"trying to make an atmosphere which is alike fine dining. The food is really excellent. It is elegant and classy, I like it very much!",
					"info@zak.rs", "021/44 75 65",
					"/src/main/resources/images/restaurants/zak.jpg", Arrays.asList("/src/main/resources/images/restaurants/zak-1.jpg",
							"/src/main/resources/images/restaurants/zak-2.jpg", "/src/main/resources/images/restaurants/zak-3.jpg",
							"/src/main/resources/images/restaurants/zak-4.jpg", "/src/main/resources/images/restaurants/zak-5.jpg",
							"/src/main/resources/images/restaurants/zak-6.jpg"), Arrays.asList(new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,1,0, iW++),
							new TimeDTO(9,0,1,0, iW++), new TimeDTO(9,0,22,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(
							new MenuItemTextDTO("/src/main/resources/images/food/goka.jpg",Category.breakfast,
									"Goveđi karpaćo", 890.00, "Naš goveđi karpaćo čini savršeni sezonski starter."),
							new MenuItemTextDTO("/src/main/resources/images/food/povrce.jpg",
									Category.breakfast,"Povrće u sosu od mirođije", 750.00,"Savršena kombinacija ukusa!" ),
							new MenuItemTextDTO("/src/main/resources/images/food/jagnje.jpg", Category.main_course,"Jagnjeća kolenica", 1200.00,
									"Jagnjeća kolenica sa povrćem u sosu od timjana. "),
							new MenuItemTextDTO(
									"/src/main/resources/images/food/aaa.jpg", Category.main_course,"Goveđi file", 1150.00,
									"Savršeno tanki goveđi file u u izražajnom sosu od smrčka – senzacija ukusa za vaše nepce."),
							new MenuItemTextDTO("/src/main/resources/images/food/po.jpg", Category.desert, "Cheesecake", 650.00,
									"Cheesecake sa makom i raguom od višanja! "),
							new MenuItemTextDTO("/src/main/resources/images/food/nugat.jpg",
									Category.desert,"Nugat torta", 580.00,"Nijedan obožavatelj čokolade ne može biti ravnodušan kada vidi i potom proba " +
									"– našu nugat tortu. "),
							new MenuItemTextDTO("/src/main/resources/images/food/konjak.jpg", Category.drink, "Konjak 3dl", 800.00,
									"Konjak je piće koje oduvek sa sobom nosi određenu dozu ekskluzive i hedonizma."),
							new MenuItemTextDTO("/src/main/resources/images/food/rakija.jpg", Category.drink,
									"Rakija 2dl", 420.00, "Trenutak elegancije u svakoj čašici.") ) );

			//Aqua Doria
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.vegetarian, FoodPreferences.kosher),
					iL++, 19.857487, 45.2540315, "Novi Sad",
					"Serbia", "Kamenički put 21131", "Aqua Doria", "The Aqua Doria restaurant, which offers " +
							"regional specialities, is located right next to the mighty Danube river, the view from there is really nice. Behind is a huge " +
							"rocky prominence of the Fruška Gora mountain with the awesome Petrovaradin fortress high above.The restaurant is very popular, so " +
							"the four of us had to book our table in advance.",
					"info@carda.rs", "021/64 30 949",
					"/src/main/resources/images/restaurants/carda.jpg", Arrays.asList("/src/main/resources/images/restaurants/carda-1.jpg",
							"/src/main/resources/images/restaurants/carda-2.jpg", "/src/main/resources/images/restaurants/carda-3.jpg",
							"/src/main/resources/images/restaurants/carda-4.jpg", "/src/main/resources/images/restaurants/carda-5.jpg",
							"/src/main/resources/images/restaurants/carda-6.jpg"), Arrays.asList(new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,1,0, iW++),
							new TimeDTO(9,0,1,0, iW++), new TimeDTO(9,0,22,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(
							new MenuItemTextDTO("/src/main/resources/images/food/do.jpg",Category.breakfast,
									"Doručak za dvoje", 890.00, "Nema šta nema!"),
							new MenuItemTextDTO("/src/main/resources/images/food/ah.jpg",
									Category.breakfast,"Meze sa namazima", 950.00,"Savršena kombinacija ukusa!" ),
							new MenuItemTextDTO("/src/main/resources/images/food/brancin.jpg", Category.main_course,"Brancin", 600.00,
									"Jer je riba najbolji izbor. "),
							new MenuItemTextDTO("/src/main/resources/images/food/vesalica.jpg", Category.main_course,"Dimljena vešalica ", 800.00,
									"Ukus koji osvaja!"),
							new MenuItemTextDTO("/src/main/resources/images/food/krofne.jpg", Category.desert, "Krofne", 450.00,
									"Kod dobrog domaćina! "),
							new MenuItemTextDTO("/src/main/resources/images/food/mak.jpg",
									Category.desert,"Rezanci sa makom", 380.00,"Samo domaće!"),
							new MenuItemTextDTO("/src/main/resources/images/food/turska.jpg", Category.drink, "Turska kafa", 80.00,
									"Za dobro jutro!"),
							new MenuItemTextDTO("/src/main/resources/images/food/sljiva.jpeg", Category.drink,
									"Šljiva 2dl", 420.00, "Rakija od šljive.") ) );

			//Projekat 72
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.vegetarian, FoodPreferences.vegan),
					iL++, 19.8471587, 45.2595586, "Novi Sad",
					"Serbia", "Kosovska 15", "Project 72 Wine & Deli", "Project 72 Wine&Deli is no ordinary " +
							"restaurant but a place of exquisite taste, where fine food is prepared with a lot of imagination and soul that go along with" +
							" finely selected wines in perfect harmony for your senses to which it is impossible to remain indifferent. Food is prepared" +
							" in a special atmosphere with great dedication since we believe that the food absorbs the energy and vibration that" +
							" is prepared with. Soul food is prepared in a creative way in a pleasant atmosphere, with beautiful rhythms " +
							"and different tastes so that you can fully enjoy. This is a restaurant where it's all about the enjoyment in food and wine.",
					"info@project.rs", "021/65 72 720",
					"/src/main/resources/images/restaurants/projekat.jpg", Arrays.asList("/src/main/resources/images/restaurants/projekat-1.jpg",
							"/src/main/resources/images/restaurants/projekat-2.jpg", "/src/main/resources/images/restaurants/projekat-3.jpg",
							"/src/main/resources/images/restaurants/projekat-4.jpg", "/src/main/resources/images/restaurants/projekat-5.jpg",
							"/src/main/resources/images/restaurants/projekat-6.jpg"), Arrays.asList(new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,1,0, iW++),
							new TimeDTO(9,0,1,0, iW++), new TimeDTO(9,0,22,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(
							new MenuItemTextDTO("/src/main/resources/images/food/dorucak.jpg",Category.breakfast,
									"Projekat doručak", 890.00, "Beli urnebes sa seckanim suvim šljivama, rogačem, ljutom paprikom i hrskavom koricom"),
							new MenuItemTextDTO("/src/main/resources/images/food/jaja.jpg",
									Category.breakfast,"Projekat jaja", 750.00,"Grilovane mlade špargle s poširanim jajetom i ovčijim sirom!" ),
							new MenuItemTextDTO("/src/main/resources/images/food/pita.jpg", Category.main_course,"Pita sa biftekom", 1200.00,
									" Pita s biftekom i šumskim pečurkama, servirana uz mus od kajmaka i čips od krompira. "),
							new MenuItemTextDTO("/src/main/resources/images/food/ju.jpg", Category.main_course,"Juneća rebra ", 1800.00,
									"Bogat je aromama kandiranih trešanja i bobičastog voća i odlično se slaže uz #Project72 " +
											"juneća rebra s njokama od spanaća i sosom od vina"),
							new MenuItemTextDTO("/src/main/resources/images/food/rolada.jpg", Category.desert, "Rolada sa jagodama", 650.00,
									"Osim vazdušaste kore od belanca i jagoda sosa, sadrži sorbe od jagoda, listiće badema i svežu mentu! "),
							new MenuItemTextDTO("/src/main/resources/images/food/che.jpg",
									Category.desert,"Cheesecake sa jabukom", 650.00,"CHEESECAKE S JABUKOM, cimetom, suvim grožđem i karamel kremom, čija hrskava korica od " +
									"ovsenog brašna čeka da bude prelivena toplim sosom od vanile!"),
							new MenuItemTextDTO("/src/main/resources/images/food/crveno.jpg", Category.drink, "Crno vino 3dl", 480.00,
									"Za opuštanje!"),
							new MenuItemTextDTO("/src/main/resources/images/food/ka.jpg", Category.drink,
									"Kafa", 220.00, "Danas 70% sveta pije Arabicu blagog ukusa i jake arome, a preostalih 30%" +
									" gorku Robustu koja ima više kofeina.Naša Coffee Rooster je fina mešavina ove dve vrste, spremna da vas razbudi, ugreje," +
									" razmrda i osveži!") ) );

			//Gondola
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.vegetarian, FoodPreferences.vegan,FoodPreferences.halal),
					iL++, 19.8505462, 45.2541489, "Novi Sad",
					"Serbia", "Bulevar Mihajla Pupina 18", "Gondola", "5 star place by my criterias... Not to fancy, but still very modern," +
							" atmosphere briliant, music, chillout, service...And food...They really know seriously to do this..I am looking at scores for.some.of" +
							" the places (for example the one which is the \"best in N.Sad\")...That place is comparing with this one like 3 classes below...Don't " +
							"beleive in everything you see and read, pick place spontaneously and make your own experience...For me, after trying so many diff places, one of.the best in Novi Sad.",
					"info@gondola.rs", "021/456 563",
					"/src/main/resources/images/restaurants/gondola.jpg", Arrays.asList("/src/main/resources/images/restaurants/gondola-1.jpg",
							"/src/main/resources/images/restaurants/gondola-2.jpg", "/src/main/resources/images/restaurants/gondola-3.jpg",
							"/src/main/resources/images/restaurants/gondola-4.jpg", "/src/main/resources/images/restaurants/gondola-5.jpg",
							"/src/main/resources/images/restaurants/gondola-6.jpg"), Arrays.asList(new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,1,0, iW++),
							new TimeDTO(9,0,1,0, iW++), new TimeDTO(9,0,22,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(
							new MenuItemTextDTO("/src/main/resources/images/food/dvoje.jpg",Category.breakfast,
									"Doručak za dvoje", 1890.00, "Paleta sireva,suhomesnatog, brusketa sa paradajzom..."),
							new MenuItemTextDTO("/src/main/resources/images/food/self.jpg",
									Category.breakfast,"Gondola doručak", 750.00,"Paleta iznenađenja!" ),
							new MenuItemTextDTO("/src/main/resources/images/food/pace.jpg", Category.main_course,"Pačiji file u sosu od pomorandže", 1300.00,
									" Pačiji file u sosu od pomorandže i brusnice, sa pireom od celera & karamelizovanom balzamiko cveklom."),
							new MenuItemTextDTO("/src/main/resources/images/food/stake.jpg", Category.main_course,"RIB EYE Steak ", 1800.00,
									"U službi u vrhunskog hedonizma - RIB EYE Steak!"),
							new MenuItemTextDTO("/src/main/resources/images/food/lako.jpg", Category.desert, "Lava kolač", 550.00,
									"Najlepše stvari u životu su slatke...poput našeg #lava kolača. "),
							new MenuItemTextDTO("/src/main/resources/images/food/cokokugla.jpg",
									Category.desert,"Čoko kugla", 650.00,"Krema od crne čokolade i lešnika sa vaflima, bananom i granolom."),
							new MenuItemTextDTO("/src/main/resources/images/food/ap.jpg", Category.drink, "Aperol Spritz", 480.00,
									"Za opuštanje!"),
							new MenuItemTextDTO("/src/main/resources/images/food/zvo.jpg", Category.drink,
									"Crno vino Zvonko Bogdan 3dl", 420.00, "Uživajte!") ) );

			//Piknik, Bar & Restoran
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.vegetarian, FoodPreferences.vegan,FoodPreferences.halal),
					iL++, 19.8348606, 45.2299389, "Novi Sad",
					"Serbia", "Ribarsko ostrvo", "Piknik, Bar & Restoran", "Very nice place for families, friends or even" +
							" romantic visits. On the bank of river Danube with wonderful view no matter if you are sitting inside or in the garden. Food is great, " +
							"wide range of wines, cakes are delicious (specially šnenokle!), garden is adorable, playground for children is safe, coffee is great, service is " +
							"polite... what more can you expect! :) Few minute by car from city center and you will feel like in the middle of nature :)",
					"info@piknik.rs", "069 1745645",
					"/src/main/resources/images/restaurants/piknik.jpg", Arrays.asList("/src/main/resources/images/restaurants/piknik-1.jpg",
							"/src/main/resources/images/restaurants/piknik-2.jpg", "/src/main/resources/images/restaurants/piknik-3.jpg",
							"/src/main/resources/images/restaurants/piknik-4.jpg", "/src/main/resources/images/restaurants/piknik-5.jpg",
							"/src/main/resources/images/restaurants/piknik-6.jpg"), Arrays.asList(new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,1,0, iW++),
							new TimeDTO(9,0,1,0, iW++), new TimeDTO(9,0,22,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(
							new MenuItemTextDTO("/src/main/resources/images/food/mediteran.jpg",Category.breakfast,
									"Mediteranski doručak", 550.00, "Ukusi Mediterana na Pikniku "),
							new MenuItemTextDTO("/src/main/resources/images/food/salata.jpg",
									Category.breakfast,"Piknik salata", 750.00,"salata sa svežim španatom, šampinjonim, " +
									"crvenim lukom i krutonima u kremastom prelivu " ),
							new MenuItemTextDTO("/src/main/resources/images/food/meso.jpg", Category.main_course,"Grilovana piletina", 860.00,
									" Grilovana piletina sa ponfritom"),
							new MenuItemTextDTO("/src/main/resources/images/food/pastrmka.jpg", Category.main_course,"Pastrmka ", 1000.00,
									"Pastrmka sa povrćem!"),
							new MenuItemTextDTO("/src/main/resources/images/food/kolac.jpg", Category.desert, "Piknik kolač", 550.00,
									"Najlepše je veče uz kolače pokraj Dunava "),
							new MenuItemTextDTO("/src/main/resources/images/food/raj.jpg",
									Category.desert,"Dezert za dvoje", 1050.00,"Tri leće, Snenokle i Krempita"),
							new MenuItemTextDTO("/src/main/resources/images/food/dunja.jpg", Category.drink, "Dunja", 280.00,
									"Proći će ili neće proći.Sa dobrom rakijom sigurno prolazi."),
							new MenuItemTextDTO("/src/main/resources/images/food/da.jpg", Category.drink,
									"Piknik koktel", 420.00, "Uživajte!") ) );

			//Garden
			createRestaurant( Arrays.asList(FoodPreferences.regular, FoodPreferences.vegetarian, FoodPreferences.vegan),
					iL++, 19.8403625, 45.2493122, "Novi Sad",
					"Serbia", "Vase Stajića 27", "Garden Pizza & Salad Bar", "One of the best Pizza place i been " +
							", they really are good and the Pizza is delicious, great stuff you will meet and you can also watch them while they are doing it " +
							", highly recommended",
					"info@garden.rs", "021 6333222",
					"/src/main/resources/images/restaurants/garden.jpg", Arrays.asList("/src/main/resources/images/restaurants/garden-1.jpg",
							"/src/main/resources/images/restaurants/garden-2.jpg", "/src/main/resources/images/restaurants/garden-3.jpg",
							"/src/main/resources/images/restaurants/garden-4.jpg", "/src/main/resources/images/restaurants/garden-5.jpg",
							"/src/main/resources/images/restaurants/garden-6.jpg"), Arrays.asList(new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,22,0, iW++),
							new TimeDTO(9,0,22,0, iW++), new TimeDTO(9,0,1,0, iW++),
							new TimeDTO(9,0,1,0, iW++), new TimeDTO(9,0,22,0, iW++)),
					Arrays.asList(2,2,4,4,6,6), Arrays.asList(
							new MenuItemTextDTO("/src/main/resources/images/food/jajaa.jpg",Category.breakfast,
									"Avokado tost sa jajima", 350.00, "Avokado tost sa jajima je odlican izbor za početak dana" +
									" i nutritivno kvalitetan doručak/ branč "),
							new MenuItemTextDTO("/src/main/resources/images/food/bruskete.jpg",
									Category.breakfast,"Bruskete", 550.00,"Da li ste možda za naše bruskete? Sa šunkom, paradajzom ili sa " +
									"urmama, hrskave i bogatog ukusa " +
									"crvenim lukom i krutonima u kremastom prelivu " ),
							new MenuItemTextDTO("/src/main/resources/images/food/pizza.jpg", Category.main_course,"Pepperoni pizza", 760.00,
									" Original Pepperoni pizza, jedinstvenog i bogatog ukusa. "),
							new MenuItemTextDTO("/src/main/resources/images/food/bol.jpg", Category.main_course,"Canelloni pizza ", 900.00,
									"Canelloni pizza je kombinacija bolognese sosa i pizze, fantastičnog je ukusa! Pravo iz pećina drva, hrskava i još vruća"),
							new MenuItemTextDTO("/src/main/resources/images/food/nutela.pica.jpg", Category.desert, "Nutela pica", 550.00,
									"Naša najsladja pizza!"),
							new MenuItemTextDTO("/src/main/resources/images/food/cii.jpg",
									Category.desert,"Cheesecake ", 300.00,"Zasladite svoju dušu i telo!"),
							new MenuItemTextDTO("/src/main/resources/images/food/kala.jpg", Category.drink, "Kafa i Limunada", 380.00,
									"Odlična kombinacija za uživanje"),
							new MenuItemTextDTO("/src/main/resources/images/food/mix.jpg", Category.drink,
									"Garden mix", 220.00, "Uživajte!") ) );

			if(userDetailsService.findAll().size() == 0) {
				User user = userDetailsService.registration(new RegistrationDTO("jeca", "123", "jecc@gmail.com",
						"0631234567", "Jelena", "Maravic"));
				user.setVerify(true);
				userDetailsService.save(user);
				log.info("User " + user.getUsername() + " added!");

				User user2 = userDetailsService.registration(new RegistrationDTO("boka", "sifra", "bojanacrlc@gmail.com",
						"0669176333", "Bojana", "Corilic"));
				user2.setVerify(true);
				userDetailsService.save(user2);
				log.info("User " + user2.getUsername() + " added!");
			}
		}
	}

	private List<Image> createImages(List<String> images, Restaurant restaurant) {
		List<Image> objectImages = new ArrayList<>();
		for(String i: images) {
			Image image = new Image();
			image.setModified(new Date());
			image.setPath(i);
			image.setRestaurant(restaurant);
			objectImages.add(image);
		}

		return objectImages;
	}

	private void createRestaurant(List<FoodPreferences> foodPreferencesList, long idLocation, double longitude, double latitude, String city, String country, String address,
								  String restaurantName, String restaurantDescription, String restaurantEmail, String restaurantNumber, String mainImagePath,
								  List<String> imagePaths, List<TimeDTO> timeWorking, List<Integer> tableCapacity,List<MenuItemTextDTO> items){

		List<FoodPreferences> foodPreferences = foodPreferencesList;
		// location
		Location location = new Location(idLocation, longitude, latitude,
				city, country, address, new Date());
		locationService.save(location);
		// restaurant
		Restaurant restaurant = new Restaurant(restaurantName, restaurantDescription, restaurantEmail, restaurantNumber, foodPreferences, location);
		// image
		Image mainImage = new Image();
		mainImage.setModified(new Date());
		mainImage.setPath(mainImagePath);
		mainImage.setRestaurant(restaurant);
		restaurant.setMainImage(mainImage);
		// images
		List<String> images = imagePaths;
		List<Image> allImages = createImages(images, restaurant);
		restaurant.setImages(allImages);
		// working days
		WorkingDay monday = new WorkingDay(timeWorking.get(0).getId(), "Monday", LocalTime.of(timeWorking.get(0).getHourFrom(),
				timeWorking.get(0).getMinuteFrom()), LocalTime.of(timeWorking.get(0).getHourTo(), timeWorking.get(0).getMinuteTo()), restaurant, new Date());
		monday.setRestaurant(restaurant);
		WorkingDay tuesday = new WorkingDay(timeWorking.get(1).getId(), "Tuesday", LocalTime.of(timeWorking.get(1).getHourFrom(),
				timeWorking.get(1).getMinuteFrom()), LocalTime.of(timeWorking.get(1).getHourTo(), timeWorking.get(1).getMinuteTo()), restaurant, new Date());
		tuesday.setRestaurant(restaurant);
		WorkingDay wednesday = new WorkingDay(timeWorking.get(2).getId(), "Wednesday", LocalTime.of(timeWorking.get(2).getHourFrom(),
				timeWorking.get(2).getMinuteFrom()), LocalTime.of(timeWorking.get(2).getHourTo(), timeWorking.get(2).getMinuteTo()), restaurant, new Date());
		wednesday.setRestaurant(restaurant);
		WorkingDay thursday = new WorkingDay(timeWorking.get(3).getId(), "Thursday", LocalTime.of(timeWorking.get(3).getHourFrom(),
				timeWorking.get(3).getMinuteFrom()), LocalTime.of(timeWorking.get(3).getHourTo(), timeWorking.get(3).getMinuteTo()), restaurant, new Date());
		thursday.setRestaurant(restaurant);
		WorkingDay friday = new WorkingDay(timeWorking.get(4).getId(), "Friday", LocalTime.of(timeWorking.get(4).getHourFrom(),
				timeWorking.get(4).getMinuteFrom()), LocalTime.of(timeWorking.get(4).getHourTo(), timeWorking.get(4).getMinuteTo()), restaurant, new Date());
		friday.setRestaurant(restaurant);
		WorkingDay saturday = new WorkingDay(timeWorking.get(5).getId(), "Saturday", LocalTime.of(timeWorking.get(5).getHourFrom(),
				timeWorking.get(5).getMinuteFrom()), LocalTime.of(timeWorking.get(5).getHourTo(), timeWorking.get(5).getMinuteTo()), restaurant, new Date());
		saturday.setRestaurant(restaurant);
		WorkingDay sunday = new WorkingDay(timeWorking.get(6).getId(), "Sunday", LocalTime.of(timeWorking.get(6).getHourFrom(),
				timeWorking.get(6).getMinuteFrom()), LocalTime.of(timeWorking.get(6).getHourTo(), timeWorking.get(6).getMinuteTo()), restaurant, new Date());
		sunday.setRestaurant(restaurant);

		Menu menu = new Menu();
		menu = menuRepository.save(menu);

		menu = addMenuItems(items, menu);

		menuRepository.save(menu);
		restaurant.setMenu(menu);
		restaurantService.save(restaurant);
		workingDayService.save(Arrays.asList(monday, tuesday, wednesday, thursday, friday, saturday, sunday));

		for (int i:tableCapacity) {
			Table table = new Table("", i);
			table.setRestaurant(restaurant);
			tableRepository.save(table);
		}
	}

	private Menu addMenuItems(List<MenuItemTextDTO> items,Menu menu){
		for (MenuItemTextDTO i:items) {
			Image imageFood = new Image(i.getImagePath());
			imageService.save(imageFood);
			MenuItem menuItem = new MenuItem(i.getCategory(),i.getName(),i.getCost(),i.getDescription(),imageFood);
			menuItem.setMenu(menu);
			menuItemRepository.save(menuItem);
			menu.getItems().add(menuItem);
		}

		return menu;
	}
}
