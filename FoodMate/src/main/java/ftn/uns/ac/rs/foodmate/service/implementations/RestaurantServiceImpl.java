package ftn.uns.ac.rs.foodmate.service.implementations;

import ftn.uns.ac.rs.foodmate.commons.DateFormatter;
import ftn.uns.ac.rs.foodmate.dto.*;
import ftn.uns.ac.rs.foodmate.dto.RatingDTO;
import ftn.uns.ac.rs.foodmate.models.*;
import ftn.uns.ac.rs.foodmate.repository.RestaurantRepository;
import ftn.uns.ac.rs.foodmate.service.ImageService;
import ftn.uns.ac.rs.foodmate.service.RatingService;
import ftn.uns.ac.rs.foodmate.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private ImageService imageService;

    @Autowired
    private RatingService ratingService;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    public double addRating(RatingDTO ratingDTO) {
        Rating rating = ratingService.findRating(ratingDTO.getUsername(), ratingDTO.getRestaurantName());

        if(rating != null) {
            rating.setRating(ratingDTO.getRating());
            ratingService.save(rating);
        } else {
            Optional<User> user = userDetailsService.findByUsername(ratingDTO.getUsername());
            Restaurant restaurant = restaurantRepository.findByName(ratingDTO.getRestaurantName());
            ratingService.save(new Rating(ratingDTO.getRating(), user.get(), restaurant, new Date()));
        }

        return ratingService.calculateRestaurantRating(ratingDTO.getRestaurantName());
    }

    public double getRating(String restaurantName) {
        return ratingService.calculateRestaurantRating(restaurantName);
    }

    public void save(Restaurant restaurant) {
        restaurantRepository.save(restaurant);
    }

    public RestaurantDTO getRestaurant(String name) throws IOException {
        Restaurant restaurant = restaurantRepository.findByName(name);
        return getRestaurantDTO(restaurant);
    }

    public Restaurant getRestaurantEntity(long id) {
        Optional<Restaurant> optionalRestaurant = restaurantRepository.findById(id);
        return optionalRestaurant.orElse(null);
    }

    public RestaurantDTO getRestaurantDTO(Restaurant r) throws IOException {
        RestaurantDTO restaurantDTO = new RestaurantDTO(r.getId(), r.getName(), r.getDescription(), r.getEmail(), r.getPhoneNumber(),
                getFoodPreferencesDTO(r.getFoodPreferences()), getLocationDTO(r.getLocation()), imageService.getImageDTO(r.getMainImage()), imageService.getImageDTOs(r.getImages()), getWorkingDayDTOs(r.getWorkingDays()), ratingService.calculateRestaurantRating(r.getName()));
        return restaurantDTO;

    }

    private List<FoodPreferencesDTO> getFoodPreferencesDTO(List<FoodPreferences> foodPreferences) {
        List<FoodPreferencesDTO> foodPreferencesDTOs = new ArrayList<>();
        for(FoodPreferences fp: foodPreferences) {
            foodPreferencesDTOs.add(new FoodPreferencesDTO(fp.toString()));
        }
        return foodPreferencesDTOs;
    }

    private LocationDTO getLocationDTO(Location location) {
        return new LocationDTO(location.getLongitude(), location.getLatitude(), location.getCity(), location.getCountry(),location.getAddress());
    }

    private List<WorkingDayDTO> getWorkingDayDTOs(List<WorkingDay> workingDays) {
        List<WorkingDayDTO> workingDayDTOs = new ArrayList<>();
        for(WorkingDay wd: workingDays) {
            workingDayDTOs.add(new WorkingDayDTO(wd.getDay(), wd.getTimeFrom().format(DateFormatter.dtf), wd.getTimeTo().format(DateFormatter.dtf)));
        }
        return workingDayDTOs;
    }

    public List<RestaurantDTO> getUserChosenRestaurants(String username) throws IOException {
        Optional<User> user = userDetailsService.findByUsername(username);
        List<RestaurantDTO> restaurantDTOs = new ArrayList<>();

        for(Restaurant r: user.get().getChosenRestaurants()) {
            restaurantDTOs.add(getRestaurantDTO(r));
        }
        return restaurantDTOs;
    }

    public void addUserChosenRestaurant(NewUserChosenRestaurantDTO newUserChosenRestaurantDTO) {
        Restaurant restaurant = restaurantRepository.findByName(newUserChosenRestaurantDTO.getRestaurantName());
        Optional<User> user = userDetailsService.findByUsername(newUserChosenRestaurantDTO.getUsername());
        user.get().addChosenRestaurant(restaurant);
        userDetailsService.save(user.get());
    }

    public void deleteUserChosenRestaurant(List<NewUserChosenRestaurantDTO> newUserChosenRestaurantDTOs) {
        Optional<User> user = userDetailsService.findByUsername(newUserChosenRestaurantDTOs.get(0).getUsername());
        for(NewUserChosenRestaurantDTO newUserChosenRestaurantDTO: newUserChosenRestaurantDTOs) {
            Restaurant restaurant = restaurantRepository.findByName(newUserChosenRestaurantDTO.getRestaurantName());
            user.get().deleteChosenRestaurant(restaurant);
        }
        userDetailsService.save(user.get());
    }

    public List<Restaurant> findAll() {
        return restaurantRepository.findAll();
    }

    public List<OtherImageDTO> getOtherImagesDTO(long restaurantId) throws IOException {
        Optional<Restaurant> restaurant = restaurantRepository.findById(restaurantId);
        List<OtherImageDTO> imagesString = new ArrayList<>();
        for(Image i: restaurant.get().getImages()) {
            imagesString.add(new OtherImageDTO(i.getId(), imageService.getImage(i.getPath()), i.getPath()));
        }
        return imagesString;
    }

}
